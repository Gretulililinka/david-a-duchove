// naimportujem si knihovny
#include <math.h>
#include <stdlib.h>
#include <raylib.h>
#include <time.h>

//makra na zišťování minimální a maximální hodnoty
#ifndef MAX
#define MAX(a, b) ((a)>(b)? (a) : (b))
#define MIN(a, b) ((a)<(b)? (a) : (b))
#endif

//šířka a výška vokna
#define HERNI_SIRKA 1280
#define HERNI_VYSKA 960

// proměná která nám bude držet referenci na texturu mesice
Texture2D textura_mesic;

int main ( void )
{

    // nakonfigurujeme si raylib, že budeme chtít vytvořit okno s proměnlivou velikostí a s vertikální synchronizací
    // poznámka pod čarou, ten fígl s bitovým operátorem 'or' jakože se znakem '|' funguje tak, že každá z těch flagovejch
    // konstant má hodotu nastavenou tak, by byl v jejich bytu vobsazenej dycky jenom jeden jedinej bit. Noa když uděláme
    // to bitový or, tak se nám ty proměný zkombinujou do nový unikátní hodnoty kterou ta knihovna umí rozlišit,
    // respektive čte jednotlivý bity v bajtech :O ;D
    SetConfigFlags ( FLAG_WINDOW_RESIZABLE | FLAG_VSYNC_HINT );

    // inicializujeme vokno vo daný šířce, vejšce a s titulkem
    InitWindow ( HERNI_SIRKA, HERNI_VYSKA, "David a duchove" );

    // inicializujem audio zařízení, by sme mohli přehrávat zvuky
    InitAudioDevice();

    // nastavíme požadovanou frekvecni vykreslování
    // takle řikáme, že chceme vykreslovat šedesát snímků za sekundu (framů per sekundu)
    SetTargetFPS ( 60 );

    // načtem si ze složšky assets texturu měsice
    textura_mesic = LoadTexture ( "assets/moon.png" );

    // spustíme 'nekonečnej' while cyklus, ve kterým poběží ta naše hra
    // přeruší se když se zavře vokno nebo se zmáčkne na klávesnici čudlik 'escape'
    while ( !WindowShouldClose() ) {


        // uložíme si do proměný 'dt' kolik času realně uplynulo vod posledního vykreslenýho framu
        // máme sice nastavený to FPS na 60, nicmeně na to se nedá spolehnout, jednotlivý cykly nám mužou
        // trvat ruzně dlouho, něco se muže špracnout etc a je víc lepšejší a víc přesnější si tu deltu dycky změřit
        float dt = GetFrameTime();


        // začnem vykreslovat
        BeginDrawing();

        // překreslíme si celou vobrazovku vobdélníkem s takovým mordočerným gradientem
        // První dva argumenty funkce sou iksová a ypsilonová souřadnice levýho horního rohu toho vykreslovanýho vobdélníka,
        // když jsme tam napsali 0,0 tak tim řikáme, že ho chceme začít vykreslovat v levým horním rohu toho našeho vokna.
        // Druhý dva argumenty jsou šířka a vejška našeho vobdelnika, vykreslíme ho přes celou šířku a vejšku našeho vokna
        // poslední dva jsou barvy našeho gradientu, raylib má některý základní předdefinovaný, jinak tam mužem strkat struturu Color,
        // ve který jsou definovaný barvy RGBA v rozsahu vod nuly až po 255
        DrawRectangleGradientV ( 0,0,GetScreenWidth(),GetScreenHeight(),DARKBLUE,BLACK );

        //hovorka se asi jako bojí děsně velkejch měsiců když je furt v horrorovejch komixech kreslí,
        // tak mu uděláme radost a na pozadí vykreslíme supr velkej strašidelnej měsíc :D :D
        
        //vezmem si ten víc menšejší rozměr z šiřky a vejšky naší vobrazovky, to použijem jako velikost toho měsice
        float mensi_rozmer = MIN ( GetRenderWidth(),GetRenderHeight() );
        
        // a vykreslíme texturu měsíce. Raylib má víc podobnejch funkcí pro 2D vykreslování textur,
        // použijeme 'DrawTexturePro' pokrejvá asi nejvíc nejvěší množinu různejch kombinací argumentů co ty
        // jednotlivý fce berou, vicemeně si myslim že jich de věčinu nahradit toudle fcí
        
        // první argument je textura
        
        // druhej argument je 'zdrojovej' vobdelnik, kterým definujem v textuře voblast, kterou chceme vykreslovat
        // chceme vykreslit celej měsic, takže vybereme uplně všecko, vod horního levýho vokraje (0,0) až po dolní pravej vokraj,
        // takže šiřku a vejšku zdrojovýho vobdelniku nastavíme na šířku a vejšku textury
        
        // třetí argument je 'cílovej' vobdelník, jakože voblast na našem vokně
        // Měsíc chceme vykreslit přesně uprostřed vobrazovky a nebyl by problém si
        // dopočitat iksovou a ypsilonovu souřadnici, nicmeně mužeme použít 'fígl s originem/počátkem', takže
        // jeho ikosvou a ypsilonovou souřadnici nastavíme na střed vobrazovky (půlku vejšky a půlku šířky)
        // šířku a vejšku cílovýho vobdélníka nastavíme na proměnou 'mensi_rozmer', by nám měl ten menší rozměr vobrazovky
        // uplně vyplnit
        
        // črtvrtej argument je struktura 'Vector2', jakože dvourozměrnej bod, kterej nám řiká kde má náš vobdelnik svuj počátek
        // (defaultně to je bod (0,0)). My ho nastavíme na půlku vejšky a šířky vobdelnika, tzn. do jeho středu.
        // Tim dosáhnem toho, že se nám měsic vykreslí uplně přesně doprostřed :O ;D
        
        // předposledním argumentem je uhel povotočení textury ve stupních
        // mužeme si z legrace vyrobit třeba ňákou proměnou do který budeme cpát uplynulej čas, takže se nám měsic bude furt točit :D :D
        static float uhel = 0.0f;
        uhel += dt * 50.0f;
        
        // noa poslední argument je barva. Funguje tam takovej zálkadní blending, že nám to jakože celou tu vykreslovanou texturu tou barvou pronásobí.
        // prostě si to jakoby pro každej pixel vezme jednotlivý barevný složšky a vynásbí je to s jednotlivejma barevnejma složškama zvolený barvy,
        // ty barvy je asi nejlepčí si přectavit jako hodnoty v rozsahu vod nuly až do jedničky.
        // takže když jako barvu zvolime třeba bílou jakože WHITE, tak texturu vykreslíme pronásobenou jedničkou, takže nezměněnou, Když navopak zvolime
        // černou BLACK, tak to zase prozměnu vynasobime nulama a budem mit texturu celou černou. Když si nastavíme třeba červenou RED, tak tám to
        // z puvodních RGBA kanálů nechá zase jenom červenou a vostatní barvy pronásobí nulou etc
        // Je tam nastavenej ňákej vodstín modrý, mužeme si zkusit s jednotlivejma barvičkama hejbat
        
        DrawTexturePro ( textura_mesic, ( Rectangle ) {
            0,0,textura_mesic.width,textura_mesic.height
        }, ( Rectangle ) {
            GetRenderWidth() /2, GetRenderHeight() /2, mensi_rozmer, mensi_rozmer
        }, ( Vector2 ) {
            mensi_rozmer/2,mensi_rozmer/2
        },uhel, ( Color ) {
            102, 191, 255, 255
        } );

        // ukončíme vykreslování
        EndDrawing();
    }

    // jestli se přerušil tamten náš hlavní while cyklus, tak zavřem okno, uklidíme po sobě a skončíme
    // běh programu vrácením návratový hodnoty
    CloseWindow();

    //uklidíme texturu, to se musí
    UnloadTexture ( textura_mesic );

    // vypnem audio zařízení
    CloseAudioDevice();

    // nakonec vrátíme nulu jakože všecko proběhlo v cajku a hotovo
    return 0;
}

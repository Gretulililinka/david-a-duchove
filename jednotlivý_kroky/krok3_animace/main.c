// naimportujem si knihovny
#include <math.h>
#include <stdlib.h>
#include <raylib.h>
#include <time.h>

// naimportujem si vlastní hlavičku
#include "animace.h"

//makra na zišťování minimální a maximální hodnoty
#ifndef MAX
#define MAX(a, b) ((a)>(b)? (a) : (b))
#define MIN(a, b) ((a)<(b)? (a) : (b))
#endif

//šířka a výška vokna
#define HERNI_SIRKA 1280
#define HERNI_VYSKA 960

// textury
Texture2D textura_mesic;
Texture2D textura_david_spritesheet;

// šířka jednotlivejch obrázků/framů animace
// (pro necečkaře, slovičko 'DAVID_F_SIRKA' a jiný definovaný nám pak prekompilátor nahradí tou hodnotou)
#define DAVID_F_SIRKA 150.0f

// vejška jednotlivejch obrázků animace
#define DAVID_F_VYSKA 240.0f

//jednotlivý animace/pole framů

// jsou to vlastně pole strutkur typu 'Rectangle'
// rectangle deklarujeme čtyrma proměnejma:
// 1. iksová souřadnice levýho horního rohu
// 2. ypsilonová souřadnice levýho horního rohu
// 3. šiřka vobdelnika
// 4. vejška vobdelnika

// všechny framy maj stejnou vejšku i šířku takže máme vlastně takovou jakože mřížku
// když si ten vobrázek spritesheetu votevřeme v ňákým prohlížeči vobrázků,
// tak si mužeme jednoduše prstem na monitoru vodpočítat sloupec (iksová souřadnice) a řádek (ypsilonová)
// a tim pronásobit šiřku příp. vejšku framu a tim dostanem pozici v tý textuře
// (nvm jestli jakoby mam vysvětlovat i takovýdle trivialni věci ale asi jo když už to dělám)


// pole framů animace běhu
Rectangle david_framy_behu[] = {
    {DAVID_F_SIRKA*0,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*1,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*2,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*3,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*4,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*5,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},

    {DAVID_F_SIRKA*0,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*1,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*2,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA}
};

// pole framů animace skoku
// ( sou to vlastně použitý některý framy běhu, takže sme vlastně ziskali
// 4 aktivity z puvodnich 3 )
Rectangle david_framy_skoku[] = {

    {DAVID_F_SIRKA*5,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},

    {DAVID_F_SIRKA*0,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*1,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*2,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA}
};

// pole framů animace pérování na místě/flákání se
Rectangle david_framy_idle[] = {
    {DAVID_F_SIRKA*4,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*5,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*0,DAVID_F_VYSKA*3,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*1,DAVID_F_VYSKA*3,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*2,DAVID_F_VYSKA*3,DAVID_F_SIRKA,DAVID_F_VYSKA},
};

// pole framů sednutí si na zadek
Rectangle david_framy_sed[] = {
    {DAVID_F_SIRKA*3,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*4,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*5,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*0,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*1,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*2,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*3,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
};

int main ( void )
{

    // nakonfigurujeme si raylib, že budeme chtít vytvořit okno s proměnlivou velikostí a s vertikální synchronizací
    // poznámka pod čarou, ten fígl s bitovým operátorem 'or' jakože se znakem '|' funguje tak, že každá z těch flagovejch
    // konstant má hodotu nastavenou tak, by byl v jejich bytu vobsazenej dycky jenom jeden jedinej bit. Noa když uděláme
    // to bitový or, tak se nám ty proměný zkombinujou do nový unikátní hodnoty kterou ta knihovna umí rozlišit,
    // respektive čte jednotlivý bity v bajtech :O ;D
    SetConfigFlags ( FLAG_WINDOW_RESIZABLE | FLAG_VSYNC_HINT );

    // inicializujeme vokno vo daný šířce, vejšce a s titulkem
    InitWindow ( HERNI_SIRKA, HERNI_VYSKA, "David a duchove" );

    // inicializujem audio zařízení, by sme mohli přehrávat zvuky
    InitAudioDevice();

    // nastavíme požadovanou frekvecni vykreslování
    // takle řikáme, že chceme vykreslovat šedesát snímků za sekundu (framů per sekundu)
    SetTargetFPS ( 60 );

    // načtem soubory textur
    textura_david_spritesheet = LoadTexture ( "assets/david.png" );
    textura_mesic = LoadTexture ( "assets/moon.png" );
    
    // vyrobíme si animace
    
    //animace běhání
    Animace a_behani = 
    {
        // jedna patnactina sekundy
        .trvani_framu = 1.0f/15.0f,
        
        //relativní čas a index vynulujem
        .relativni_cas = 0.0f,
        .index = 0,
        
        // textura bude ten spritesheet
        .textura = textura_david_spritesheet,
        
        // animace běhání bude mit samože framy běhu
        .framy = david_framy_behu,
        
        // počet framů ziskáme podělením velikosti celýho pole velikostí jednoho prvku
        .pocet_framu = sizeof ( david_framy_behu ) / sizeof ( Rectangle )
    };
    
    //animace skákání
    Animace a_skok = 
    {
        .trvani_framu = 1.0f/15.0f,
        .relativni_cas = 0.0f,
        .index = 0,
        .textura = textura_david_spritesheet,
        .framy = david_framy_skoku,
        .pocet_framu = sizeof ( david_framy_skoku ) / sizeof ( Rectangle )
    };
    
    //animace idle
    Animace a_idle = 
    {
        .trvani_framu = 1.0f/15.0f,
        .relativni_cas = 0.0f,
        .index = 0,
        .textura = textura_david_spritesheet,
        .framy = david_framy_idle,
        .pocet_framu = sizeof ( david_framy_idle ) / sizeof ( Rectangle )
    };
    
    //animace sednutí si
    Animace a_sed = 
    {
        .trvani_framu = 1.0f/15.0f,
        .relativni_cas = 0.0f,
        .index = 0,
        .textura = textura_david_spritesheet,
        .framy = david_framy_sed,
        .pocet_framu = sizeof ( david_framy_sed ) / sizeof ( Rectangle )
    };

    // spustíme 'nekonečnej' while cyklus, ve kterým poběží ta naše hra
    // přeruší se když se zavře vokno nebo se zmáčkne na klávesnici čudlik 'escape'
    while ( !WindowShouldClose() ) {

        float dt = GetFrameTime();
        
        // aktualizujem animace
        // ( naše funkce chce po nás ukazatel, takže tam cpem adresu )
        aktualizovatAnimaci(&a_behani, dt);
        aktualizovatAnimaci(&a_skok, dt);
        aktualizovatAnimaci(&a_idle, dt);
        aktualizovatAnimaci(&a_sed, dt);


        // začnem vykreslovat
        BeginDrawing();

        DrawRectangleGradientV ( 0,0,GetScreenWidth(),GetScreenHeight(),DARKBLUE,BLACK );

        //vykreslíme animace
        vykreslitAnimaci(&a_behani,(Vector2){0,0},WHITE);
        vykreslitAnimaci(&a_skok,(Vector2){DAVID_F_SIRKA*1,0},WHITE);
        vykreslitAnimaci(&a_idle,(Vector2){DAVID_F_SIRKA*2,0},WHITE);
        vykreslitAnimaci(&a_sed,(Vector2){DAVID_F_SIRKA*3,0},WHITE);

        // skončímes vykreslováním
        EndDrawing();
    }

    // jestli se přerušil tamten náš hlavní while cyklus, tak zavřem okno, uklidíme po sobě a skončíme
    // běh programu vrácením návratový hodnoty
    CloseWindow();

    //uklidíme textury
    UnloadTexture ( textura_mesic );
    UnloadTexture ( textura_david_spritesheet );

    // vypnem audio zařízení
    CloseAudioDevice();

    // nakonec vrátíme nulu jakože všecko proběhlo v cajku a hotovo
    return 0;
} 

#ifndef _DAVID_A_DUCHOVE_ANIMACE_H_
#define _DAVID_A_DUCHOVE_ANIMACE_H_
// ten fígl s ifndef nahoře slouží k tomu by se nám
// soubor naimportoval jenom jednou

#include <raylib.h>

typedef struct Animace {
    
    // textura animace
    Texture2D textura;

    // jak dlouho bude trvat jeden frame/snímek animace
    float trvani_framu;
    
    // jakože vnitřní čas tý animace (se bude hodit)
    float relativni_cas;

    // ukazatel na pole framů (v cčku je pole a ukazatel tak trochu uplně to samý)
    Rectangle * framy;
    
    // kolik má animace framů celkem
    size_t pocet_framu;
    
    // index právě zobrazenýho framu
    int index;
} Animace;

// bere dva argumenty, ukazatel na animaci noa pak časovou deltu 
void aktualizovatAnimaci ( Animace * animace, float dt )
{
    //přičtem časovou deltu k tamtomu vnitřnímu času
    animace->relativni_cas += dt;

    // dokud je relativní čas víc věčí než je doba trvání jednoho framu,
    // tak vodečtem vod relativního času bodu trvání framu a posunem index vo jedničku
    while ( animace->relativni_cas > animace->trvani_framu ) {
        animace->relativni_cas -= animace->trvani_framu;
        animace->index++;
        
        // jestli sme vyskočili z maximální dýlky animce, tak se
        // přetočíme zpátky na začátek
        if(animace->index >= animace->pocet_framu)
            animace->index = 0;

    }
}

// bere tři argumenty, ukazatel na animaci, souřadnici kde se bude vykreslovat levej horní roh a barvu
void vykreslitAnimaci ( Animace * animace, Vector2 pozice, Color barva )
{
        DrawTextureRec ( animace->textura, animace->framy[animace->index],pozice, barva );
}

#endif 

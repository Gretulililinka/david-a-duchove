#ifndef _DAVID_A_DUCHOVE_DAVID_H_
#define _DAVID_A_DUCHOVE_DAVID_H_

// naimportujem si animaci
// (s ní se nám současně natáhne raylib.h)
#include "animace.h"
#include "mapa.h"

// kouzelným slovíčkem 'extern' https://www.geeksforgeeks.org/understanding-extern-keyword-in-c/
// mužeme 'sdílet' se souborem 'main.c' v něm deklarovaný proměný, teďko na ukázku zatim jenom
// zvuk chůze, de ale všecko :O ;D 
extern Sound zvuk_kroku;

// přestěhujem si sem z 'main.c' framy animace by to tam zbytečně nezabiralo misto ve zdrojáčku 
#define DAVID_F_SIRKA 150.0f
#define DAVID_F_VYSKA 240.0f

Rectangle david_framy_behu[] = {
    {DAVID_F_SIRKA*0,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*1,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*2,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*3,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*4,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*5,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},

    {DAVID_F_SIRKA*0,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*1,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*2,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA}
};

Rectangle david_framy_skoku[] = {

    {DAVID_F_SIRKA*5,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},

    {DAVID_F_SIRKA*0,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*1,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*2,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA}


};

Rectangle david_framy_idle[] = {
    {DAVID_F_SIRKA*4,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*5,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*0,DAVID_F_VYSKA*3,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*1,DAVID_F_VYSKA*3,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*2,DAVID_F_VYSKA*3,DAVID_F_SIRKA,DAVID_F_VYSKA},
};

Rectangle david_framy_sed[] = {
    {DAVID_F_SIRKA*3,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*4,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*5,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*0,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*1,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*2,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*3,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
};

// tady si budeme definovat různý hodnoty a konstatny k davidoj
// zatim si tady jenom nadeklarujeme rychlost jeho chůze
// Je vzdálenost kterou david urazí za jednu vteřinu
#define RYCHLOST_CHUZE_DAVIDA 350.0f

typedef struct David {
    
    // jednotlivý animace davida
    Animace animace_beh;
    Animace animace_idle;
    Animace animace_sed;
    Animace animace_skok;

    // ukazatel na animaci, která bude jakože jedniná vybraná,
    // aktualizovaná a vykreslovaná. Máme čtyry ale vidět chcem přece jako
    // jenom jednu :D :D
    Animace * aktualni_animace;


    // pozice levýho horního vokraje vykreslovaný textury
    Vector2 pozice;
    
    // okraje naší herní postavičky (vykreslíme si, když v 'main.c' nahoře aktivujem 'DEBUG')
    // bude se nám hodit v budoucnu třeba pro kolize s jinejma herníma věcma/entitamama
    // (vymezíme si menší voblast než jsou rozměry framu animace)
    Rectangle okraje;
    
    // směr kterým David kouká. Jednička znamená z leva doprava, -1 znamená z z prava doleva
    int smer;
    
    // dlaždicová mapa, ve který se david pohybuje
    Mapa * mapa;

} David;

// funcke na aktualizování davida, argumenty jsou ukazatel na strukturu 'David' a hodnota časový delty
// Funkce nám bude kromě aktualizovávání Davida hlídat i uživatelský vstupy na klávesnici, tzn. mačkání
// čudlíků, kterejma se david bude hejbat
void aktualizovatDavida ( David * david, float dt )
{

    // pokud je máčknutá na klávesnici šipka nahoru tak se kouknem jestli si david sednul na
    // zem nebo jestli si právě na zem sedá. To poznáme tim, že jako atribut 'aktualni_animace' je nastavená
    // animace sednutí si na zem, tzn. že ukazatel aktuální animace ukazuje na adresu atributu 'animace_sed'
    if ( IsKeyDown ( KEY_UP ) ) {

        if ( david->aktualni_animace == &david->animace_sed ) {
            
            // pro případ že je animace pauznutá (neloopujicí animace se nám samy pauznou když dojenou na konec)
            // ji vodpauzujeme....
            david->aktualni_animace->pauznuta = false;
            // ....a začnem přehrávat pozpátku
            david->aktualni_animace->reverzne=true;

            // pokud aktuální animace (což je furt animace sednutí si) dojela zpátky na začátek (index je nula),
            // tak přepnem aktuální animaci na takový to pérování v kolenou (do ukazatele aktualni_animace narvem adresu
            // animace idle)
            if ( david->aktualni_animace->index == 0 ) {
                david->aktualni_animace = & david->animace_idle;
            }
        } 
    }


    
    // voproti minulýmu kódu sme si sem přidali podmínku, že davida bude možný posouvat doleva jenom když je jeho pozice
    // věčí než nula (by nám neutek z mapy)
    if ( IsKeyDown ( KEY_LEFT ) && david->pozice.x > 0 && david->aktualni_animace != &david->animace_sed ) {
        
        // nastavíme jeho směr doleva (vicemeně jenom kuli vykreslování animací)
        david->smer = -1;
        
        Rectangle prepozice = david->okraje;
        prepozice.x -= RYCHLOST_CHUZE_DAVIDA * dt;

        // kouknem se, jestli nemá prepozice kolizi s některým ze bloků mapy 
        // Jestli jo, tak se nebudem posouvat a přepnem animaci na 'idle'
        if ( kolizeRectSeBlokemMapy ( prepozice, david->mapa ) ) {
            david->aktualni_animace = & david->animace_idle;
        } else {

            //jestli ale kolizi nemáme, tak Davida normálně šoupnem na prepozici jako sme to ďáli předtim
            david->okraje.x = prepozice.x;
            david->pozice.x = david->okraje.x - 45;
            david->aktualni_animace = &david->animace_beh;
            }

        }
    
    // Voproti minule sme si sem přidali podmínku by david nemoch utýct z herní mapy, doprava teďko bude chodit navíc jenom když bude jeho
    // iksová souřadnice menčí než šiřka mapy (- minus šiřka velikosti framu animace, davidova pozice vodpovídá levý horní souřadnici jeho
    // atributu 'okraje', nás zajímá ale pravej vokraj tak to celý musíme šoupnout vo jeho šířku)
    else if ( IsKeyDown ( KEY_RIGHT ) && david->pozice.x < david->mapa->sirka*BLOK_SIRKA - DAVID_F_SIRKA && david->aktualni_animace != &david->animace_sed ) {

        david->smer = 1;
        Rectangle prepozice = david->okraje;
        prepozice.x += RYCHLOST_CHUZE_DAVIDA * dt;
        
        if ( kolizeRectSeBlokemMapy ( prepozice, david->mapa ) ) {
            david->aktualni_animace = & david->animace_idle;
        } else {
            david->okraje.x = prepozice.x;
            david->pozice.x = david->okraje.x - 45;
            david->aktualni_animace = & david->animace_beh;
            } 
    }
    // jestli je máčkutej na klávesnici čudlik šipky dolu, tak začnem přehrávat animaci sednutí
    // si na zadek
    else if ( IsKeyDown ( KEY_DOWN ) ) {

        // přepnem aktualní animaci....
        david->aktualni_animace = & david->animace_sed;
        
        // ....nastavíme atribut 'reverzně' na 'false' by se nám animace přehrávala normálně,
        // jakože směrem vod začátku ke konci....
        david->aktualni_animace->reverzne = false;
        // ....a vodpauzujem ji, pro případ kdyby byla pauznutá
        david->aktualni_animace->pauznuta = false;
        }
    
    // mačkání tědlech čudliků sme si jakoby zřetězili takovou jakože nudlí if()else if() else...... takže jestli
    // sme se teďko v běhu programu dostali k tomudlectomu kousku kódu, tak neni máčknutej žádnej knoflik.
    // Koukneme se, jestli si David náhodou nesednul na zem nebo jestli si zrovna teďko nesedá nebo z tý země nevstává
    // (to poznáme tim, jestli má jako aktualní animaci nastavenou animaci sedání) noa jestli ne, tak mu přepnem
    // animaci na to akční pérování v kolenou
    else if ( david->aktualni_animace != &david->animace_sed )
    {
        david->aktualni_animace = & david->animace_idle;
    }

    // nastavíme vertikální překlopení vykreslování textury podle Davidova atributu 'směr', jakože podle toho, jakým
    // směrem se kouká
    david->aktualni_animace->zrcadlit = david->smer<0;

    // pokud je aktuální animace animace sedání a pokud ta animace dojela už na konec (animace sednutí se neloopuje,
    // takže když dojede na konec, tak se sama pauzne) tak se kouknem, na index zobrazovanýho snimku. Pokud to je první
    // snímek animace (index rovnej nule), tak víme, že David už nesedí, ani si nesedá, ani nevstává, ale že už vstal.
    // Takže přepnem animaci na 'idle'
    if ( david->aktualni_animace == &david->animace_sed ) {
        if ( david->animace_sed.pauznuta ) {
            if ( david->animace_sed.index == 0 ) {
                david->aktualni_animace = & david->animace_idle;
            }
        }

    }

    // pokud je aktualní animace běhání, tak se kouknem jestli hraje zvuk kroků noa jestli ne,
    // ho začnem přehrávat 
    else if ( david->aktualni_animace == &david->animace_beh ) {
        if ( !IsSoundPlaying ( zvuk_kroku ) ) {
            PlaySound ( zvuk_kroku );
        }
    }

    // nakonec aktualizujem aktuální animaci :D ;D
    aktualizovatAnimaci ( david->aktualni_animace, dt );

}

// funkce na vykreslování Davida
void vykreslitDavida ( David * david )
{
    vykreslitAnimaci ( david->aktualni_animace, david->pozice, WHITE );

    // jestli je definovanej 'DEBUG', vykreslíme vokraje třeba zelenou barvičkou
#ifdef DEBUG
    DrawRectangleLines ( david->okraje.x, david->okraje.y, david->okraje.width, david->okraje.height, GREEN );
#endif
}

#endif

// naimportujem si knihovny
#include <math.h>
#include <stdlib.h>
#include <raylib.h>
#include <time.h>

// Pokuď vodkomentujeme, tak to zkompiluje preprocesorovou podmínkou vypnutý věci
// napřiklad se kolem některejch herních voběktů budou vykreslovat okraje
// #define DEBUG

// naimportujem si vlastní hlavičky
#include "animace.h"
#include "david.h"

//makra na zišťování minimální a maximální hodnoty
#ifndef MAX
#define MAX(a, b) ((a)>(b)? (a) : (b))
#define MIN(a, b) ((a)<(b)? (a) : (b))
#endif

//šířka a výška vokna
#define HERNI_SIRKA 1280
#define HERNI_VYSKA 960

// textury
Texture2D textura_mesic;
Texture2D textura_david_spritesheet;
Texture2D textura_kameny;

// zvuky
Sound zvuk_kroku;

int main ( void )
{

    // nakonfigurujeme si raylib, že budeme chtít vytvořit okno s proměnlivou velikostí a s vertikální synchronizací
    // poznámka pod čarou, ten fígl s bitovým operátorem 'or' jakože se znakem '|' funguje tak, že každá z těch flagovejch
    // konstant má hodotu nastavenou tak, by byl v jejich bytu vobsazenej dycky jenom jeden jedinej bit. Noa když uděláme
    // to bitový or, tak se nám ty proměný zkombinujou do nový unikátní hodnoty kterou ta knihovna umí rozlišit,
    // respektive čte jednotlivý bity v bajtech :O ;D
    SetConfigFlags ( FLAG_WINDOW_RESIZABLE | FLAG_VSYNC_HINT );

    // inicializujeme vokno vo daný šířce, vejšce a s titulkem
    InitWindow ( HERNI_SIRKA, HERNI_VYSKA, "David a duchove" );

    // inicializujem audio zařízení, by sme mohli přehrávat zvuky
    InitAudioDevice();

    // nastavíme požadovanou frekvecni vykreslování
    // takle řikáme, že chceme vykreslovat šedesát snímků za sekundu (framů per sekundu)
    SetTargetFPS ( 60 );

    // načtem soubory textur
    textura_david_spritesheet = LoadTexture ( "assets/david.png" );
    textura_mesic = LoadTexture ( "assets/moon.png" );
    textura_kameny = LoadTexture ( "assets/kameny.png" );
    
    // načtem zvuky
    zvuk_kroku = LoadSound ( "assets/kroky.wav" );
    
    // vyrobíme si instance jednotlivejch animací možnejch davidovejch aktivit
    // animace běhu

    // jeden snímek animace nám bude trvat jednu třicetinu sekundy, relativní čas a index vynulujem, zapnem atribut 'jojo' by
    // se nám pak animace po normálním přehrání pouštěla pozpátku, nastavíme atributy 'reverzně' a 'zrcadlit' na false,
    // zapnem loopování, nastavíme texturu a pole jednotlivejch framů ve spritesheetu, počet framů/dylku pole spočitáme podělením velikosti
    // celýho pole velikostí jednoho prvku
    Animace david_beh = {
        .trvani_framu = 1.0f/30.0f, .relativni_cas=0.0f, .index = 0,
        .jojo = true, .reverzne = false, .zrcadlit = false, .loopovat = true,
        .textura = textura_david_spritesheet, .framy = david_framy_behu, .pocet_framu = sizeof ( david_framy_behu ) /sizeof ( Rectangle )
    };
    // animace idle, jakože když se fláká a nic nedělá. Je to takový pérování nohama na místě
    Animace david_idle = {
        .trvani_framu = 1.0f/15.0f, .relativni_cas=0.0f, .index = 0,
        .jojo = true, .reverzne = false, .zrcadlit = false, .loopovat = true,
        .textura = textura_david_spritesheet, .framy = david_framy_idle, .pocet_framu = sizeof ( david_framy_idle ) /sizeof ( Rectangle )
    };
    // animace sednutí si, puvodně to měla bejt animace chcípnutí jakože si ztoho sedne na zadek :D ale pak mě napadlo že ta hra
    // by mohla bejt víc zajimavější když by se david moch přikrčovat před muslimskou střelbou k zemi
    // pozn. si všimněte že má animace vypnutej loop
    Animace david_sed = {
        .trvani_framu = 1.0f/30.0f, .relativni_cas=0.0f, .index = 0,
        .jojo = true, .reverzne = false, .zrcadlit = false, .loopovat = false,
        .textura = textura_david_spritesheet, .framy = david_framy_sed, .pocet_framu = sizeof ( david_framy_sed ) /sizeof ( Rectangle )
    };

    // animace skoku, david tam vicemeně jenom máchá nožičkama ve vzduchu
    Animace david_skok = {
        .trvani_framu = 1.0f/10.0f, .relativni_cas=0.0f, .index = 0,
        .jojo = true, .reverzne = false, .zrcadlit = false, .loopovat = true,
        .textura = textura_david_spritesheet, .framy = david_framy_skoku, .pocet_framu = sizeof ( david_framy_skoku ) /sizeof ( Rectangle )
    };
    
    // alokujeme si dynamický dourozměrný pole který jakože bude mřížka tý naší herní mapy
    // použijem mito mallocu calloc, kterej má tu vyhodu že alokovanou paměť vyplní nulama
    // (alokovanou paměť zase musíme uvolnit)
    const int vejska_mapy = 10;
    const int sirka_mapy = 20;
    int ** bloky = calloc ( vejska_mapy, sizeof ( int * ) * vejska_mapy );
    for ( size_t i=0; i<vejska_mapy; i++ ) {
        bloky[i] = calloc ( sirka_mapy,sizeof ( int ) );
    }

    // a spodní vrstvu mapy vyplníme plošinou
    for ( size_t i=0; i<sirka_mapy; i++ )
        bloky[vejska_mapy - 1][i] = 1;
    
    // přidáme si do cesty pár bloků, by sme viděli jak se David zasekává vo překážky
    bloky[vejska_mapy - 2][7] = 1;
    bloky[vejska_mapy - 2][10] = 1;
    bloky[vejska_mapy - 3][10] = 1;
    
    // vyrobíme si herní mapu
    Mapa mapa = {
        
        .textura = textura_kameny,
        .sirka = sirka_mapy,
        .vyska = vejska_mapy,
        .bloky = bloky
        
    };
    
    
    // kdyžuž máme vyrobený animace, tak si mužeme vyrobit Davida
    David david = {
                    
        .animace_beh = david_beh,
        .animace_idle = david_idle,
        .animace_sed = david_sed,
        .animace_skok = david_skok,
         
        .aktualni_animace = NULL,

        // davida postavíme nohama na ty kostky
        // (spočitáme si souřadnici spodní vrstvy a vodečtem vejšku davida
        // (pozice je levej horní roh, my nastavujem spodní stranu))
        .pozice = {0,(vejska_mapy - 1) * 80 - DAVID_F_VYSKA},
                    

        .smer = 1,
        
        // strčíme mapu do Davida
        .mapa = &mapa,

    };

    // nastavíme ukazatel aktuální animace na animaci 'idle'
    david.aktualni_animace = &david.animace_idle;
                
    // podle aktuální pozice nastavíme okraje oběktu
    // (jsou vo trošku menčí než vokraje voblasti framu animace)
    david.okraje = ( Rectangle ) {
        david.pozice.x + 45, david.pozice.y +8, DAVID_F_SIRKA -90, DAVID_F_VYSKA - 10
    };

    
    while ( !WindowShouldClose() ) {

        float dt = GetFrameTime();
        
        //aktualizujem Davida
        aktualizovatDavida(&david, dt);

        BeginDrawing();

        DrawRectangleGradientV ( 0,0,GetScreenWidth(),GetScreenHeight(),DARKBLUE,BLACK );

        //vykreslíme mapu
        vykreslitMapu(&mapa,0.0f,9999.0f);
        
        //vykreslíme davida
        vykreslitDavida(&david);

        EndDrawing();
    }

    CloseWindow();
    
    // musíme uklidit to alokovaný pole 'bloky'
    for ( size_t i=0; i<vejska_mapy; i++ ) {
        free ( bloky[i] );
    }
    free ( bloky );

    //uklidíme textury
    UnloadTexture ( textura_mesic );
    UnloadTexture ( textura_david_spritesheet );
    UnloadTexture ( textura_kameny );

    // vypnem audio zařízení
    CloseAudioDevice();
    
    // a taky uvolníme zvuky
    UnloadSound ( zvuk_kroku );

    return 0;
}

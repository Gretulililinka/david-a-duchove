#ifndef _DAVID_A_DUCHOVE_MAPA_H_
#define _DAVID_A_DUCHOVE_MAPA_H_

#include <raylib.h>

// vejška a šířka kostiček, resp. 'dlaždic', ze kterejch budem skládat tu naši herní mapu
#define BLOK_SIRKA 80
#define BLOK_VYSKA 80

// makra na zišťování maximální/minimální hodnoty
#ifndef MAX
#define MAX(a, b) ((a)>(b) ? (a) : (b))
#define MIN(a, b) ((a)<(b) ? (a) : (b))
#endif

// struktura tý dlaždicový mapy
typedef struct Mapa
{
    // v cčku muže bejt ukazatel pole, noa když máme ukazatel na ukazatele takže tam vlastně mužeme schovat
    // 'pole polí' a tak ziskat dvourozměrný pole. Todlecto 2d pole bude vlastně taková jakože mřížka a jednotlivý
    // chlívečky v mřížce mužou bejt buďto volný misto, noa nebo šutr.Jednotlivý kostičky herní mapy budem popisovat číslem,
    // nula bude prázndej prostor, kladný číslo bude znamenat žeje tednlecten 'chlíveček v mřížce' vobsazenej pevným blokem.
    // Noa konkrétní hodnota toho čísla bude určovat konkrétní texturu v spritesheetu kamenů, kterou vykreslit (máme víc variant)
    int ** bloky;
    
    //vejška a šířka toho našeho 2d pole
    int sirka, vyska;
    
    // textura, kterou budem používat na vykreslování těch kostek
    Texture2D textura;
}Mapa;

// funkce na vykreslování mapy
// bere tři argumenty, první je samozdřejmě ukazatel na mapu, noa pak minimální a maximální hodnota vykreslovaný iksový
// souřadnice, ty nám vymezujou kterou voblast mapy budem malovat. Dokavaď máme tu mapu relativně malou, tak takovýho něco asi
// jako nemusíme moc řešit. Kdybysme ale měli mapu děsně dlouhatatatatánckou bambilion kostiček tak by nám vykreslování celý mřížky
// dlaždicový mapy žralo zbytečně celkem poctatnej kus počítacího víkonu. Proto si stanovujem vokraje vykreslovaný mapy 
void vykreslitMapu(Mapa * mapa, float min_x, float max_x)
{
    // vypočítáme si z hodnoty iksový polohy souřadnici kostky v dlaždicový mapě
    // (prostě to číslo podělíme dýlkou bloku/dlaždice a zavokrouhlíme. Menčí hodnotu zavokrouhlíme floorem
    // dolu, večí čislo ceilem nahoru, by sme fakt jako pokryly i ty nejvíc nejvokrajovějšejší hodnoty)
    int _min_x = (int)floor((min_x) / (float)BLOK_SIRKA);
    int _max_x = (int)ceil((max_x) / (float)BLOK_SIRKA);
    
    // si pohlídáme si by sme mezema nevyskočili z mapy a nelezli mimo voblast pole
    // takže dolní mez muže bejt minimálně nula....
    _min_x = MAX(0, _min_x);
    // ....a horní mez maximálně velká jako šířka celý mapy
    _max_x = MIN(mapa->sirka, _max_x);
    
    // projdeme si mapu vod min_x až po max_x sloupec po sloupci....
    for(int x = _min_x; x < _max_x; x++)
    {
        // každej sloupeček budem číst vodzhora až dolu
        for(int y = 0; y < mapa->vyska; y++)
        {
            // a pro každej chlíveček v tý 2d mřížce se kouknem jaká je tam skovaná hodnota 
            int blok = mapa->bloky[y][x];
            
            //noa jestli to neni nula (nebo zaporný čislo), tak vykreslíme vodpovidajicí kostku
            if(blok)
                // jeruzálémský kameny sou ve spritesheetu srovnaný v jednom řádku za sebou takže vodpovidajicí pořadí ziskame pronásobením
                // dýlky kostky hodnotou vobsaženou v proměný 'blok'. Šutry maj ve spritesheetu kolem sebe trošku volnýho místa (by se předešlo
                // tile bleedingu), tendle vokraj musíme zohlednit (sou to ty 2.0f, co tam strašej)  
                DrawTextureRec(mapa->textura, (Rectangle){2.0f +84.0f * (float)(blok-1),2.0f,80.0f,80.0f},(Vector2){(float)x*BLOK_SIRKA,(float)y*BLOK_VYSKA},DARKBLUE);
        }
        
    }
    
    // noa teďko mě napadnul uplně supr zlepšovák pro vykreslování tý naší mapy, by nám jentak nevysela vevzuduchu jako taková tenká vrstva
    // tlustá jenom jednu kostku na vejšku. Projeme si zase celou mapu a kouknem jestli souřadnice v tom nejvíc nejspodnějším řádku mapy
    // neni ďoura, noa jestli neni resp. je tam pevnej blok, tak eště tři kostičky vykreslíme
    // ( jestli nastavíme vejšku mapy na deset kostiček, tak to bude dohromady nějakejch 800 pixelů. Vejšku vokna máme definovanou v main.c jako
    // 960 pixelů, takže když budeme vykreslovat naši dlaždicovou mapu na pozici (0,0) jakože v levým a hlavně horním rohu vobrazovky, tak nám
    // na vejšku na pokrytí celý vobrazovky zbejvá eště 160 pixelů, to sou dvě kostky, takže tři musej stačit)

    for(int x = _min_x; x < _max_x; x++)
    {
        if(mapa->bloky[mapa->vyska-1][x] != 0)
        for(int y = mapa->vyska; y < mapa->vyska + 3; y++)
        {
            int index = mapa->bloky[mapa->vyska - 1][x];
            index = 7;
            DrawTextureRec(mapa->textura, (Rectangle){2 +84*(index-1),2,80,80},(Vector2){x*BLOK_SIRKA,y*BLOK_VYSKA},DARKBLUE);
        }
    }
}

// funkce na vytažení hodnoty bloku mapy na souřadnicích 'x' a 'y'
// jenom to hlídá by sme nevyskočili z mapy a neptali se na hodnotu ležicí mimo rozsah toho 2d pole
// (hežčí by to asi jako bylo hlídat ty meze v těch druhejch funkcích páč by to zredukovalo množšství
// volání ifů)
int getBlokMapy(int x, int y, Mapa * mapa)
{    
    if(x >= 0 && x < mapa->sirka && y>=0 && y < mapa->vyska)
    {
        return mapa->bloky[y][x];
    }
    // jestli sme vyskočili z mapy, vrátíme -1
    return -1;
}

// funkce na zišťování, jestli bod leží ve voblasti některý z kostek mapy 
bool kolizeSeBlokemMapy_bod(Vector2 bod, Mapa * mapa)
{
    // převedem si souřadnici na polohu v mapě 
    int x = (int)floor(bod.x / (float)BLOK_SIRKA);
    int y = (int)floor(bod.y / (float)BLOK_VYSKA);
    // a kouknem jestli je to kladná hodnota
    // (v cčku by sme to mohli napsat i bez toho '> 0' na konci :O ;D)
    return getBlokMapy(x,y, mapa) > 0;
}

// funkce na zišťování, jestli vobdelnik leží na některý kostce mapy
bool kolizeRectSeBlokemMapy(Rectangle box, Mapa * mapa)
{
    // převedem si souřadnice vobdelnika do souřadnic mapy
    int x = (int)floor((box.x) / (float)BLOK_SIRKA);
    int y = (int)floor(box.y / (float)BLOK_VYSKA);
    
    int y_max = (int)ceil((box.y + box.height) / (float)BLOK_VYSKA);
    int x_max = (int)ceil((box.width + box.x) / (float)BLOK_VYSKA);

    // a projdem všecky chlívečky mřížky, do kterejch ten náš vobdelnik zasahuje
    // pokavaď aspoň jeden chlíveček vobsahuje bevnej blok, tak máme kolizi
    for (int j = x; j < x_max; j++)
    for (int i = y; i < y_max; i++)
    {
        if(getBlokMapy(j,i, mapa) > 0)
            return true;
    }
    
    return false;
}



#endif

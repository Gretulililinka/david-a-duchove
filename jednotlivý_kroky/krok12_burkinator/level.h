#ifndef _DAVID_A_DUCHOVE_LEVEL_H_
#define _DAVID_A_DUCHOVE_LEVEL_H_

#include <raylib.h>
#include <stdlib.h>
#include <math.h>

#include "mapa.h"
#include "david.h"
#include "duch.h"

// zvuky
extern Sound zvuk_zasah;
extern Sound zvuk_kontakt;

#define MAPA_MAX_VYSKA 10
#define NUTNA_VZDALENOST (BLOK_SIRKA * 20)

// struktura levelu
typedef struct Level {
    
    Duch ** duchove;
    size_t pocet_duchu;
    
    // level teďko bude mit burkinátory,
    // alokujem si je stejným stylem jako předtim duchy
    Duch ** burkinatori;
    size_t pocet_burkinatoru;

    Mapa * dlazdicova_mapa;

} Level;

typedef struct SegmentMapy {
    int sirka;
    int vyska;
    int  * pole;
} SegmentMapy;


// funkce na vygenerování náhodnýho levelu, vlastně něco jako konstruktor
// první argument 'šiřka' je počet kostek jak má bejt level dlouhej (předpokládá se čislo věčí dvacíti a dělitelný pěti)
// druhej textura tý dlaždicový mapy
Level * vygenerovatLevel ( int sirka, Texture2D texturaTiledMapy )
{
    Level * lvl = (Level * )malloc ( sizeof ( Level ) );
    Mapa * mapa = (Mapa * )malloc ( sizeof ( Mapa ) );

    Duch ** duchove = (Duch **)malloc ( sizeof ( Duch * ) * 256 );
    
    // alokujem si burkinátory
    Duch ** burkinatori = (Duch **)malloc ( sizeof ( Duch * ) * 256 );
    
    // enum který nám bude popisovat jednotlivý prvky mapy,
    // 'N' jakože nic, 'B' jakože blok, 'D' jakože duch
    // 'Q' jako burkinátor (vono se to prej správně piše ňák s kvé jakože 'burqa' nebo jak)
    enum herniVec {N,D,B,Q};

    // pole jednotlivejch segmentů, ze kterejch budeme skládat tu mapu
    // přidáme si tam ňáký burkinátory
    
    int seg1 [] = {
        0,0,0,0,0,
        0,0,0,0,0,
        0,0,0,0,0,
        0,0,0,0,0,
        B,B,B,B,B,
    };
    int seg2 [] =

    {
        0,0,0,0,0,
        0,0,0,0,0,
        0,0,B,0,0,
        0,B,B,B,0,
        B,B,B,B,B
    };

    int seg3 [] =

    {
        0,0,0,0,0, 0,0,0,0,0,
        0,0,0,0,0, 0,0,0,0,0,
        0,0,0,0,0, 0,0,0,0,0,
        0,B,0,D,0, 0,D,0,B,0,
        B,B,B,B,B, B,B,B,B,B,
    };
    int seg4 [] =

    {

        0,0,0,0,0,
        B,0,0,0,B,
    };
    int seg5 [] = {

        0,0,0,0,0,
        B,0,Q,0,B,
    };
    int seg6 [] = {

        0,0,0,0,0,0,0,0,0,0,0,0,0,B,0,
        0,0,0,0,B,0,0,0,B,0,0,0,0,B,0,
        0,0,0,0,B,0,0,0,B,0,0,B,0,B,0,
        0,0,B,0,B,0,0,0,B,0,0,B,0,B,0,
        B,Q,B,Q,B,Q,0,Q,B,Q,0,B,Q,B,B,
    };
    int seg7 [] = {

        0,0,0,0,0,0,0,0,0,B,0,0,0,0,0,
        0,0,0,0,0,0,0,0,B,B,0,0,0,0,0,
        0,0,0,0,0,0,0,B,B,B,0,0,0,0,0,
        0,0,0,0,0,0,B,B,B,B,0,0,0,0,0,
        0,0,0,0,0,B,B,B,B,B,0,0,0,0,0,
        0,0,0,0,B,B,B,B,B,B,0,0,0,0,0,
        0,0,0,B,B,B,B,B,B,B,0,0,0,0,0,
        0,0,B,B,B,B,B,B,B,B,0,0,0,0,0,
        0,B,B,B,B,B,B,B,B,B,0,0,0,0,0,
        B,B,B,B,B,B,B,B,B,B,Q,Q,Q,Q,B,
    };
    int seg8 [] =

    {
        0,0,0,0,0, 0,0,0,0,0,
        0,0,0,0,0, 0,0,0,0,0,
        0,0,0,D,D, D,D,0,0,0,
        0,0,B,B,B, B,B,B,0,0,
        B,B,B,B,B, B,B,B,B,B,
    };
    int seg9 [] =

    {
        0,0,0,0,0,
        0,0,0,0,0,
        0,0,D,0,0,
        0,B,B,B,0,
        B,B,B,B,B
    };
    int seg10 [] = {
        0,0,0,0,0,
        0,0,0,0,0,
        0,0,0,B,0,
        0,B,0,B,0,
        B,B,B,B,B
    };
    int seg11 [] =

    {
        0,0,0,0,0, 0,0,0,0,0,
        0,0,0,0,0, 0,0,0,0,0,
        0,0,B,0,0, 0,0,0,0,0,
        0,B,B,D,0, D,0,D,0,B,
        B,B,B,B,B, B,B,B,B,B,
    };


    SegmentMapy segmenty [] = {
        ( SegmentMapy ) {5,5,seg1},
        ( SegmentMapy ) {5,5,seg2},
        ( SegmentMapy ) {10,5,seg3},
        ( SegmentMapy ) {5,2,seg4},
        ( SegmentMapy ) {5,2,seg5},
        ( SegmentMapy ) {15,5,seg6},
        ( SegmentMapy ) {15,10,seg7},
        ( SegmentMapy ) {10,5,seg8},
        ( SegmentMapy ) {5,5,seg9},
        ( SegmentMapy ) {5,5,seg10},
        ( SegmentMapy ) {10,5,seg11},
    };

    // počet těch segmentů ze kterejch budem vybírat
    const size_t segmentu = sizeof ( segmenty ) /sizeof ( SegmentMapy );

    // alokujem si bloky dlaždicový mapy
    int ** bloky = calloc ( MAPA_MAX_VYSKA, sizeof ( int * ) * MAPA_MAX_VYSKA );
    for ( size_t i=0; i<MAPA_MAX_VYSKA; i++ ) {
        bloky[i] = calloc ( sirka,sizeof ( int ) );
    }

    // prvních a posledních deset sloupečků herní mapy bude placka,
    for ( size_t i=0; i<10; i++ ) {
        bloky[MAPA_MAX_VYSKA-1][i]=GetRandomValue ( 0,6 ) + 1; //vybíráme náhodnou texturu
        bloky[MAPA_MAX_VYSKA-1][ sirka - i - 1 ]=GetRandomValue ( 0,6 ) + 1;
    }

    int zbyva_delka = sirka - 10;
    
    size_t duchu = 0;
    
    // podobně jako sme si počitali normální duchy, si budem počitat burkinátory
    size_t burkinatoru = 0;
    
    while ( zbyva_delka >= 15 ) {
        
        // vyberem si náhodnej segment
        int index = GetRandomValue ( 0,segmentu-1 );
        
        int vyska_segmentu = segmenty[index].vyska;
        int sirka_segmentu = segmenty[index].sirka;

        if ( sirka_segmentu > zbyva_delka -10 ) {
            continue;
        }

        // noa teďko si projdem celý pole toho náhodně vybranýho segmentu....
        for ( size_t segment_y = 0; segment_y < vyska_segmentu; segment_y++ ) {
            for ( size_t segment_x = 0; segment_x < sirka_segmentu; segment_x++ ) {
                int hodnota = segmenty[index].pole[segment_x + segment_y * sirka_segmentu];

                // ....a podle toho na jakou hodnotu sme tam narazili se budem chovat
                switch ( hodnota ) {
                case B:
                    // vyrobíme náhodnej blok mapy
                    // zarovnáváme to k dolnímu vokraji mapy
                    bloky[segment_y + MAPA_MAX_VYSKA - vyska_segmentu][segment_x + ( sirka - zbyva_delka )] = GetRandomValue ( 0,6 ) + 1;
                    break;
                case D:
                    //vyrobíme na tý pozici ducha
                    {
                        Vector2 pozice = {
                            .x = ( segment_x + ( sirka - zbyva_delka ) ) * BLOK_SIRKA,
                            .y = ( segment_y + MAPA_MAX_VYSKA - vyska_segmentu ) * BLOK_VYSKA - 161,
                        };
                        Duch * duch = vygenerovatDucha ( pozice );
                        duchove[duchu++] = duch;
                    }
                    break;
                case Q:
                    //vyrobíme burkinátora
                    {
                        Vector2 pozice = {
                            .x = ( segment_x + ( sirka - zbyva_delka ) ) * BLOK_SIRKA,
                            .y = ( segment_y + MAPA_MAX_VYSKA - vyska_segmentu ) * BLOK_VYSKA + BLOK_VYSKA*3,
                        };
                        Duch * duch = vygenerovatBurkinatora ( pozice );
                        burkinatori[burkinatoru++] = duch;
                    }
                    break;
                default:
                    break;
                };

            }

        }

        zbyva_delka-=sirka_segmentu;
    }
    
    duchove = realloc ( duchove, sizeof ( Duch * ) * duchu );
    // realokujem burkinátory
    burkinatori = realloc ( burkinatori, sizeof ( Duch * ) * burkinatoru );

    // nacpem duchy do tý struktury levelu
    lvl->pocet_duchu = duchu;
    lvl->duchove = duchove;
    
    // napcem tam i burkinátory
    lvl->pocet_burkinatoru = burkinatoru;
    lvl->burkinatori = burkinatori;


    // strčíme bloky do mapy
    mapa->bloky = bloky;
    mapa->sirka = sirka;
    mapa->vyska = MAPA_MAX_VYSKA;
    mapa->textura = texturaTiledMapy;
    
    // a mapu strčíme do levelu
    lvl->dlazdicova_mapa = mapa;

    return lvl;
}

void freeLevel ( Level * lvl )
{
    for ( size_t i=0; i<lvl->pocet_duchu; i++ ) {
        freeDucha ( lvl->duchove[i] );
    }
    free ( lvl->duchove );
    
    // musíme uvolnit i burkinátory
    for ( size_t i=0; i<lvl->pocet_burkinatoru; i++ ) {
        freeDucha ( lvl->burkinatori[i] );
    }
    free ( lvl->burkinatori );
    
    for ( size_t i=0; i<MAPA_MAX_VYSKA; i++ ) {
        free ( lvl->dlazdicova_mapa->bloky[i] );
    }
    free ( lvl->dlazdicova_mapa->bloky );
    free ( lvl->dlazdicova_mapa );
    
    free ( lvl );
}

void aktualizovatLevel ( Level * lvl, David * david, float dt )
{
    Strela * strely = david->strely;
    
    for ( size_t i = 0; i < lvl->pocet_duchu; i++ ) {
        
        if(fabsf ( david->pozice.x - lvl->duchove[i]->okraje.x ) > NUTNA_VZDALENOST)
            continue;
        
        if ( ! lvl->duchove[i]->chcipe )

            for ( size_t j = 0; j < POCET_STREL_DAVIDA_MAX; j++ ) {
                Strela * s = strely + j;
                if ( s->aktivni ) {
                    if ( CheckCollisionPointRec ( s->pozice, lvl->duchove[i]->hitbox ) ) {
                        
                        s->aktivni = false;
                        lvl->duchove[i]->hp--;
                        PlaySound ( zvuk_zasah );
                    }
                }
            }

        aktualizovatDucha ( lvl->duchove[i], lvl->dlazdicova_mapa, dt );
        
        // pohlídáme si kolizi Davida s duchem, pokud se srazej tak duch Davida zraní
        // kolizi zistíme raylibí funkcí 'CheckCollisionRecs' do který nacpem hitboxy vobou herních entit
        if ( !lvl->duchove[i]->chcipe && CheckCollisionRecs ( lvl->duchove[i]->hitbox, david->hitbox ) ) {
            if ( david->zranitelny ) {
                
                // zahrajem zvuk kontaktu
                // (vlastně nvm jestli to má bejt jakože zvuk co vydává duch nebo david :D )
                PlaySound ( zvuk_kontakt );
                
                // vodečtem davidoj život
                david->zivoty--;
                
                // nastavíme davidoj blikací čas dočasný nezranitelnosti..
                david->blikaci_cas = BLIKACI_CAS_DAVIDA;
                
                // ..a zapnem mu tu nezranitelnost
                david->zranitelny = false;
                
                // pokud má ňákou vertikální rychlost směrem nahoru k hornímu vokraji vobrazkovky,
                // tak mu ji snižime na nulu. Vono to vytváří takovej psychochologickej efekt jakože
                // hráče ty duchové chytaj a bráněj mu v pohybu :O ;D
                if ( david->vertikalni_rychlost < 0.0f ) {
                    david->vertikalni_rychlost = 0.0f;
                }

            }
        }

        for ( size_t j=0; j<ZASOBNIK_STREL_DUCHA; j++ ) {
            Strela * strela_ducha = &lvl->duchove[i]->strely[j];
            if ( strela_ducha->aktivni ) {
                aktualizovatStrelu ( strela_ducha,lvl->dlazdicova_mapa, dt );
                
                // podobně jako sme hlídali zásah hitboxu ducha davidovou střelou,
                // tak budeme klídat zásah davida střelou ducha
                if ( CheckCollisionPointRec ( strela_ducha->pozice, david->hitbox ) ) {
                    strela_ducha->aktivni = false;

                    if ( david->zranitelny ) {
                        // v poctatě to samý jako při kontaktu
                        PlaySound ( zvuk_kontakt );
                        david->zivoty--;
                        david->blikaci_cas = BLIKACI_CAS_DAVIDA;
                        david->zranitelny = false;
                    }
                }
            }
        }
    }
    
    // vicemeně skoro uplně stejně si sem přidáme aktualizaci burkinátorů, jako sme napsali aktualizaci vobyč duchů
    for ( size_t i =0; i<lvl->pocet_burkinatoru; i++ ) {

        if ( fabsf ( david->pozice.x - lvl->burkinatori[i]->okraje.x ) > NUTNA_VZDALENOST )
            continue;
        
        if ( ! lvl->burkinatori[i]->chcipe )
            for ( size_t j = 0; j < POCET_STREL_DAVIDA_MAX; j++ ) {
                Strela * s = strely + j;
                if ( s->aktivni ) {
                    if ( CheckCollisionPointRec ( s->pozice, lvl->burkinatori[i]->hitbox ) ) {
                        s->aktivni = false;
                        lvl->burkinatori[i]->hp--;
                        PlaySound ( zvuk_zasah );
                    }
                }
                
            }
            
            // zkusíme kolizi s davidem
            if ( david->zranitelny && CheckCollisionRecs ( lvl->burkinatori[i]->hitbox, david->hitbox ) ) {
                PlaySound ( zvuk_kontakt );
                david->zivoty--;
                david->blikaci_cas = BLIKACI_CAS_DAVIDA;
                if ( david->vertikalni_rychlost < 0.0f ) {
                    david->vertikalni_rychlost = 0.0f;
                }

            }

        
        aktualizovatBurkinatora ( lvl->burkinatori[i], david->pozice.x, dt );
        
    }
    
}

// vykreslíme level
void vykreslitLevel ( Level * lvl, Camera2D * kamera )
{
    float min_x = kamera->target.x - GetRenderWidth() / 2.0f / kamera->zoom;
    float max_x = kamera->target.x + GetRenderWidth() / 2.0f / kamera->zoom;
    vykreslitMapu ( lvl->dlazdicova_mapa,min_x,max_x );

    // vykreslíme všecky viditelný duchy a všecky střely duchů
    for ( size_t i = 0; i < lvl->pocet_duchu; i++ ) {
        if ( fabsf ( kamera->target.x - lvl->duchove[i]->okraje.x ) < NUTNA_VZDALENOST ) {
            vykreslitDucha ( lvl->duchove[i] );
        }

        for ( size_t j =0; j<ZASOBNIK_STREL_DUCHA; j++ ) {
            vykreslitStrelu ( lvl->duchove[i]->strely + j );
        }
    }
    

    // vykreslíme burkinátory
    for ( size_t i = 0; i < lvl->pocet_burkinatoru; i++ ) {
        if ( fabsf ( kamera->target.x - lvl->burkinatori[i]->okraje.x ) < NUTNA_VZDALENOST ) {
            vykreslitDucha ( lvl->burkinatori[i] );
        }
    }
    
}

#endif

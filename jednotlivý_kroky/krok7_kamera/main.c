// naimportujem si knihovny
#include <math.h>
#include <stdlib.h>
#include <raylib.h>
#include <time.h>

// Pokuď vodkomentujeme, tak to zkompiluje preprocesorovou podmínkou vypnutý věci
// napřiklad se kolem některejch herních voběktů budou vykreslovat okraje
#define DEBUG

// naimportujem si vlastní hlavičky
#include "animace.h"
#include "david.h"

//makra na zišťování minimální a maximální hodnoty
#ifndef MAX
#define MAX(a, b) ((a)>(b)? (a) : (b))
#define MIN(a, b) ((a)<(b)? (a) : (b))
#endif

//šířka a výška vokna
#define HERNI_SIRKA 1280
#define HERNI_VYSKA 960

//kolik kostek bude dlouhá naše herní mapa
#define DELKA_LEVELU_BLOKU 50

// textury
Texture2D textura_mesic;
Texture2D textura_david_spritesheet;
Texture2D textura_kameny;

// zvuky
Sound zvuk_kroku;
Sound zvuk_skoku;

int main ( void )
{

    SetConfigFlags ( FLAG_WINDOW_RESIZABLE | FLAG_VSYNC_HINT );

    InitWindow ( HERNI_SIRKA, HERNI_VYSKA, "David a duchove" );

    InitAudioDevice();

    SetTargetFPS ( 60 );

    // načtem soubory textur
    textura_david_spritesheet = LoadTexture ( "assets/david.png" );
    textura_mesic = LoadTexture ( "assets/moon.png" );
    textura_kameny = LoadTexture ( "assets/kameny.png" );
    
    // načtem zvuky
    zvuk_kroku = LoadSound ( "assets/kroky.wav" );
    zvuk_skoku = LoadSound ( "assets/skok.wav" );
    
    // inicializujeme si strukturu kamery
    // (vlastně kromě rotace nemusíme vyplňovat atributy
    // vono si je to bude aktuallizovat každým loopem)
    Camera2D kamera = {
        
        //posun 'středu' kamery, posunem na střed vobrazovky
        .offset = ( Vector2 ) { GetRenderWidth() / 2.0f, GetRenderHeight() / 2.0f },

        // souřadnice cíle, na co jakože kamera kouká
        .target = ( Vector2 ) {0,0},

        // uhel náklonu kamery ve stupních
        .rotation = 0.0f,

        //přiblížení
        .zoom = 1.0f
    };
    
    // animace běhu
    Animace david_beh = {
        .trvani_framu = 1.0f/30.0f, .relativni_cas=0.0f, .index = 0,
        .jojo = true, .reverzne = false, .zrcadlit = false, .loopovat = true,
        .textura = textura_david_spritesheet, .framy = david_framy_behu, .pocet_framu = sizeof ( david_framy_behu ) /sizeof ( Rectangle )
    };
    // animace idle, jakože když se fláká a nic nedělá. Je to takový pérování nohama na místě
    Animace david_idle = {
        .trvani_framu = 1.0f/15.0f, .relativni_cas=0.0f, .index = 0,
        .jojo = true, .reverzne = false, .zrcadlit = false, .loopovat = true,
        .textura = textura_david_spritesheet, .framy = david_framy_idle, .pocet_framu = sizeof ( david_framy_idle ) /sizeof ( Rectangle )
    };

    Animace david_sed = {
        .trvani_framu = 1.0f/30.0f, .relativni_cas=0.0f, .index = 0,
        .jojo = true, .reverzne = false, .zrcadlit = false, .loopovat = false,
        .textura = textura_david_spritesheet, .framy = david_framy_sed, .pocet_framu = sizeof ( david_framy_sed ) /sizeof ( Rectangle )
    };

    // animace skoku, david tam vicemeně jenom máchá nožičkama ve vzduchu
    Animace david_skok = {
        .trvani_framu = 1.0f/10.0f, .relativni_cas=0.0f, .index = 0,
        .jojo = true, .reverzne = false, .zrcadlit = false, .loopovat = true,
        .textura = textura_david_spritesheet, .framy = david_framy_skoku, .pocet_framu = sizeof ( david_framy_skoku ) /sizeof ( Rectangle )
    };
    
    const int vejska_mapy = 10;
    const int sirka_mapy = DELKA_LEVELU_BLOKU;
    int ** bloky = calloc ( vejska_mapy, sizeof ( int * ) * vejska_mapy );
    for ( size_t i=0; i<vejska_mapy; i++ ) {
        bloky[i] = calloc ( sirka_mapy,sizeof ( int ) );
    }

    // a spodní vrstvu mapy vyplníme plošinou
    for ( size_t i=0; i<sirka_mapy; i++ )
        bloky[vejska_mapy - 1][i] = 1;
    
    // přidáme si do cesty pár bloků, by sme viděli jak se David zasekává vo překážky
    bloky[vejska_mapy - 2][7] = 1;
    bloky[vejska_mapy - 2][10] = 1;
    bloky[vejska_mapy - 3][10] = 1;
    
    // vyrobíme si herní mapu
    Mapa mapa = {
        
        .textura = textura_kameny,
        .sirka = sirka_mapy,
        .vyska = vejska_mapy,
        .bloky = bloky
        
    };
    
    
    // kdyžuž máme vyrobený animace, tak si mužeme vyrobit Davida
    David david = {
                    
        .animace_beh = david_beh,
        .animace_idle = david_idle,
        .animace_sed = david_sed,
        .animace_skok = david_skok,
         
        .aktualni_animace = NULL,

        // necháme ho na herní mapu spadnou z vejšky
        .pozice = {0,0},
                    

        .smer = 1,
        
        // strčíme mapu do Davida
        .mapa = &mapa,
        
        // nastavíme vertikální rychost na nulu a to zda skáče na true
        // (ikdyž to zda skáče by se asi jako stejně přeplo samo hnedka :D)
        .vertikalni_rychlost = 0.0f,
        .zdaSkace = true,

    };

    // nastavíme ukazatel aktuální animace na animaci 'idle'
    david.aktualni_animace = &david.animace_idle;
                
    // podle aktuální pozice nastavíme okraje oběktu
    // (jsou vo trošku menčí než vokraje voblasti framu animace)
    david.okraje = ( Rectangle ) {
        david.pozice.x + 45, david.pozice.y +8, DAVID_F_SIRKA -90, DAVID_F_VYSKA - 10
    };

    
    while ( !WindowShouldClose() ) {

        float dt = GetFrameTime();
        
        // spočitáme si přiblížení naší kamery
        // uděláme to tak, že si spočitáme poměr skutečný šířky obrazovky s naší 'virtuální' požadovanou,
        // to samý uděláme se skutečnou a požadovanou vejškou, noa vybereme tu menší hodnotu
        // (jak se to chová si mužeme vyzkoušet behem hry, když budeme ruzně měnit velikost vokna)
        const float priblizeni = MIN ( ( float ) GetRenderWidth() / HERNI_SIRKA, ( float ) GetRenderHeight() / HERNI_VYSKA );

        // nastavíme atribut 'zoom' tou naší spočitanou hodnotou
        kamera.zoom = priblizeni;
        //nastavíme posun kamery na velikost půlky vobrazovky
        kamera.offset = ( Vector2 ) {
            GetScreenWidth() /2, GetScreenHeight() /2
        };
        
        //aktualizujem Davida
        aktualizovatDavida(&david, dt);
        
        // nastavíme cíl kamery na střed davida
        kamera.target = ( Vector2 ) {
            david.okraje.x + david.okraje.width / 2.0f, david.okraje.y + david.okraje.height / 2.0f
        };
        
        //určíme si meze, ve kterejch se kamera muže pohybovat
        
        // minimální iksová souřadnice je polovina šířky vobrazovky (takže když se david bude přibližovat
        // levýmu vokraji mapy, přestanem ho kamerou sledovat by sme nevykoukli mimo voblast tý dlaždicový mapy),
        // maximální dýlka levelu v počtu bloků minus zase půlka vobrazovky (ze stejnejch duvodů jen pro druhej vokraj)
        // maximální ypsilon (roste nám směrem zezhora dolu, horní vokraj obrazovky má nulu) je
        // půlka vejšky vobrazovky + půlka vejšky davida + vejška pár bloků navíc, by hráč nemusel mit voči moc sklopený k dolnímu vokraji
        // vobrazovky (zpomináte si jak sme si vykreslovali v souboru 'mapa.h' těch pár kostek navíc?? teďko se šiknou. Posun kamery mi
        // přišel nejhežčí dokonce vo vejšku pěti bloků, nejenom tří jak to tam máme v hlavičce 'mapa.h' napsaný. Necháme soubor mapa.h jak je,
        // už mam vymyšlený jak to celý zavonačíme by spodek tý mapy byl pěknej a nemuseli sme si rendrovat zbytečně moc kostek :O ;D)
        
        // (hodnoty transformujem do rozměrů kamerózního světa podělením ňákýho toho rozměru nebo
        // souřadnice spočitanou hodnotou proměný 'přiblížení')
        
        const float kamera_target_min_x = GetRenderWidth() / 2.0f / priblizeni;
        const float kamera_target_max_x = DELKA_LEVELU_BLOKU * BLOK_SIRKA - GetRenderWidth() /2/priblizeni;
        const float kamera_target_max_y = GetRenderHeight() / 2.0f / priblizeni - DAVID_F_VYSKA + BLOK_VYSKA*5;

        // pohlídáme si ty minimální a maximální možný hodnoty
        kamera.target.x = MAX ( kamera.target.x, kamera_target_min_x );
        kamera.target.x = MIN ( kamera.target.x, kamera_target_max_x );
        kamera.target.y = MIN ( kamera.target.y, kamera_target_max_y );

        // zapnem vykreslování
        BeginDrawing();

        // vykreslíme ten gradient
        DrawRectangleGradientV ( 0,0,GetScreenWidth(),GetScreenHeight(),DARKBLUE,BLACK );
        
        // aktivujem transformování tou naší kamerou
        // takže jakoby vykreslujem to, co kamera vidí
        BeginMode2D ( kamera );

        // vykreslíme mapu
        vykreslitMapu(&mapa,0.0f,9999.0f);
        
        // vykreslíme davida
        vykreslitDavida(&david);
        
        // a teďko příde to slíbený zavonačení :D :D 
        // nakreslíme na spodku mapy čtverec s černým gradientem, takže to bude vypadat že se nám spodek mapy
        // jakože noří do ňákýho stínu nebo čeho
        DrawRectangleGradientV ( kamera.target.x - GetRenderWidth() / 2 / priblizeni,BLOK_VYSKA*10, GetRenderWidth()/priblizeni,BLOK_VYSKA*3,BLANK, BLACK );
        // a pod tim všecko vyčerníme černým vodelnikem, kterej hezky navazuje na ten náš černej gradient
        DrawRectangle ( kamera.target.x - GetRenderWidth() /2/priblizeni,BLOK_VYSKA*13,GetRenderWidth() /priblizeni,GetRenderHeight()/priblizeni,BLACK );
        
        // vypneme kameru
        EndMode2D();

        // a skončíme s vykreslováním by se naše scéna poslala na monitor
        EndDrawing();
    }

    CloseWindow();
    
    // musíme uklidit to alokovaný pole 'bloky'
    for ( size_t i=0; i<vejska_mapy; i++ ) {
        free ( bloky[i] );
    }
    free ( bloky );

    //uklidíme textury
    UnloadTexture ( textura_mesic );
    UnloadTexture ( textura_david_spritesheet );
    UnloadTexture ( textura_kameny );

    // vypnem audio zařízení
    CloseAudioDevice();
    
    // a taky uvolníme zvuky
    UnloadSound ( zvuk_kroku );
    UnloadSound ( zvuk_skoku );

    return 0;
}

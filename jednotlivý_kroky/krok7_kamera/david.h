#ifndef _DAVID_A_DUCHOVE_DAVID_H_
#define _DAVID_A_DUCHOVE_DAVID_H_

// naimportujem si animaci
// (s ní se nám současně natáhne raylib.h)
#include "animace.h"
#include "mapa.h"

// kouzelným slovíčkem 'extern' https://www.geeksforgeeks.org/understanding-extern-keyword-in-c/
// mužeme 'sdílet' se souborem 'main.c' v něm deklarovaný proměný, teďko na ukázku zatim jenom
// zvuk chůze, de ale všecko :O ;D 
extern Sound zvuk_kroku;
extern Sound zvuk_skoku;

// přestěhujem si sem z 'main.c' framy animace by to tam zbytečně nezabiralo misto ve zdrojáčku 
#define DAVID_F_SIRKA 150.0f
#define DAVID_F_VYSKA 240.0f

Rectangle david_framy_behu[] = {
    {DAVID_F_SIRKA*0,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*1,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*2,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*3,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*4,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*5,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},

    {DAVID_F_SIRKA*0,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*1,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*2,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA}
};

Rectangle david_framy_skoku[] = {

    {DAVID_F_SIRKA*5,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},

    {DAVID_F_SIRKA*0,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*1,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*2,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA}


};

Rectangle david_framy_idle[] = {
    {DAVID_F_SIRKA*4,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*5,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*0,DAVID_F_VYSKA*3,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*1,DAVID_F_VYSKA*3,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*2,DAVID_F_VYSKA*3,DAVID_F_SIRKA,DAVID_F_VYSKA},
};

Rectangle david_framy_sed[] = {
    {DAVID_F_SIRKA*3,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*4,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*5,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*0,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*1,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*2,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*3,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
};

#define RYCHLOST_CHUZE_DAVIDA 350.0f

//máme tady dvě další hodnoty, rychlost skoku a gravitaci
#define RYCHLOST_SKOKU_DAVIDA -15.0f
#define GRAVITACE_DAVIDA 40.0f

typedef struct David {
    
    // jednotlivý animace davida
    Animace animace_beh;
    Animace animace_idle;
    Animace animace_sed;
    Animace animace_skok;

    Animace * aktualni_animace;


    Vector2 pozice;
    
    Rectangle okraje;
    
    int smer;
    
    Mapa * mapa;
    
    // david bude mit dva nový atributy:
    // boolean jestli skáče a vertikální rychlost kterou se jakoby pohybuje
    bool zdaSkace;
    float vertikalni_rychlost;

} David;


void aktualizovatDavida ( David * david, float dt )
{

    // čudlikem šipky nahroru jsme zatim jenom zvedali Davida ze země, teďko mu přidáme eště jednu funkci a to
    // že jim david bude skákat (pokud nesedí a nesedá si nebo nevstává)
    if ( IsKeyDown ( KEY_UP ) ) {

        if ( david->aktualni_animace == &david->animace_sed ) {
            david->aktualni_animace->pauznuta = false;
            david->aktualni_animace->reverzne=true;

            if ( david->aktualni_animace->index == 0 ) {
                david->aktualni_animace = & david->animace_idle;
            }
        // pokud je zmáčknutá šipka nahoru a animace není animace sedání,
        // tak se kouknem jestli David už neskáče, a jestli ne, tak zapnem zvuk skoku,
        // nastavíme atrimut skákání, resetujem skákací animaci na index 0 a nastavíme vertikální rychlost na max
        // (aktuální animaci přepnem kousek dál v upraveným kódu)
        } else if ( !david->zdaSkace ) {
            PlaySound ( zvuk_skoku );
            david->zdaSkace = true;
            david->animace_skok.index = 0;
            david->vertikalni_rychlost = RYCHLOST_SKOKU_DAVIDA;
        }
    }


    
    if ( IsKeyDown ( KEY_LEFT ) && david->pozice.x > 0 && david->aktualni_animace != &david->animace_sed ) {
        
        david->smer = -1;
        
        Rectangle prepozice = david->okraje;
        prepozice.x -= RYCHLOST_CHUZE_DAVIDA * dt;

        if ( kolizeRectSeBlokemMapy ( prepozice, david->mapa ) ) {
            david->aktualni_animace = & david->animace_idle;
        } else {

            david->okraje.x = prepozice.x;
            david->pozice.x = david->okraje.x - 45;
            david->aktualni_animace = &david->animace_beh;
            }

        }
    
    else if ( IsKeyDown ( KEY_RIGHT ) && david->pozice.x < david->mapa->sirka*BLOK_SIRKA - DAVID_F_SIRKA && david->aktualni_animace != &david->animace_sed ) {

        david->smer = 1;
        Rectangle prepozice = david->okraje;
        prepozice.x += RYCHLOST_CHUZE_DAVIDA * dt;
        
        if ( kolizeRectSeBlokemMapy ( prepozice, david->mapa ) ) {
            david->aktualni_animace = & david->animace_idle;
        } else {
            david->okraje.x = prepozice.x;
            david->pozice.x = david->okraje.x - 45;
            david->aktualni_animace = & david->animace_beh;
            } 
    }
    // jestli je máčkutej na klávesnici čudlik šipky dolu, tak začnem přehrávat animaci sednutí
    // si na zadek
    else if ( IsKeyDown ( KEY_DOWN ) ) {

        david->aktualni_animace = & david->animace_sed;
        david->aktualni_animace->reverzne = false;
        david->aktualni_animace->pauznuta = false;
        }
        
    else if ( david->aktualni_animace != &david->animace_sed ) {
        david->aktualni_animace = & david->animace_idle;
        }
        
    // kouknem jestli má david pod nohama pevnou zem
    // vyrobíme si kopii okrajů/boundingboxu a postrčíme ji o 5 pixelů dolu
    // pokud okraje/boundingbox nebude mit kolizi s pevnejma kostkama herní mapy,
    // tak davidoj zapnem skákací atribut, ale nedáme mu žádnou rychlost,
    // tzn. necháme ho padat volným pádem dolu s počateční vertikální rychlostí nula
    // (rychlost nastavujem na nulu dycky když se David nohama dotkne země takže by už rychlost
    // na nulu měla bejt nastavená z jinýho kousku kodu)
    Rectangle prepozice = david->okraje;
    prepozice.y += 5;
    if ( ! kolizeRectSeBlokemMapy ( prepozice, david->mapa ) ) {
        david->zdaSkace = true;
    }

    // kouknem jestli David má nastavenej atribut skákání na true. Pokud jakože jo,
    // tak mu budem aktualizovat polohu podle tý jeho vertikální rychlosti.
    if ( david->zdaSkace ) {

        // aktualizujem si Davidovu vertikální rychlost
        // (pomalinku ji táhnem gravitací směrem dolu k zemi)
        david->vertikalni_rychlost += dt * GRAVITACE_DAVIDA;
        
        // uplně stejně jako při chození do stran si spočitáme jeho prepozici, jakože jeho
        // vokraje posunutý vo vertikální rychlost směrem dolu
        Rectangle prepozice = david->okraje;
        prepozice.y += david->vertikalni_rychlost;

        // noa kouknem, jestli by posunem na tu prepozici došlo ke kontaktu s mapou
        // (posouváme jenom po ose ypsilon, bočniho narazu do kostek mapy se jakoby nemusíme bát)
        if ( kolizeRectSeBlokemMapy ( prepozice, david->mapa ) ) {
            
            // pokud by mělo příštím krokem dojit k tomu kontaktu Davida s mapou, tak si to budem počitat jako že
            // nám David už spadnul na zem. Takže mu vynulujem vertikální rychlost a nastavíme atribut skákání 'zdaSkace' na false
            david->vertikalni_rychlost = 0.0f;
            david->zdaSkace = false;

            // jenže David nám furt visí ve vzduchu, a když ho posunem  vo vetikální rychlost, tak ho zase zanoříme do dlaždicový mapy :O :O
            // takže si spočitáme vertikální polohu spodního vokraje Davida (okraje.y + vejška), přepočitáme ho na souřadnici dlažidový mapy
            // ( ceil(y/BLOK_VYSKA) a tu zpátky pronásobíme vejškou bloku, takže sme získali souřadnici zarovnanou na kostku mapy
            // teďko vod toho eště musíme vodečíst vejšku Davida, páč Davidova poloha je jeho levej horní roh ( A eště vodečtem ňákou mrňavou
            // hodnotu, páč by byl jinak těsně zanořenej ve hraně mapy)
            david->okraje.y =  ceil ( ( david->okraje.y + david->okraje.height  ) /BLOK_VYSKA ) * BLOK_VYSKA - david->okraje.height - 1;

            // pokud je díky mačkutí šipky doprava nebo doleva náhodou nastavená jako aktuální animace běhání,
            // tak využijem toho že sme animaci skoku vyrobili z podmnožiny framů animace běhu a nastavíme běhací animaci jako
            // právě teď zobrazenej snímek (index) vodpovídající snimek animace skoku, současně nastavíme stejnej směr přehrávání, jakože
            // jestli přehráváme reverzně nebo ne. Tim si myslim že dosáhneme trošičku lepší navaznosti animací na sebe a budou jakoby mezi sebou
            // krásně plynule přecházet. Uvidime :D
            if ( david->aktualni_animace == &david->animace_beh ) {
                david->aktualni_animace->index = david->animace_skok.index +5;
                david->aktualni_animace->reverzne = david->animace_skok.reverzne;
            }

        } else {
            
            // pokud se padací/skákací prepozice neprotne s dlaždicovou mapou, tzn. David eště nedopadně na zem,
            // tak mu nastavíme animaci na animaci skákání (pro případ kdyby ji hráč mačkáním šipek přepnul na běch)
            david->aktualni_animace = & david->animace_skok;
            
            // a posunem okraje na spočitanou prepozici
            david->okraje.y += david->vertikalni_rychlost;

        }
        
        // aktualizujem davidovu pozici podle aktuální pozice vokrajů
        // (trošku sem tu polohu upravila, vektor 'pozice' určuje polohu vykreslování textury Davida,
        // šoupla sem ji vo pár čísel dolu, by David neplandal nohama vevzduchu nad mapou. Vypočty kolizí a elálný polohy stejně
        // věčinou ve zdrojáčku děláme pomocí toho rectanglu 'okraje', tady de spíš jakože vo tu estetiku :D)
        david->pozice.y = david->okraje.y - 3;
        

    }

    david->aktualni_animace->zrcadlit = david->smer<0;


    if ( david->aktualni_animace == &david->animace_sed ) {
        if ( david->animace_sed.pauznuta ) {
            if ( david->animace_sed.index == 0 ) {
                david->aktualni_animace = & david->animace_idle;
            }
        }

    }

    // pokud je aktualní animace běhání, tak se kouknem jestli hraje zvuk kroků noa jestli ne,
    // ho začnem přehrávat 
    else if ( david->aktualni_animace == &david->animace_beh ) {
        if ( !IsSoundPlaying ( zvuk_kroku ) ) {
            PlaySound ( zvuk_kroku );
        }
    }
    
    // pokud je aktualní animace skákání a pokud hraje zvuk kroků, tak ten zvuk vypnem
    if ( david->aktualni_animace == &david->animace_skok && IsSoundPlaying ( zvuk_kroku ) ) {
        StopSound ( zvuk_kroku );
    }

    // nakonec aktualizujem aktuální animaci :D ;D
    aktualizovatAnimaci ( david->aktualni_animace, dt );

}

// funkce na vykreslování Davida
void vykreslitDavida ( David * david )
{
    vykreslitAnimaci ( david->aktualni_animace, david->pozice, WHITE );

    // jestli je definovanej 'DEBUG', vykreslíme vokraje třeba zelenou barvičkou
#ifdef DEBUG
    DrawRectangleLines ( david->okraje.x, david->okraje.y, david->okraje.width, david->okraje.height, GREEN );
#endif
}

#endif

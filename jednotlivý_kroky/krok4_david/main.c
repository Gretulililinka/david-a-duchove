// naimportujem si knihovny
#include <math.h>
#include <stdlib.h>
#include <raylib.h>
#include <time.h>

// naimportujem si vlastní hlavičky
#include "animace.h"
#include "david.h"

//makra na zišťování minimální a maximální hodnoty
#ifndef MAX
#define MAX(a, b) ((a)>(b)? (a) : (b))
#define MIN(a, b) ((a)<(b)? (a) : (b))
#endif

//šířka a výška vokna
#define HERNI_SIRKA 1280
#define HERNI_VYSKA 960

// textury
Texture2D textura_mesic;
Texture2D textura_david_spritesheet;

// zvuky
Sound zvuk_kroku;

int main ( void )
{

    // nakonfigurujeme si raylib, že budeme chtít vytvořit okno s proměnlivou velikostí a s vertikální synchronizací
    // poznámka pod čarou, ten fígl s bitovým operátorem 'or' jakože se znakem '|' funguje tak, že každá z těch flagovejch
    // konstant má hodotu nastavenou tak, by byl v jejich bytu vobsazenej dycky jenom jeden jedinej bit. Noa když uděláme
    // to bitový or, tak se nám ty proměný zkombinujou do nový unikátní hodnoty kterou ta knihovna umí rozlišit,
    // respektive čte jednotlivý bity v bajtech :O ;D
    SetConfigFlags ( FLAG_WINDOW_RESIZABLE | FLAG_VSYNC_HINT );

    // inicializujeme vokno vo daný šířce, vejšce a s titulkem
    InitWindow ( HERNI_SIRKA, HERNI_VYSKA, "David a duchove" );

    // inicializujem audio zařízení, by sme mohli přehrávat zvuky
    InitAudioDevice();

    // nastavíme požadovanou frekvecni vykreslování
    // takle řikáme, že chceme vykreslovat šedesát snímků za sekundu (framů per sekundu)
    SetTargetFPS ( 60 );

    // načtem soubory textur
    textura_david_spritesheet = LoadTexture ( "assets/david.png" );
    textura_mesic = LoadTexture ( "assets/moon.png" );
    
    // načtem zvuky
    zvuk_kroku = LoadSound ( "assets/kroky.wav" );
    
    // vyrobíme si instance jednotlivejch animací možnejch davidovejch aktivit
    // animace běhu

    // jeden snímek animace nám bude trvat jednu třicetinu sekundy, relativní čas a index vynulujem, zapnem atribut 'jojo' by
    // se nám pak animace po normálním přehrání pouštěla pozpátku, nastavíme atributy 'reverzně' a 'zrcadlit' na false,
    // zapnem loopování, nastavíme texturu a pole jednotlivejch framů ve spritesheetu, počet framů/dylku pole spočitáme podělením velikosti
    // celýho pole velikostí jednoho prvku
    Animace david_beh = {
        .trvani_framu = 1.0f/30.0f, .relativni_cas=0.0f, .index = 0,
        .jojo = true, .reverzne = false, .zrcadlit = false, .loopovat = true,
        .textura = textura_david_spritesheet, .framy = david_framy_behu, .pocet_framu = sizeof ( david_framy_behu ) /sizeof ( Rectangle )
    };
    // animace idle, jakože když se fláká a nic nedělá. Je to takový pérování nohama na místě
    Animace david_idle = {
        .trvani_framu = 1.0f/15.0f, .relativni_cas=0.0f, .index = 0,
        .jojo = true, .reverzne = false, .zrcadlit = false, .loopovat = true,
        .textura = textura_david_spritesheet, .framy = david_framy_idle, .pocet_framu = sizeof ( david_framy_idle ) /sizeof ( Rectangle )
    };
    // animace sednutí si, puvodně to měla bejt animace chcípnutí jakože si ztoho sedne na zadek :D ale pak mě napadlo že ta hra
    // by mohla bejt víc zajimavější když by se david moch přikrčovat před muslimskou střelbou k zemi
    // pozn. si všimněte že má animace vypnutej loop
    Animace david_sed = {
        .trvani_framu = 1.0f/30.0f, .relativni_cas=0.0f, .index = 0,
        .jojo = true, .reverzne = false, .zrcadlit = false, .loopovat = false,
        .textura = textura_david_spritesheet, .framy = david_framy_sed, .pocet_framu = sizeof ( david_framy_sed ) /sizeof ( Rectangle )
    };

    // animace skoku, david tam vicemeně jenom máchá nožičkama ve vzduchu
    Animace david_skok = {
        .trvani_framu = 1.0f/10.0f, .relativni_cas=0.0f, .index = 0,
        .jojo = true, .reverzne = false, .zrcadlit = false, .loopovat = true,
        .textura = textura_david_spritesheet, .framy = david_framy_skoku, .pocet_framu = sizeof ( david_framy_skoku ) /sizeof ( Rectangle )
    };
    
    
    // kdyžuž máme vyrobený animace, tak si mužeme vyrobit Davida
    David david = {
                    
        //nakopírujeme do davida jednotlivý animace
        .animace_beh = david_beh,
        .animace_idle = david_idle,
        .animace_sed = david_sed,
        .animace_skok = david_skok,
        
        // aktuální animaci nastavíme zatim na nulovou adresu, ten ukazatel vyplníme až 
        // adresou animace z instance Davida v tý právě vyráběný proměný 'david' 
        .aktualni_animace = NULL,

        //souřadnice davida
        // (je to pozice levýho horního okraje 'obrázku')
        .pozice = {GetRenderWidth()/2 - DAVID_F_SIRKA/2,200},
                    
        //směr kterým kouká (jednička je zleva doprava, -1 je zprava doleva)
        .smer = 1,

    };

    // nastavíme ukazatel aktuální animace na animaci 'idle'
    david.aktualni_animace = &david.animace_idle;
                
    // podle aktuální pozice nastavíme okraje oběktu
    // (jsou vo trošku menčí než vokraje voblasti framu animace)
    david.okraje = ( Rectangle ) {
        david.pozice.x + 45, david.pozice.y +8, DAVID_F_SIRKA -90, DAVID_F_VYSKA - 10
    };

    
    while ( !WindowShouldClose() ) {

        float dt = GetFrameTime();
        
        //aktualizujem Davida
        aktualizovatDavida(&david, dt);

        BeginDrawing();

        DrawRectangleGradientV ( 0,0,GetScreenWidth(),GetScreenHeight(),DARKBLUE,BLACK );

        //vykreslíme davida
        vykreslitDavida(&david);

        EndDrawing();
    }

    // jestli se přerušil tamten náš hlavní while cyklus, tak zavřem okno, uklidíme po sobě a skončíme
    // běh programu vrácením návratový hodnoty
    CloseWindow();

    //uklidíme textury
    UnloadTexture ( textura_mesic );
    UnloadTexture ( textura_david_spritesheet );

    // vypnem audio zařízení
    CloseAudioDevice();
    
    // a taky uvolníme zvuky
    UnloadSound ( zvuk_kroku );

    return 0;
}

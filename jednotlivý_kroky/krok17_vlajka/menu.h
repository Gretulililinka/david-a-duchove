#ifndef _DAVID_A_DUCHOVE_MENU_H_
#define _DAVID_A_DUCHOVE_MENU_H_

#include <raylib.h>
#include <stdlib.h>

// budeme potřebovat hodně vobrázků
extern Texture2D textura_duchove_spritesheet;
extern Texture2D textura_menu_titulek;
extern Texture2D textura_menu_pozadi;
extern Texture2D textura_menu_pozadi2;
extern Texture2D textura_menu_david;
extern Texture2D textura_menu_zmackni_enter;

// počet duchů vykreslenejch v menu
// duchové maj jakoby levitovat po řbitově
#define MENU_DUCHU 6

// pro každýho ducha si budem pamatovat jeho texturu....
Rectangle menu_duchove [MENU_DUCHU];

// ....ypsilonovej posun....
float menu_y_duchu [MENU_DUCHU];
// ....a ypsilonovou rychlost
float menu_rychlost_duchu [MENU_DUCHU];

// pro efekt blikání textu (a vlastně i houpání Davida) si budem počítat 
const float menu_fade_max = 100.0f;
float menu_fade = menu_fade_max;

// jestli k blikací proměný deltu přičitame nebo vodčitáme
int menu_fade_smer=-1;

void vygenerovatMenu()
{
    // nastavíme duchům nahodnou texturu, ypsilonovou polohu i rychlost
    for ( size_t i=0; i<MENU_DUCHU; i++ ) {
        menu_duchove[i]= ( Rectangle ) {
            0 + 85 * ( rand() %6 ),0 + 240 * ( rand() %2 ),85,240
        };
        menu_y_duchu[i] = ( float ) GetRandomValue ( 250,350 );
        menu_rychlost_duchu[i] = ( float ) GetRandomValue ( 100,150 );
    }
}

void aktualizovatVykreslitMenu ( float dt )
{
    // větší rozměr z šiřky a vyšky vobrazovky
    float vetsi_rozmer = MAX ( GetRenderWidth(),GetRenderHeight() );
    
    // vykreslíme ten fialovej řbitov na pozadí
    DrawTexturePro ( textura_menu_pozadi, ( Rectangle ) {
        0,0,textura_menu_pozadi.width,textura_menu_pozadi.height
    }, ( Rectangle ) {
        GetRenderWidth() /2,GetRenderHeight() /2,vetsi_rozmer, vetsi_rozmer
    }, ( Vector2 ) {
        vetsi_rozmer/2,vetsi_rozmer/2
    },0,WHITE );

    // minimálnáí a maximální vyspilonová vejška ducha
    const float min_vejska = 250.0f;
    const float max_vejska = 350.0f;

    // střed ypsilonový trajektorie ducha
    const float stred_y = ( max_vejska + min_vejska ) /2.0f;
    
    // maximální možná zdálenost kterou duch muže mit vod tamtoho ypsilonovýho středu tý trajektorie
    const float max_vzdalenost = max_vejska - stred_y;
    
    // šiřka a vejška ducha
    const float duch_sirka = GetRenderWidth() / ( MENU_DUCHU );
    const float duch_vyska = duch_sirka / ( 85.0f/240.0f );

    for ( size_t i=0; i<MENU_DUCHU; i++ ) {

        // aktualizace ypsilonovejch poloch a ryhclostí duchů
        // (je tam přidaný brždění podle zdálenosti vod středu tý jejich ypsilonový trajektorie by se jakože houpali plynule)
        
        menu_y_duchu[i] += ( menu_rychlost_duchu[i]/4 + menu_rychlost_duchu[i] * ( 1.0f - fabsf ( menu_y_duchu[i] - stred_y ) /max_vzdalenost ) ) *dt;
        if ( menu_y_duchu[i]>max_vejska ) {
            menu_y_duchu[i] = max_vejska;
            menu_rychlost_duchu[i]=-fabsf ( menu_rychlost_duchu[i] );
        } else if ( menu_y_duchu[i]<min_vejska ) {
            menu_y_duchu[i] = min_vejska;
            menu_rychlost_duchu[i]=fabsf ( menu_rychlost_duchu[i] );
        }

        DrawTexturePro (
            textura_duchove_spritesheet,
            menu_duchove[i],
            ( Rectangle ) {GetRenderWidth() / ( MENU_DUCHU ) * i,menu_y_duchu[i],duch_sirka, duch_vyska},
            ( Vector2 ) { 0,0 },0.0f, WHITE
        );
    }
    
    // vykreslíme hrobečky
    DrawTexturePro ( textura_menu_pozadi2, ( Rectangle ) {
        0,0,textura_menu_pozadi2.width,textura_menu_pozadi2.height
    }, ( Rectangle ) {
        GetRenderWidth() /2,GetRenderHeight() /2,vetsi_rozmer, vetsi_rozmer
    }, ( Vector2 ) {
        vetsi_rozmer/2,vetsi_rozmer/2
    },0,WHITE );
    
    // vykreslíme stín Davida
    // (jak stín tak i David se budou trošku houpat (měnit svuj uhel) podle tý fade proměný)
    DrawTexturePro ( textura_menu_david, ( Rectangle ) {
        0,0,textura_menu_david.width,textura_menu_david.height
    }, ( Rectangle ) {
        vetsi_rozmer/4- 40 + ( 10* ( menu_fade/menu_fade_max ) ),GetRenderHeight()+ vetsi_rozmer/8,vetsi_rozmer/2, vetsi_rozmer/2
    }, ( Vector2 ) {
        vetsi_rozmer/4,vetsi_rozmer/2
    },menu_fade/menu_fade_max * 3 - 1.5f,Fade ( BLACK,0.25f ) );
    
    // vykreslíme normálního Davida
    DrawTexturePro ( textura_menu_david, ( Rectangle ) {
        0,0,textura_menu_david.width,textura_menu_david.height
    }, ( Rectangle ) {
        vetsi_rozmer/4,GetRenderHeight()+ vetsi_rozmer/8,vetsi_rozmer/2, vetsi_rozmer/2
    }, ( Vector2 ) {
        vetsi_rozmer/4,vetsi_rozmer/2
    },menu_fade/menu_fade_max * 3 - 1.5f,WHITE );

    // vykreslíme titulek
    DrawTexture ( textura_menu_titulek,GetRenderWidth() /2 - textura_menu_titulek.width/2,0,WHITE );

    // aktualizujeme tu fadeovací proměnou
    // (asi by sme mohli nahradit sinusovkou nebo něčim a mit to hladčejší a bez tý 'směrový' proměný)
    menu_fade+=menu_fade_smer * dt * 250.0f;
    if ( menu_fade<=0.0f ) {
        menu_fade_smer = 1;
    } else if ( menu_fade>=menu_fade_max ) {
        menu_fade_smer = -1;
    }

    // vykreslíme nápis s tim mačkáním enteru
    DrawTexture ( textura_menu_zmackni_enter,GetRenderWidth() /2 - textura_menu_zmackni_enter.width/2,GetRenderHeight() - textura_menu_zmackni_enter.height,Fade ( WHITE,0.5f + 0.5f * menu_fade/menu_fade_max ) );
}

#endif

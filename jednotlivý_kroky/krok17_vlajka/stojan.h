#ifndef _DAVID_A_DUCHOVE_STOJAN_H_
#define _DAVID_A_DUCHOVE_STOJAN_H_

#include <raylib.h>
#include <stdlib.h>

extern Texture2D textura_ruzne;

// jak dlouho bude na stojanu trvat vyměna vlajky
#define PREVLAJKOVAVACI_CAS_STOJANU 2.0f

// voblasti textur stojanu a vlajek v textuře 'různé'
const Rectangle ISRA_VLAJKA_RECT = (Rectangle){2,590,160,80};
const Rectangle BUBU_VLAJKA_RECT = (Rectangle){2,674,160,80};
const Rectangle STOJAN_RECT = (Rectangle){2,2,18,497};

typedef struct Stojan
{
    Rectangle okraje;
    
    // vobdelniky popsiujicí polohu a voblast vobou vlajek
    Rectangle vlajka_puvodni, vlajka_nova;
    
    float relativni_cas;
    
    // stojan bude mit vlastně jakoby tři stavy, defaultní, kdy na něm
    // bude plandat bubu vlajka, pak ňákej přechodnej kdy se dole voběví
    // nová vlajka a začnou se vyměňovat,
    // noa pak ňákej třetí stav, kdy už ta změna jakoby dojede do konce
    // ty tři stavy popišem dvouma boolama (pokud sou voba false tak platí jakoby ten třetí stav) 
    bool prevlajkovava_se, uz_se_prevlajkoval_uplne;
    
}
Stojan;

Stojan * vygenerovatStojan(Vector2 kde)
{
    Stojan * s = calloc(1,sizeof(Stojan));
    if(!s) return NULL;
    
    s->okraje = (Rectangle){kde.x,kde.y - STOJAN_RECT.height,STOJAN_RECT.width,STOJAN_RECT.height};
    s->vlajka_puvodni = (Rectangle){kde.x + STOJAN_RECT.width/2.0f, kde.y - STOJAN_RECT.height,BUBU_VLAJKA_RECT.width,BUBU_VLAJKA_RECT.height};
    s->vlajka_nova = (Rectangle){kde.x + STOJAN_RECT.width/2.0f, kde.y - STOJAN_RECT.height,ISRA_VLAJKA_RECT.width,ISRA_VLAJKA_RECT.height};
    
    return s;
}

void aktualizovatStojan(Stojan * stojan, float dt)
{
    // stojan budem aktualizovat jenom když se mění vlajka, až se vymění,
    // tak už aktualizovat nebudem
    if(!stojan->prevlajkovava_se || stojan->uz_se_prevlajkoval_uplne)
        return;
    
    // jestli už uplynul převlajkovávací čas tak se stojan určitě převlajkoval
    float progress = stojan->relativni_cas/PREVLAJKOVAVACI_CAS_STOJANU;
    if(progress>1.0f)
    {
        progress = 1.0f;
        stojan->prevlajkovava_se = false;
        stojan->uz_se_prevlajkoval_uplne = true;
        
    }
    
    // vlajky budem posouvat tou pomocnou normalizovanou hodnotou progress, prostě tim čislem
    // vynásobíme vejšku ve který tu vlajku chcem, jednu vlajku budem pronásobovat rovnou, drhuou převrácenou hodnotou
    stojan->vlajka_puvodni.y = (STOJAN_RECT.height - BUBU_VLAJKA_RECT.height) * (progress) + stojan->okraje.y;
    stojan->vlajka_nova.y = (STOJAN_RECT.height - ISRA_VLAJKA_RECT.height) * (1.0f - progress) + stojan->okraje.y;
    
    
    stojan->relativni_cas += dt;
    
    
}

void vykreslitStojan(Stojan * stojan)
{
    DrawTexturePro(textura_ruzne,STOJAN_RECT,stojan->okraje,(Vector2){0,0},0.0f,DARKBLUE);
    
    // stará vlajka muže bejt vidět jenom dokavaď se stojan uplně nepřevlajkoval
    if(!stojan->uz_se_prevlajkoval_uplne)
    {
        DrawTexturePro(textura_ruzne,BUBU_VLAJKA_RECT,stojan->vlajka_puvodni,(Vector2){0,0},0.0f,SKYBLUE);
    }
    // nová vlajka muže bejt logicky vidět jenom když se stojan převlajkovává nebo už celej převlajkoval
    if(stojan->prevlajkovava_se || stojan->uz_se_prevlajkoval_uplne)
    {
        DrawTexturePro(textura_ruzne,ISRA_VLAJKA_RECT,stojan->vlajka_nova,(Vector2){0,0},0.0f,SKYBLUE);
    }
}

#endif

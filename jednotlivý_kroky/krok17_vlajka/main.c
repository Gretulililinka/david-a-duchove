// naimportujem si knihovny
#include <math.h>
#include <stdlib.h>
#include <raylib.h>
#include <time.h>

// Pokuď vodkomentujeme, tak to zkompiluje preprocesorovou podmínkou vypnutý věci
// napřiklad se kolem některejch herních voběktů budou vykreslovat okraje
// #define DEBUG

// naimportujem si vlastní hlavičky
#include "animace.h"
#include "david.h"
#include "projektily.h"
#include "level.h"
#include "menu.h"

//makra na zišťování minimální a maximální hodnoty
#ifndef MAX
#define MAX(a, b) ((a)>(b)? (a) : (b))
#define MIN(a, b) ((a)<(b)? (a) : (b))
#endif

// šířka a výška vokna
#define HERNI_SIRKA 1280
#define HERNI_VYSKA 960

// kolik kostek bude dlouhá naše herní mapa
#define DELKA_LEVELU_BLOKU 80

// přidáme si enum popisující na který vobrazovce jakože sme,
// jestli tý s menu, tý s herním levelem nebo tý s game over
enum KteraObrazovka {OBRAZOVKA_HLAVNI_MENU, OBRAZOVKA_HRA, OBRAZOVKA_GAME_OVER};

enum KteraObrazovka obrazovka = OBRAZOVKA_HLAVNI_MENU;

// textury
Texture2D textura_mesic;
Texture2D textura_david_spritesheet;
Texture2D textura_kameny;
Texture2D textura_duchove_spritesheet;
Texture2D textura_ruzne;
Texture2D textura_hrad;

//textury menu a 'gui' prvků
Texture2D textura_menu_titulek;
Texture2D textura_menu_pozadi;
Texture2D textura_menu_pozadi2;
Texture2D textura_menu_game_over;
Texture2D textura_menu_zmackni_enter;
Texture2D textura_menu_david;

// zvuky
Sound zvuk_kroku;
Sound zvuk_skoku;
Sound zvuk_vystrel;
Sound zvuk_duch_chcip;
Sound zvuk_duch_strela;
Sound zvuk_zasah;
Sound zvuk_kontakt;
Sound zvuk_padu;
Sound zvuk_zaghrouta;
Sound zvuk_powerup;

// přidáme si znělku výhry
Sound zvuk_vyhra;

// hudby
Music hudba_lvl;
Music hudba_bonus;
Music hudba_menu;
Music hudba_game_over;

//ukazatel na právě hranou hudbu
Music * hudba_aktualni;

int main ( void )
{
    // nastavíme generátor nahodnejch čisel nějakým seedem
    // (vobvykle se tam strká aktualní čas ale mužeme si tam dát
    // třeba ňákou konstantu by sme to měli vopakovatelný a mohli reprodukovat stejnej level)
    SetRandomSeed ( time ( 0 ) );

    SetConfigFlags ( FLAG_WINDOW_RESIZABLE | FLAG_VSYNC_HINT );

    InitWindow ( HERNI_SIRKA, HERNI_VYSKA, "David a duchove" );

    InitAudioDevice();

    SetTargetFPS ( 60 );

    // načtem soubory textur
    textura_david_spritesheet = LoadTexture ( "assets/david.png" );
    textura_mesic = LoadTexture ( "assets/moon.png" );
    textura_kameny = LoadTexture ( "assets/kameny.png" );
    textura_duchove_spritesheet = LoadTexture ( "assets/duchove.png" );
    textura_ruzne = LoadTexture ( "assets/misc.png" );
    textura_hrad = LoadTexture ( "assets/hrad2.png" );
    textura_menu_titulek = LoadTexture ( "assets/title.png" );
    textura_menu_pozadi = LoadTexture ( "assets/gy.png" );
    textura_menu_pozadi2 = LoadTexture ( "assets/gy_hroby.png" );
    textura_menu_game_over = LoadTexture ( "assets/game_over.png" );
    textura_menu_zmackni_enter = LoadTexture ( "assets/zmackni_enter.png" );
    textura_menu_david = LoadTexture ( "assets/koli.png" );
    
    // načtem zvuky
    zvuk_kroku = LoadSound ( "assets/kroky.wav" );
    zvuk_skoku = LoadSound ( "assets/skok.wav" );
    zvuk_vystrel = LoadSound ( "assets/bum.wav" );
    zvuk_duch_chcip = LoadSound ( "assets/duch_chcip.wav" );
    zvuk_duch_strela = LoadSound ( "assets/duch_strela.wav" );
    zvuk_zasah = LoadSound ( "assets/zasah.wav" );
    zvuk_kontakt = LoadSound ( "assets/kontakt.wav" );
    zvuk_padu = LoadSound ( "assets/pad.wav" );
    zvuk_zaghrouta = LoadSound ( "assets/zaghrouta.ogg" );
    zvuk_powerup = LoadSound ( "assets/powerup.wav" );
    zvuk_vyhra = LoadSound ( "assets/Victory!.wav" );
    
    //načtem hudby
    hudba_lvl = LoadMusicStream ( "assets/waltz_of_the_ghosts.ogg" );
    hudba_bonus = LoadMusicStream ( "assets/for_a_few_shekels_more_band.ogg" );
    hudba_menu = LoadMusicStream ( "assets/ghost_trip.ogg" );
    hudba_game_over = LoadMusicStream ( "assets/hatikva.ogg" );
    
    Camera2D kamera = {
        
        //posun 'středu' kamery, posunem na střed vobrazovky
        .offset = ( Vector2 ) { GetRenderWidth() / 2.0f, GetRenderHeight() / 2.0f },
        // souřadnice cíle, na co jakože kamera kouká
        .target = ( Vector2 ) {0,0},
        // uhel náklonu kamery ve stupních
        .rotation = 0.0f,
        //přiblížení
        .zoom = 1.0f
    };
    
    //ukazatel, kde si budeme držet vygenerovanej level
    Level * level = NULL;
    // ukazatel, kterej bude držet 'pole' davidovejch střel
    Strela * david_strely = NULL;
    
    // animace běhu
    Animace david_beh = {
        .trvani_framu = 1.0f/30.0f, .relativni_cas=0.0f, .index = 0,
        .jojo = true, .reverzne = false, .zrcadlit = false, .loopovat = true,
        .textura = textura_david_spritesheet, .framy = david_framy_behu, .pocet_framu = sizeof ( david_framy_behu ) /sizeof ( Rectangle )
    };
    // animace idle, jakože když se fláká a nic nedělá. Je to takový pérování nohama na místě
    Animace david_idle = {
        .trvani_framu = 1.0f/15.0f, .relativni_cas=0.0f, .index = 0,
        .jojo = true, .reverzne = false, .zrcadlit = false, .loopovat = true,
        .textura = textura_david_spritesheet, .framy = david_framy_idle, .pocet_framu = sizeof ( david_framy_idle ) /sizeof ( Rectangle )
    };

    Animace david_sed = {
        .trvani_framu = 1.0f/30.0f, .relativni_cas=0.0f, .index = 0,
        .jojo = true, .reverzne = false, .zrcadlit = false, .loopovat = false,
        .textura = textura_david_spritesheet, .framy = david_framy_sed, .pocet_framu = sizeof ( david_framy_sed ) /sizeof ( Rectangle )
    };

    // animace skoku, david tam vicemeně jenom máchá nožičkama ve vzduchu
    Animace david_skok = {
        .trvani_framu = 1.0f/10.0f, .relativni_cas=0.0f, .index = 0,
        .jojo = true, .reverzne = false, .zrcadlit = false, .loopovat = true,
        .textura = textura_david_spritesheet, .framy = david_framy_skoku, .pocet_framu = sizeof ( david_framy_skoku ) /sizeof ( Rectangle )
    };
    
    
    David david;

    // nastavíme ukazatel aktuální animace na animaci 'idle'
    david.aktualni_animace = &david.animace_idle;
                
    // podle aktuální pozice nastavíme okraje oběktu a teďko nově i hitbox
    david.okraje = ( Rectangle ) {
        david.pozice.x + 45, david.pozice.y +8, DAVID_F_SIRKA -90, DAVID_F_VYSKA - 10
    };
    david.hitbox = ( Rectangle ) {
        david.pozice.x + 55, david.pozice.y +20, DAVID_F_SIRKA -110, DAVID_F_VYSKA-30
    };
    
    // nastavíme aktuální hudbu na hudbu menu
    hudba_aktualni = &hudba_menu;
    
    //vygenerujem si hlavní menu a zapnem vodpovidajicí muziku
    vygenerovatMenu();
    PlayMusicStream ( hudba_menu );
    
    
    while ( !WindowShouldClose() ) {

        float dt = GetFrameTime();
        
        //aktualizujeme hudbu, na kterou ukazuje ukazatel 'hudba_aktualni'
        UpdateMusicStream ( *hudba_aktualni );
        
        // spočitáme si přiblížení naší kamery
        // uděláme to tak, že si spočitáme poměr skutečný šířky obrazovky s naší 'virtuální' požadovanou,
        // to samý uděláme se skutečnou a požadovanou vejškou, noa vybereme tu menší hodnotu
        // (jak se to chová si mužeme vyzkoušet behem hry, když budeme ruzně měnit velikost vokna)
        const float priblizeni = MIN ( ( float ) GetRenderWidth() / HERNI_SIRKA, ( float ) GetRenderHeight() / HERNI_VYSKA );

        // nastavíme atribut 'zoom' tou naší spočitanou hodnotou
        kamera.zoom = priblizeni;
        //nastavíme posun kamery na velikost půlky vobrazovky
        kamera.offset = ( Vector2 ) {
            GetScreenWidth() /2, GetScreenHeight() /2
        };
        if ( obrazovka == OBRAZOVKA_HLAVNI_MENU ) {
            BeginDrawing();
            // GetRenderHeight a GetRenderWidth je skoro to samý co GetScreenHeight a GetScreenWidth jenom s tim rozdílem
            // že to započitává DPI hele https://cs.wikipedia.org/wiki/DPI, jakože počet pixelů na jednotku délky
            // (na vobyčejným počitači nás to asi nemusí moc trápit, na mobilním zařizení by to ale bylo zásadní)
            DrawRectangleGradientV ( 0,0,GetRenderWidth(),GetRenderHeight(),DARKBLUE,BLACK );

            //aktualizujeme a vykreslíme menu
            aktualizovatVykreslitMenu ( dt );
            EndDrawing();

            if ( IsKeyPressed ( KEY_ENTER ) ) {

                // pokud ukazatel na strukturu level neni vynulovanej, tak to pravděpodobně znamená že se už jednou hrálo a
                // že musíme starej level uklidit by sme mohli vygenerovat novej
                if ( level != NULL ) {
                    freeLevel ( level );
                    
                    // uklidíme i střely, idkyž by asi jako bylo víc lepčejší každý prostě nastavit atribut 'aktivni' na false :D
                    free ( david_strely );
                }
                
                //vygenerujeme si herní level
                level = vygenerovatLevel ( DELKA_LEVELU_BLOKU,textura_kameny );
                
                // správně by jsme asi jako měli ňák líp informovat uživatele že se něco pokazilo třeba ňákým logem
                // takle by na to koukal jako péro z gauče co se jako pokazilo že hra hnedka spadla :D :D
                // to až příště jestli budete hodný :D :D :D ;D
                if(! level) return 1;


                david_strely = calloc ( POCET_STREL_DAVIDA_MAX,sizeof ( Strela ) );
                if(!david_strely)
                {
                    freeLevel(level);
                    return 1;
                }
                
                //inicializujeme si davida
                david = (David){
                    
                    .animace_beh = david_beh,
                    .animace_idle = david_idle,
                    .animace_sed = david_sed,
                    .animace_skok = david_skok,
                    .aktualni_animace = NULL,
                    .pozice = {0,0},
                    .smer = 1,
                    .mapa = level->dlazdicova_mapa,
                    .vertikalni_rychlost = 0.0f,
                    .zdaSkace = true,
                    .strely = david_strely,
                    .strileci_cooldown = 0.0f,
                    .blikaci_cas = 0.0f,
                    .zivoty = POCET_ZIVOTU_DAVIDA_MAX,
                    .zranitelny = true,
                    .ma_bonus = false,
                    .hvezdny_bonus_cas = 0.0f,

                };

                //nastavíme ukazatel aktuální animace na animaci 'idle'
                david.aktualni_animace = &david.animace_idle;
                
                //podle aktuální pozice nastavíme okraje oběktu a hitbox
                david.okraje = ( Rectangle ) {
                    david.pozice.x + 45, david.pozice.y +8, DAVID_F_SIRKA -90, DAVID_F_VYSKA - 10
                };
                david.hitbox = ( Rectangle ) {
                    david.pozice.x + 55, david.pozice.y +20, DAVID_F_SIRKA -110, DAVID_F_VYSKA-30
                };

                //vypnem hudbu menu
                StopMusicStream ( hudba_menu );
                
                // vypnem hudbu levelu (by sme ji restartovali aby nám nehrála třeba někde vodprostředka)
                StopMusicStream ( hudba_lvl );
                
                // zapnem hudbu levelu
                PlayMusicStream ( hudba_lvl );
                
                // a nastavíme jeji adresu do ukazatele na aktuální hudbu, by se nám muzika taky aktualizovala, bez  toho by nehrála :D
                hudba_aktualni = &hudba_lvl;
                
                //nakonec přepnem vobrazovku
                obrazovka = OBRAZOVKA_HRA;
            }

            // vykreslujem menu, takže nás další věci nezajímaj a přeskočíme je tim, že skočíme na začátek dalšího while cyklusu
            continue;
        }
        
        
        if ( obrazovka == OBRAZOVKA_HRA ) {

            // aktualizujem davida
            // funkce žere ukazatel, takže davida tam nacpem tak, že tam strčíme jeho adresu
            aktualizovatDavida ( &david, dt );
            
            // aktualizujem level
            enum LevelStatus status = aktualizovatLevel ( level, &david, dt );

            // podle návratový hodnoty určíme, jesttli se hraje dál nebo jestli nám David umřel a musíme
            // přepnout na 'game over' nebo jestli vyhrál a musíme se vrátit zpátky do hlavního menu
            // (ve finální verzi tý hry by sme asi měli mit ňáký hi-score nebo tak něco)
            // jenom tady přepínáme muziku a aktualní 'vobrazovku'
            if ( status == LVL_PROHRA ) {
                obrazovka = OBRAZOVKA_GAME_OVER;
                StopMusicStream ( hudba_lvl );
                StopMusicStream ( hudba_bonus );
                PlayMusicStream ( hudba_game_over );
                hudba_aktualni = &hudba_game_over;

            } else if ( status == LVL_VYHRA ) {
                StopMusicStream ( hudba_lvl );
                StopMusicStream ( hudba_bonus );

                PlayMusicStream ( hudba_menu );
                hudba_aktualni = &hudba_menu;
                obrazovka = OBRAZOVKA_HLAVNI_MENU;

            }
        }
        
        // nastavíme cíl kamery na střed davida
        kamera.target = ( Vector2 ) {
            david.okraje.x + david.okraje.width / 2.0f, david.okraje.y + david.okraje.height / 2.0f
        };
        
        const float kamera_target_min_x = GetRenderWidth() / 2.0f / priblizeni;
        const float kamera_target_max_x = DELKA_LEVELU_BLOKU * BLOK_SIRKA - GetRenderWidth() /2/priblizeni;
        const float kamera_target_max_y = GetRenderHeight() / 2.0f / priblizeni - DAVID_F_VYSKA + BLOK_VYSKA*5;

        // pohlídáme si ty minimální a maximální možný hodnoty
        kamera.target.x = MAX ( kamera.target.x, kamera_target_min_x );
        kamera.target.x = MIN ( kamera.target.x, kamera_target_max_x );
        kamera.target.y = MIN ( kamera.target.y, kamera_target_max_y );

        // zapnem vykreslování
        BeginDrawing();

        // vykreslíme ten gradient
        DrawRectangleGradientV ( 0,0,GetScreenWidth(),GetScreenHeight(),DARKBLUE,BLACK );
        
        // první kus našeho parallaxu
        // hovorka se asi jako bojí děsně velkejch měsiců když je furt v horrorovejch komixech kreslí,
        // tak mu uděláme radost na na pozadí vykreslíme supr strašidelnej měsíc :D :D
        float mensi_rozmer = MIN ( GetRenderWidth(),GetRenderHeight() );
        DrawTexturePro ( textura_mesic, ( Rectangle ) {
            0,0,textura_mesic.width,textura_mesic.height
        }, ( Rectangle ) {
            GetRenderWidth() /2,GetRenderHeight() /2,mensi_rozmer*1.5, mensi_rozmer*1.5
        }, ( Vector2 ) {
            mensi_rozmer*1.5/2,mensi_rozmer*1.5/2
        },0, ( Color ) {
            102, 191, 255, 128
        } );
        
        // aktivujem transformování tou naší kamerou
        // takže jakoby vykreslujem to, co kamera vidí
        BeginMode2D ( kamera );
        
        //vykreslíme parallax

        // to je jakože takový pozadí, který se různě pomaličku posouvá, když se hráč pohybuje na mapě a vytváří to
        // víc lepší iluzi že se hráč jakože někam pohybuje
        // todle je zatim dělaný jentak na hrubo, vykresluje to takovou siluletu jeruzalémskýho hradu která se děsně pomaličku posouvá
        // když David poskakuje nebo de dopředu


        // velikost tý textury hradu
        Rectangle hrad_zdroj = {0,0,1024,512};
        
        // cílovej vobdelnik hradu
        // uděláme ho 2x delší než je šířka vobrazovky a budem 
        // (asi to nebude moc dobře fungovat na uzkejch vobrazovkách, tam by sme asi jako museli zvolit
        // ňákej uplně jinej koncept, nvm)
        Rectangle hrad_cil = {
            .x = kamera.target.x - GetRenderWidth() /priblizeni,
            .y = kamera.target.y - GetRenderWidth() /2/priblizeni,
            // vodelnik taky bude muset bejt 2x širší než vyšší, by se nám moc nezdeformovala textura
            .width = GetRenderWidth() /priblizeni*2,
            .height = GetRenderWidth() /2/priblizeni*2,
        };
        
        // počátek/origin toho velkýho rectanglu budem určovat z mezí vobrazovky
        // musíme si pohlídat když hráč stojí u kraje mapy by nám tam nevylezla někam doprostředka vobrazovky hrana textury :D
        float hrad_y_posun_paralax = -300.0f + 300.0f * kamera.target.y/kamera_target_max_y;
        float hrad_x_posun_paralax = -GetRenderWidth() /priblizeni/2 + GetRenderWidth() /priblizeni * ( kamera.target.x - kamera_target_min_x ) /kamera_target_max_x;

        // noa vykreslíme ten náš parallax
        DrawTexturePro ( textura_hrad, hrad_zdroj, hrad_cil, ( Vector2 ) {
            hrad_x_posun_paralax,hrad_y_posun_paralax
        },0,WHITE );

        // vykreslíme level
        vykreslitLevel( level, &kamera);
        
        // vykreslíme davida
        vykreslitDavida(&david);
        
        DrawRectangleGradientV ( kamera.target.x - GetRenderWidth() / 2 / priblizeni,BLOK_VYSKA*10, GetRenderWidth()/priblizeni,BLOK_VYSKA*3,BLANK, BLACK );
        // a pod tim všecko vyčerníme černým vodelnikem, kterej hezky navazuje na ten náš černej gradient
        DrawRectangle ( kamera.target.x - GetRenderWidth() /2/priblizeni,BLOK_VYSKA*13,GetRenderWidth() /priblizeni,GetRenderHeight()/priblizeni,BLACK );
        
        // vypneme kameru
        EndMode2D();
        
        
        const Rectangle srdicko_rect = {2,503,96,83};
        
        for ( int i = 0; i < POCET_ZIVOTU_DAVIDA_MAX; i++ ) {
            Rectangle cil = {
                .x = i*srdicko_rect.width/2,
                .y = GetScreenHeight() - srdicko_rect.height/2,
                .width = srdicko_rect.width/2,
                .height = srdicko_rect.height/2,
            };
            DrawTexturePro ( textura_ruzne,srdicko_rect,cil, ( Vector2 ) {
                0,0
            },0.0f,i<david.zivoty? WHITE:BLACK );
        }
        
        // 'obrazovka' game over
        if ( obrazovka == OBRAZOVKA_GAME_OVER ) {
            // pokud david umře, tak už nebudeme herní věci aktualizovat ale budem je furt vykreslovat
            // všecko překryjeme smutečním černým poloprusvitným vobdelnikem....
            DrawRectangle ( 0,0,GetRenderWidth(),GetRenderHeight(), ( Color ) {
                0,0,0,128
            } );
            // .... přes kterej namalujeme velkej zelenej text s nápisem 'game over'....
            DrawTexture ( textura_menu_game_over,GetRenderWidth() /2 - textura_menu_game_over.width/2,GetRenderHeight() /3 - textura_menu_game_over.height/2,WHITE );
            // ....a textem, že má hráč zmáčknout na klávesnici čudlik 'enter'
            DrawTexture ( textura_menu_zmackni_enter,GetRenderWidth() /2 - textura_menu_zmackni_enter.width/2,GetRenderHeight() - textura_menu_zmackni_enter.height,WHITE );

            if ( IsKeyPressed ( KEY_ENTER ) ) {
                // noa když ten čudlik hráč zmáčkne, tak přepnem muziku, a přepnem vobrazovku
                StopMusicStream ( hudba_game_over );
                PlayMusicStream ( hudba_menu );
                hudba_aktualni = &hudba_menu;
                obrazovka = OBRAZOVKA_HLAVNI_MENU;
            }

        }

        // a skončíme s vykreslováním by se naše scéna poslala na monitor
        EndDrawing();
    }

    CloseWindow();
    
    // uvolníme naše vlastní struktury
    if ( david_strely ) {
        free ( david_strely );
    }
    if ( level ) {
        freeLevel ( level );
    }

    //uklidíme textury
    UnloadTexture ( textura_mesic );
    UnloadTexture ( textura_david_spritesheet );
    UnloadTexture ( textura_kameny );
    UnloadTexture ( textura_duchove_spritesheet );
    UnloadTexture ( textura_ruzne );
    UnloadTexture ( textura_hrad );
    UnloadTexture ( textura_menu_titulek );
    UnloadTexture ( textura_menu_pozadi );
    UnloadTexture ( textura_menu_pozadi2 );
    UnloadTexture ( textura_menu_david );
    UnloadTexture ( textura_ruzne );
    UnloadTexture ( textura_menu_zmackni_enter );
    UnloadTexture ( textura_menu_game_over );

    // vypnem audio zařízení
    CloseAudioDevice();
    
    // musíme uvolnit i muziku
    UnloadMusicStream ( hudba_lvl );
    UnloadMusicStream ( hudba_bonus );
    UnloadMusicStream ( hudba_menu );
    UnloadMusicStream ( hudba_game_over );
    
    // a taky uvolníme zvuky
    UnloadSound ( zvuk_kroku );
    UnloadSound ( zvuk_skoku );
    UnloadSound ( zvuk_vystrel );
    UnloadSound ( zvuk_duch_chcip );
    UnloadSound ( zvuk_duch_strela );
    UnloadSound ( zvuk_zasah );
    UnloadSound ( zvuk_kontakt );
    UnloadSound ( zvuk_padu );
    UnloadSound ( zvuk_zaghrouta );
    UnloadSound ( zvuk_powerup );
    UnloadSound ( zvuk_vyhra );

    return 0;
}

#ifndef _DAVID_A_DUCHOVE_PROJEKTILY_H_
#define _DAVID_A_DUCHOVE_PROJEKTILY_H_

#include <raylib.h>

#include "mapa.h"

// přidáme si sem střelu duchů
enum druh_strely {strela_davida,strela_ducha};

typedef struct Strela {
    
    // rychlost střely
    float rychlost;
    
    // relativní čas střely
    float relativni_cas;
    
    // nastavená maximální doba života střely
    // když relativní čas překročí tudle hodnotu tak střelu deaktivujem a budem považovat za zničenou
    float doba_zivota;
    enum druh_strely druh;
    
    // pozice střely
    Vector2 pozice;
    
    // zda je střela aktivní
    // pokud aktivní neni považujem ji za zničenou a/nebo nevystřelenou, takovou střelu nebudeme
    // vykreslovat ani aktualizovat
    bool aktivni;
}
Strela;

void aktualizovatStrelu ( Strela * strela, Mapa * mapa, float dt )
{
    strela->relativni_cas += dt;
    if ( strela->relativni_cas > strela->doba_zivota ) {
        strela->aktivni = false;
    } else {
        strela->pozice.x += dt * strela->rychlost;
        
        // pokud dojde ke srážce střely s blokem mapy, tak střelu deaktivujem
        if ( kolizeSeBlokemMapy_bod ( strela->pozice, mapa ) ) {
            strela->aktivni = false;
        }
    }
}

void vykreslitStrelu ( Strela * strela )
{
    if ( !strela->aktivni ) {
        return;
    }
    switch ( strela->druh ) {
    case strela_davida:
        DrawCircleGradient ( strela->pozice.x,strela->pozice.y,5.0f,WHITE,DARKGRAY );
        break;
        
    // přidáme si sem střelu duchů
    // bude to taky taková červená softwérová kulička
    case strela_ducha:
        DrawCircleGradient ( strela->pozice.x,strela->pozice.y,5.0f,RED,Fade(RED,0.8f) );
        break;
    default:
        break;
    }
}

#endif

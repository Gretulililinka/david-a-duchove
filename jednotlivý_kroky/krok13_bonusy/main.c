// naimportujem si knihovny
#include <math.h>
#include <stdlib.h>
#include <raylib.h>
#include <time.h>

// Pokuď vodkomentujeme, tak to zkompiluje preprocesorovou podmínkou vypnutý věci
// napřiklad se kolem některejch herních voběktů budou vykreslovat okraje
// #define DEBUG

// naimportujem si vlastní hlavičky
#include "animace.h"
#include "david.h"
#include "projektily.h"
#include "level.h"

//makra na zišťování minimální a maximální hodnoty
#ifndef MAX
#define MAX(a, b) ((a)>(b)? (a) : (b))
#define MIN(a, b) ((a)<(b)? (a) : (b))
#endif

// šířka a výška vokna
#define HERNI_SIRKA 1280
#define HERNI_VYSKA 960

// kolik kostek bude dlouhá naše herní mapa
#define DELKA_LEVELU_BLOKU 100

// textury
Texture2D textura_mesic;
Texture2D textura_david_spritesheet;
Texture2D textura_kameny;
Texture2D textura_duchove_spritesheet;
Texture2D textura_ruzne;

// zvuky
Sound zvuk_kroku;
Sound zvuk_skoku;
Sound zvuk_vystrel;
Sound zvuk_duch_chcip;
Sound zvuk_duch_strela;
Sound zvuk_zasah;
Sound zvuk_kontakt;
Sound zvuk_padu;
Sound zvuk_zaghrouta;
Sound zvuk_powerup;

int main ( void )
{
    // nastavíme generátor nahodnejch čisel nějakým seedem
    // (vobvykle se tam strká aktualní čas ale mužeme si tam dát
    // třeba ňákou konstantu by sme to měli vopakovatelný a mohli reprodukovat stejnej level)
    SetRandomSeed ( time ( 0 ) );

    SetConfigFlags ( FLAG_WINDOW_RESIZABLE | FLAG_VSYNC_HINT );

    InitWindow ( HERNI_SIRKA, HERNI_VYSKA, "David a duchove" );

    InitAudioDevice();

    SetTargetFPS ( 60 );

    // načtem soubory textur
    textura_david_spritesheet = LoadTexture ( "assets/david.png" );
    textura_mesic = LoadTexture ( "assets/moon.png" );
    textura_kameny = LoadTexture ( "assets/kameny.png" );
    textura_duchove_spritesheet = LoadTexture ( "assets/duchove.png" );
    textura_ruzne = LoadTexture ( "assets/misc.png" );
    
    // načtem zvuky
    zvuk_kroku = LoadSound ( "assets/kroky.wav" );
    zvuk_skoku = LoadSound ( "assets/skok.wav" );
    zvuk_vystrel = LoadSound ( "assets/bum.wav" );
    zvuk_duch_chcip = LoadSound ( "assets/duch_chcip.wav" );
    zvuk_duch_strela = LoadSound ( "assets/duch_strela.wav" );
    zvuk_zasah = LoadSound ( "assets/zasah.wav" );
    zvuk_kontakt = LoadSound ( "assets/kontakt.wav" );
    zvuk_padu = LoadSound ( "assets/pad.wav" );
    zvuk_zaghrouta = LoadSound ( "assets/zaghrouta.ogg" );
    zvuk_powerup = LoadSound ( "assets/powerup.wav" );
    
    Camera2D kamera = {
        
        //posun 'středu' kamery, posunem na střed vobrazovky
        .offset = ( Vector2 ) { GetRenderWidth() / 2.0f, GetRenderHeight() / 2.0f },
        // souřadnice cíle, na co jakože kamera kouká
        .target = ( Vector2 ) {0,0},
        // uhel náklonu kamery ve stupních
        .rotation = 0.0f,
        //přiblížení
        .zoom = 1.0f
    };
    
    //ukazatel, kde si budeme držet vygenerovanej level
    Level * level = NULL;
    // ukazatel, kterej bude držet 'pole' davidovejch střel
    Strela * david_strely = NULL;
    
    //vygenerujeme si herní level
    level = vygenerovatLevel ( DELKA_LEVELU_BLOKU,textura_kameny );
    
    //alokujeme si 'pole' střel pomocí funkce calloc (se vod malloc liší tim, že nám alokovanou paměť vynuluje,
    // první argument je počet alokovanejch struktur, druhej velikost jedný tý struktury)
    david_strely = calloc ( POCET_STREL_DAVIDA_MAX,sizeof ( Strela ) );
    
    // animace běhu
    Animace david_beh = {
        .trvani_framu = 1.0f/30.0f, .relativni_cas=0.0f, .index = 0,
        .jojo = true, .reverzne = false, .zrcadlit = false, .loopovat = true,
        .textura = textura_david_spritesheet, .framy = david_framy_behu, .pocet_framu = sizeof ( david_framy_behu ) /sizeof ( Rectangle )
    };
    // animace idle, jakože když se fláká a nic nedělá. Je to takový pérování nohama na místě
    Animace david_idle = {
        .trvani_framu = 1.0f/15.0f, .relativni_cas=0.0f, .index = 0,
        .jojo = true, .reverzne = false, .zrcadlit = false, .loopovat = true,
        .textura = textura_david_spritesheet, .framy = david_framy_idle, .pocet_framu = sizeof ( david_framy_idle ) /sizeof ( Rectangle )
    };

    Animace david_sed = {
        .trvani_framu = 1.0f/30.0f, .relativni_cas=0.0f, .index = 0,
        .jojo = true, .reverzne = false, .zrcadlit = false, .loopovat = false,
        .textura = textura_david_spritesheet, .framy = david_framy_sed, .pocet_framu = sizeof ( david_framy_sed ) /sizeof ( Rectangle )
    };

    // animace skoku, david tam vicemeně jenom máchá nožičkama ve vzduchu
    Animace david_skok = {
        .trvani_framu = 1.0f/10.0f, .relativni_cas=0.0f, .index = 0,
        .jojo = true, .reverzne = false, .zrcadlit = false, .loopovat = true,
        .textura = textura_david_spritesheet, .framy = david_framy_skoku, .pocet_framu = sizeof ( david_framy_skoku ) /sizeof ( Rectangle )
    };
    
    
    // kdyžuž máme vyrobený animace, tak si mužeme vyrobit Davida
    David david = {
                    
        .animace_beh = david_beh,
        .animace_idle = david_idle,
        .animace_sed = david_sed,
        .animace_skok = david_skok,
         
        .aktualni_animace = NULL,

        // necháme ho na herní mapu spadnou z vejšky
        .pozice = {0,0},
                    

        .smer = 1,
        
        // nastavíme mapu na tu skovanou ve struktuře levelu
        .mapa = level->dlazdicova_mapa,
        
        .vertikalni_rychlost = 0.0f,
        .zdaSkace = true,
        
        // nastavíme ukazatel střel na ty naše callocem vygenerovaný střely
        .strely = david_strely,
                    
        //vynulujeme davidovy vnitřní časovače střílecího cooldownu a času blikání resp. dočasný
        // nezranitelnosti po nepřátelským zásahu ie. islamistickým duchem nebo rudou kulkou
        .strileci_cooldown = 0.0f,
        .blikaci_cas = 0.0f,
                    
        // nastavíme počet životů na max a atribut zranitelnosti na true
        .zivoty = POCET_ZIVOTU_DAVIDA_MAX,
        .zranitelny = true,
        
        // nastavíme atributy vokolo bonusu
        .ma_bonus = false,
        .hvezdny_bonus_cas = 0.0f,

    };

    // nastavíme ukazatel aktuální animace na animaci 'idle'
    david.aktualni_animace = &david.animace_idle;
                
    // podle aktuální pozice nastavíme okraje oběktu a teďko nově i hitbox
    david.okraje = ( Rectangle ) {
        david.pozice.x + 45, david.pozice.y +8, DAVID_F_SIRKA -90, DAVID_F_VYSKA - 10
    };
    david.hitbox = ( Rectangle ) {
        david.pozice.x + 55, david.pozice.y +20, DAVID_F_SIRKA -110, DAVID_F_VYSKA-30
    };
    
    while ( !WindowShouldClose() ) {

        float dt = GetFrameTime();
        
        // spočitáme si přiblížení naší kamery
        // uděláme to tak, že si spočitáme poměr skutečný šířky obrazovky s naší 'virtuální' požadovanou,
        // to samý uděláme se skutečnou a požadovanou vejškou, noa vybereme tu menší hodnotu
        // (jak se to chová si mužeme vyzkoušet behem hry, když budeme ruzně měnit velikost vokna)
        const float priblizeni = MIN ( ( float ) GetRenderWidth() / HERNI_SIRKA, ( float ) GetRenderHeight() / HERNI_VYSKA );

        // nastavíme atribut 'zoom' tou naší spočitanou hodnotou
        kamera.zoom = priblizeni;
        //nastavíme posun kamery na velikost půlky vobrazovky
        kamera.offset = ( Vector2 ) {
            GetScreenWidth() /2, GetScreenHeight() /2
        };
        
        //aktualizujem Davida
        aktualizovatDavida(&david, dt);
        
        //aktualizujem level
        aktualizovatLevel ( level, &david, dt );
        
        // nastavíme cíl kamery na střed davida
        kamera.target = ( Vector2 ) {
            david.okraje.x + david.okraje.width / 2.0f, david.okraje.y + david.okraje.height / 2.0f
        };
        
        const float kamera_target_min_x = GetRenderWidth() / 2.0f / priblizeni;
        const float kamera_target_max_x = DELKA_LEVELU_BLOKU * BLOK_SIRKA - GetRenderWidth() /2/priblizeni;
        const float kamera_target_max_y = GetRenderHeight() / 2.0f / priblizeni - DAVID_F_VYSKA + BLOK_VYSKA*5;

        // pohlídáme si ty minimální a maximální možný hodnoty
        kamera.target.x = MAX ( kamera.target.x, kamera_target_min_x );
        kamera.target.x = MIN ( kamera.target.x, kamera_target_max_x );
        kamera.target.y = MIN ( kamera.target.y, kamera_target_max_y );

        // zapnem vykreslování
        BeginDrawing();

        // vykreslíme ten gradient
        DrawRectangleGradientV ( 0,0,GetScreenWidth(),GetScreenHeight(),DARKBLUE,BLACK );
        
        // aktivujem transformování tou naší kamerou
        // takže jakoby vykreslujem to, co kamera vidí
        BeginMode2D ( kamera );

        // vykreslíme level
        vykreslitLevel( level, &kamera);
        
        // vykreslíme davida
        vykreslitDavida(&david);
        
        DrawRectangleGradientV ( kamera.target.x - GetRenderWidth() / 2 / priblizeni,BLOK_VYSKA*10, GetRenderWidth()/priblizeni,BLOK_VYSKA*3,BLANK, BLACK );
        // a pod tim všecko vyčerníme černým vodelnikem, kterej hezky navazuje na ten náš černej gradient
        DrawRectangle ( kamera.target.x - GetRenderWidth() /2/priblizeni,BLOK_VYSKA*13,GetRenderWidth() /priblizeni,GetRenderHeight()/priblizeni,BLACK );
        
        // vypneme kameru
        EndMode2D();
        
        // nakreslíme HUD - takový to herní menu co ukazuje počet životů, skóre a tak
        // (nám to vykresluje jenom počet životů)
        // po tom co sme vypli kameru, malujeme bez tý kamerový transformace, takže nebudem
        // mit problem trefit levej spodní vokraj vobrazovky kde si budeme malovat
        // takovou řadu srdíček který budou jakože ukazovat kolik Davidoj zbejvá životů
        
        // zrojová voblast srdička ve spritesheetu 'různé'
        const Rectangle srdicko_rect = {2,503,96,83};
        
        // srdíček budem vykreslovat v řadě za sebou furt stejnej maximální počet,
        // akorát jenom když budem vykreslovat ty který by přesahovali aktualní počet Davidovejch
        // životů, tak je budeme malovat černý
        for ( int i = 0; i < POCET_ZIVOTU_DAVIDA_MAX; i++ ) {
            Rectangle cil = {
                .x = i*srdicko_rect.width/2,
                .y = GetScreenHeight() - srdicko_rect.height/2,
                .width = srdicko_rect.width/2,
                .height = srdicko_rect.height/2,
            };
            DrawTexturePro ( textura_ruzne,srdicko_rect,cil, ( Vector2 ) {
                0,0
            },0.0f,i<david.zivoty? WHITE:BLACK );
        }

        // a skončíme s vykreslováním by se naše scéna poslala na monitor
        EndDrawing();
    }

    CloseWindow();
    
    // uvolníme naše vlastní struktury
    if ( david_strely ) {
        free ( david_strely );
    }
    if ( level ) {
        freeLevel ( level );
    }

    //uklidíme textury
    UnloadTexture ( textura_mesic );
    UnloadTexture ( textura_david_spritesheet );
    UnloadTexture ( textura_kameny );
    UnloadTexture ( textura_duchove_spritesheet );
    UnloadTexture ( textura_ruzne );

    // vypnem audio zařízení
    CloseAudioDevice();
    
    // a taky uvolníme zvuky
    UnloadSound ( zvuk_kroku );
    UnloadSound ( zvuk_skoku );
    UnloadSound ( zvuk_vystrel );
    UnloadSound ( zvuk_duch_chcip );
    UnloadSound ( zvuk_duch_strela );
    UnloadSound ( zvuk_zasah );
    UnloadSound ( zvuk_kontakt );
    UnloadSound ( zvuk_padu );
    UnloadSound ( zvuk_zaghrouta );
    UnloadSound ( zvuk_powerup );

    return 0;
}

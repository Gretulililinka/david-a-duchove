#ifndef _DAVID_A_DUCHOVE_DUCH_H_
#define _DAVID_A_DUCHOVE_DUCH_H_

#include <stdlib.h>
#include <raylib.h>

#include "mapa.h"
#include "projektily.h"


// vezmem si z 'main.c' texturu spritesheetu duchů a zvuky
extern Texture2D textura_duchove_spritesheet;

extern Sound zvuk_duch_chcip;
extern Sound zvuk_duch_strela;

// maximalni počet životů ducha
#define HITPOINTU_DUCHA 3

// čas v sekundách jak dlouho duch chcípe
#define CHIPACI_CAS_DUCHA 1.5f

// kolik má duch střel
#define ZASOBNIK_STREL_DUCHA 3

// střílecí cooldown ducha, vlastně to střílení je uplně identicky řešený jako v davidoj :O ;D
#define STRILECI_COOLDOWN_DUCHA 1.0f

// rychlost vypálenejch projektilů ducha
#define RYCHLOST_STRELY_DUCHA 600.0f

//rychlost chůze ducha
#define RYCHLOST_CHUZE_DUCHA 120.0f

// šířka a vejška jednoho 'podvobrázku' v spritesheetu
#define DUCH_F_VYSKA 240.0f
#define DUCH_F_SIRKA 85.0f

typedef struct Duch {
    
    // směr kterým duch kouká, taky identicky řešený jako v davidoj
    int smer;
    
    // hp jakože hitpointy jakože počet životů
    int hp;

    // oblast textury, jakože kterej kousek z toho spritesheetu s duchama jsme vybrali
    Rectangle oblast_textury;
    
    // okraje ducha, podobně jako u davida
    // (duchoj vodpovídaj vokraje velikosti textury, to u davida neplatí)
    Rectangle okraje;
    
    // hitbox, voblast která definuje 'zranitelnou' část vobrázku ducha
    // bude trochu tenčí než 'okraje', by sme měli zásah až když bude kulka fakt někde nad
    // viditelnou částí vobrázku
    // a pak taky trošku nad zemí (ta poloprusvitná část vobrázků duchů bude jakoby nezranitelná)
    Rectangle hitbox;

    // jestli je duch aktivní
    // pokud neni, tak ho nebudem vykreslovat ani aktualizovat, prostě bude upně vyplej
    bool aktivni;
    
    // jestli duch chícpe, jakože mu už došly hitpointy a máme za
    bool chcipe;

    // vnitřní čas ducha
    float relativni_cas;
    
    // uhel ducha pro animaci efektu chcípání
    float uhel;
    
    // střílecí cooldown ducha
    float strileci_cooldown;

    
    // zásobnik střel ducha
    Strela * strely;

    // zvuky ducha
    // pozor budou to 'aliasy' těch zvuků,povim v 'konstruktoru'
    Sound zvuk_chcipnuti;
    Sound zvuk_strela;
}
Duch;

// takovej jakože pseudokonstruktor instace ducha
// bere jedinej argument, pozici bodu kde ducha jakoby vyrobíme
//pozor, používá malloc, takže se pak musí udělat na každým duchoj freeDucha()
Duch * vygenerovatDucha ( Vector2 kde )
{
    // alokujem si ducha, kterýho pak budeme v připadě uspěšnýho vyrobení z konstrukoru vracet
    // (když se něco pokazí, vracíme nulovou adresu, prostě nulu)
    Duch * duch = calloc ( 1, sizeof ( Duch ) );
    if(!duch)return NULL;
    
    // počáteční směr ducha bude náhodnej
    duch->smer = GetRandomValue( 0,1 ) ? 1 : -1;
    
    // vokraje určíme z počáteční polohy a rozměrů podvobrázku spritesheetu
    duch->okraje = ( Rectangle ) {
        kde.x,kde.y,DUCH_F_SIRKA,DUCH_F_VYSKA
    };
    
    // podobně si určíme hitbox, bude ale votrošku menčí než okraje a bude i trošku kratčí na vejšku
    // (kuli těm nezranitelnejm nohoum duchů)
    duch->hitbox = ( Rectangle ) {
        kde.x+15,kde.y+15,55,155
    };
    
    // oblast textury určíme náhodně, vybereme uplně náhodnej podvobrázek
    duch->oblast_textury = ( Rectangle ) {
        DUCH_F_SIRKA * GetRandomValue( 0,5 ),0 + DUCH_F_VYSKA * GetRandomValue( 0,1 ),DUCH_F_SIRKA * duch->smer,DUCH_F_VYSKA
    };
    
    // aktivujem ducha a nastavíme mu hitpointy na vychozí hodnotu
    duch->aktivni = true;
    duch->hp = HITPOINTU_DUCHA;
    
    // a nakopírujem si zvuky tim, že si vyrobíme jejich alias
    // Potíž s normálníma zvukama v raylib je v tom, že dycky máme jakoby jenom jednu přehratelnou instanci. U voběktu/struktury davida
    // todle neni moc vekej problém, páč Davida máme na mapě jenom jednoho a ten jeho kod vubec moc nepřipouští situace žeby David měl
    // vydávat ňákej stenej zvuk rychle posobě. Duchů ale budem mit na mapě hodně a šance na tudle kolizi přehrávání je vyrazně věčí,
    // třeba zvuk chcípání ducha je dost dlouhej a je možný že ho budem chtít začít přehrávat ve stejným vokamžiku,
    // kdy ho už ňákej jinej chcipajicí duch hraje. Stalo by se nejspiš to že by to stoplo předchazejicí přehrávání a začalo by to hrát
    // stejnej zvuk vodzačátku.
    // Na todlecto chytrý lidi vod raylibu naštěstí mysleli a přidali možnost vyrábění aliasu zvuků, prostě si jakoby vyrobíme kopii toho zvuku
    // (pod kapotou nejspíš ty kopie a voriginál furt sdílej stejný zdrojový data zvuku, jen si k tomu každá kopie asi jakoby pamatuje jinej
    // balast vokolo, jakože čas přehrávání, nastavení hlasitosti etc)
    
    // takže si vyrobíme ty aliasy zvuků
    duch->zvuk_chcipnuti = LoadSoundAlias ( zvuk_duch_chcip );
    duch->zvuk_strela = LoadSoundAlias ( zvuk_duch_strela );

    // nakonec alokujeme střely
    // (děláme to stejně jako se střelama pro Davida)
    duch->strely = calloc ( ZASOBNIK_STREL_DUCHA, sizeof ( Strela ) );
    if(!duch->strely)
    {
        free(duch);
        return NULL;
    }
    for ( size_t i =0; i<ZASOBNIK_STREL_DUCHA; i++ ) {
        duch->strely[i].druh = strela_ducha;
    }
    

    return duch;
}

// funkce na vykreslování ducha
void vykreslitDucha ( Duch * duch )
{
    // ducha budem vykreslovat jenom když je aktivní
    if ( !duch->aktivni ) {
        return;
    }


    // jestli duch chcípe, tak budem vykreslovat ten efekt umiraní
    if ( duch->chcipe ) {
        
        // vyrobíme si kopii vokrajů,
        // převedem si progress chcípání do rozsahu vod nuly do jedničky a invertujem to
        // noa tim budem scvrkávat velikost tý kopie vobdélnika vokrajů
        Rectangle _okraje = duch->okraje;
        const float chcipani = 1.0f - duch->relativni_cas/CHIPACI_CAS_DUCHA;
        _okraje.x += _okraje.width/2;
        _okraje.y += _okraje.height/2;
        _okraje.width *= chcipani;
        _okraje.height *= chcipani;
        
        // budeme texturu vykreslovat do tý scvrklý kopie vokrajů a střed textury posunem z defaultního
        // levýho horního rohu doprostředka. To děláme proto, že nastavujeme i úhel náklonu vykreslovaný textury noa
        // chcem, by střed rotace nebyl někde v tom rohu vobrázku, ale pěkně uprostřed 
        // barvu textury budem nastavovat pomocí raylibový funkce 'Fade', ta nám umožňuje měnit alfa složšku barvy (pro nás
        // teďko alfa znamená 'průsvitnost' resp. 'neviditelnost' tý barvy) ňákou hodnotou v rosahu nula až jedna
        // (duch se bude jakoby postupně zneviditelňovat jak bude chcípat)
        DrawTexturePro ( textura_duchove_spritesheet,duch->oblast_textury,_okraje,
                            ( Vector2 ) { _okraje.width/2.0f,_okraje.height/2.0f},
                            duch->uhel, Fade ( WHITE, 1.0f - duch->relativni_cas/CHIPACI_CAS_DUCHA ) );

    } else {
        // když duch nechcípe, vykreslíme ho uplně normálně
        DrawTexturePro ( textura_duchove_spritesheet,duch->oblast_textury,duch->okraje, ( Vector2 ) {0,0},0.0f, WHITE );
    }

#ifdef DEBUG
    // v připadě debugovávání vykreslíme okraje a hitbox
    DrawRectangleLines ( duch->okraje.x,duch->okraje.y, duch->okraje.width, duch->okraje.height,GREEN );
    DrawRectangleLines ( duch->hitbox.x,duch->hitbox.y, duch->hitbox.width, duch->hitbox.height,RED );
#endif
}

// funkce na aktualizovávání ducha
// jako argumenty bere pochopytelně ducha, pak mapu ve který se duch jakože pohybuje noa pak vobligátní časovou deltu
void aktualizovatDucha ( Duch * duch, Mapa * mapa, float dt )
{

    // když neni aktivní, tak ho nebudem aktualizovat
    if ( !duch->aktivni ) {
        return;
    }
    
    // jestli eště duch nechcípe ale má hitpointy na nule,
    // tak mu zapnem chcípání a začnem přehrávat zvuk chcípání
    if ( !duch->chcipe && duch->hp <= 0 ) {
        duch->chcipe = true;
        PlaySound ( duch->zvuk_chcipnuti );
    }

    // jestli duch chcípe, tak mu začnem měřit relativní čas, by sme věděli jestli už uplynula doba
    // potřebná na uplný chpípnutí ducha. Až ta doba uplyne, tak ducha uplně vypnem (nastavíme mu atribut 'aktivní' na false)
    if ( duch->chcipe ) {
        duch->relativni_cas += dt;
        
        // eště musíme při tom chcípání furt upravovat tu rotaci vykreslovaný textury
        // myslimže to bude takový zajimavější když se rychlost rotace bude s uplynulým časem furt zvěčovat 
        duch->uhel+= dt * ( 500 + 250 * duch->relativni_cas );
        if ( duch->relativni_cas > CHIPACI_CAS_DUCHA ) {
            duch->aktivni = false;
        }
        
        //noa jestli duch chcípe, tak už nás nezajímá žádný další dělání v tý jeho aktualizční funkci, už nikam nebude chodit ani střílet,
        // prostě z týdle funkce vyskočíme returnem
        return;
    }

    // malá pravděpodobnost že duch nečekaně změní směr
    if ( GetRandomValue ( 0,1000 ) == 1 ) {
        duch->smer*=-1;
        
        // překlopíme šiřku zdrojový textury, by se nám vykreslovala zrcadlově vobráceně
        // (podobně sme ďáli už v 'animace.h')
        duch->oblast_textury.width *= -1.0f;
    }

    // uplně stejně jako u davida budem počitat prepozici
    Rectangle prepozice = duch->okraje;
    
    // když de z leva doprava
    if ( duch->smer == 1 ) {
        
        // posunem prepozici vo rychlost ducha krát časová delta
        prepozice.x += RYCHLOST_CHUZE_DUCHA * dt;
        
        // a eště si spočitáme roh pravýho dolního vokraje prepozice a posunem ho
        // vo nějakejch pět pixelů dolu. Timdle bodem se budeme koukat, jestli má prepozice
        // pod sebou pevnou kostku mapy, noa jestli ne duch přišel na vokraj plošiny :O ;D
        Vector2 bod = ( Vector2 ) {
            prepozice.x + prepozice.width, prepozice.y + prepozice.height +5
        };

        // kouknem jestli nenastává kolize prepozice s blokem mapy, nebo jestli prepozice nekončí nad ďourou
        // v tom připadě votočíme ducha a příští aktualizací pude duch vopačným směrem
        if ( kolizeRectSeBlokemMapy ( prepozice, mapa ) || ! kolizeSeBlokemMapy_bod ( bod,mapa ) )
        {
            duch->smer *= -1;
            duch->oblast_textury.width *= -1.0f;
        } else {
            
            // jinak posunem ducha na prepozici
            duch->okraje.x += RYCHLOST_CHUZE_DUCHA * dt;
            duch->hitbox.x += RYCHLOST_CHUZE_DUCHA * dt;
        }
        
    // když de z prava doleva
    // vlastně to je zase uplně to samý jako pro chození  z leva doprava, jenom na vopačnou stranu :D ;D
    } else if ( duch->smer == -1 ) {
        prepozice.x -= RYCHLOST_CHUZE_DUCHA * dt;
        if ( kolizeRectSeBlokemMapy ( prepozice, mapa ) || ! kolizeSeBlokemMapy_bod ( ( Vector2 ) {
        prepozice.x, prepozice.y + prepozice.height +5
    },mapa ) ) {
            duch->smer *= -1;
            duch->oblast_textury.width *= -1.0f;
        } else {
            duch->okraje.x -= RYCHLOST_CHUZE_DUCHA * dt;
            duch->hitbox.x -= RYCHLOST_CHUZE_DUCHA * dt;
        }
    }

    // střílení je vlastně taky uplně stejný jako u davida, taky se počitá střilecí colldown, taky se vybírá
    // volná neaktivní střela, taky se recykluje, taky se ji nastavuje ňáká rychlost etc
    if ( duch->strileci_cooldown > 0.0f ) {
        duch->strileci_cooldown-=dt;
    } else {
        // jenom tady nemáme to chování nijak hráčem vovládaný
        // prostě budem střílet náhodně když uplyne cooldown tak budeme skoušet trefovat náhodný čislo tak dlouho
        // až nám padne naše vybraný noa v ten vokamžik duch zkusí vystřelit
        if ( GetRandomValue ( 0,200 ) == 1 ) {
            for ( size_t i =0; i<ZASOBNIK_STREL_DUCHA; i++ ) {
                if ( !duch->strely[i].aktivni ) {

                    // vyrobíme si střelu a nakopírujem jeji hodnoty na vodpovidajicí misto v zasobniku
                    Strela s = {

                        .druh = strela_ducha,
                        .rychlost = RYCHLOST_STRELY_DUCHA * ( float ) duch->smer,
                        .pozice = ( Vector2 ) { duch->smer==1? duch->okraje.x+DUCH_F_SIRKA/2.0f: duch->okraje.x,duch->okraje.y + 25},
                        .relativni_cas = 0.0f,
                        .doba_zivota = 2.0f,
                        .aktivni=true,
                    };
                    duch->strely[i] = s;

                    // zahrajeme zvuk výstřelu, nastavíme střílecí čekací čas a řerušíme hledací for cyklus
                    PlaySound ( duch->zvuk_strela );
                    duch->strileci_cooldown = STRILECI_COOLDOWN_DUCHA;
                    break;
                }
            }


        }
    }

}

// něco jako destruktor ducha
// musíme samozdřejmě uvolnit alokovaný struktury
// a taky aliasy zvuků
void freeDucha ( Duch * duch )
{
    UnloadSoundAlias ( duch->zvuk_chcipnuti );
    UnloadSoundAlias ( duch->zvuk_strela );
    
    free( duch->strely );
    free ( duch );
}

#endif

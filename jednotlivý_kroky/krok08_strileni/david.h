#ifndef _DAVID_A_DUCHOVE_DAVID_H_
#define _DAVID_A_DUCHOVE_DAVID_H_

// naimportujem si animaci
// (s ní se nám současně natáhne raylib.h)
#include "animace.h"
#include "mapa.h"
#include "projektily.h"

extern Sound zvuk_kroku;
extern Sound zvuk_skoku;
extern Sound zvuk_vystrel;

// přestěhujem si sem z 'main.c' framy animace by to tam zbytečně nezabiralo misto ve zdrojáčku 
#define DAVID_F_SIRKA 150.0f
#define DAVID_F_VYSKA 240.0f

Rectangle david_framy_behu[] = {
    {DAVID_F_SIRKA*0,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*1,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*2,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*3,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*4,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*5,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},

    {DAVID_F_SIRKA*0,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*1,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*2,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA}
};

Rectangle david_framy_skoku[] = {

    {DAVID_F_SIRKA*5,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},

    {DAVID_F_SIRKA*0,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*1,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*2,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA}


};

Rectangle david_framy_idle[] = {
    {DAVID_F_SIRKA*4,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*5,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*0,DAVID_F_VYSKA*3,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*1,DAVID_F_VYSKA*3,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*2,DAVID_F_VYSKA*3,DAVID_F_SIRKA,DAVID_F_VYSKA},
};

Rectangle david_framy_sed[] = {
    {DAVID_F_SIRKA*3,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*4,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*5,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*0,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*1,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*2,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*3,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
};

#define RYCHLOST_CHUZE_DAVIDA 350.0f
#define RYCHLOST_SKOKU_DAVIDA -15.0f
#define GRAVITACE_DAVIDA 40.0f

//přidáme si další tři hodnoty
// 1. rychlost davidovejch střel
#define RYCHLOST_STREL_DAVIDA 800.0f

// 2. dobu čekání mezi jednotlivejma výstřelama (čtvrt sekundy)
#define STRILECI_COOLDOWN_DAVIDA 0.25f

// 3. maximální počet střel, jakože max velikost zasobniku
#define POCET_STREL_DAVIDA_MAX 32

typedef struct David {
    
    // jednotlivý animace davida
    Animace animace_beh;
    Animace animace_idle;
    Animace animace_sed;
    Animace animace_skok;

    Animace * aktualni_animace;


    Vector2 pozice;
    
    Rectangle okraje;
    
    int smer;
    
    Mapa * mapa;

    bool zdaSkace;
    float vertikalni_rychlost;
    
    // zásobník střel, bude to ukazatel na 'pole' střel
    Strela * strely;
    // zbejvajicí čas do dalšího výstřelu
    float strileci_cooldown;

} David;


void aktualizovatDavida ( David * david, float dt )
{

    if ( IsKeyDown ( KEY_UP ) ) {

        if ( david->aktualni_animace == &david->animace_sed ) {
            david->aktualni_animace->pauznuta = false;
            david->aktualni_animace->reverzne=true;

            if ( david->aktualni_animace->index == 0 ) {
                david->aktualni_animace = & david->animace_idle;
            }

        } else if ( !david->zdaSkace ) {
            PlaySound ( zvuk_skoku );
            david->zdaSkace = true;
            david->animace_skok.index = 0;
            david->vertikalni_rychlost = RYCHLOST_SKOKU_DAVIDA;
        }
    }
    
    // když hráč zmáčkne mezernik, tak se David pokusí vystřelit z tý svý pistolky
    if ( IsKeyDown ( KEY_SPACE ) ) {
        // vystřelit mužeme jenom když uplynula čekací doba mezi výstřelama resp. střílecí cooldown je menší nebo rovnej nule
        if ( david->strileci_cooldown <= 0.0f ) {
            // projdeme si celej davidův zásobník a pokusíme se v něm najít střelu, kterou budeme moct použít
            // to poznáme tak, že bude mit atribut aktivní nastavenej na nulu
            for ( size_t i=0; i<POCET_STREL_DAVIDA_MAX; i++ ) {
                if ( !david->strely[i].aktivni ) {
                    
                    // pokud sme takovou střelu našli, tak si vybereme pro upravování atributů
                    Strela * s = david->strely + i;

                    // nejdřiv ji nastavíme novou polohu,
                    //to znamená přibližně někam na konec hlavně tý pistolky co má david v rukou
                    
                    //nj jenže david muže stát, sedět na zemi, muže koukat z prava doleva, nebo muže dokonce právě vstávat ze země
                    // všecky tydlecty eventualitky jakoby musíme pokrejt
                    if ( david->aktualni_animace != &david->animace_sed )
                        s->pozice = ( Vector2 ) {
                        // střelu umisťujem podle toho jakým směrem david kouká
                        david->smer==1? david->pozice.x+DAVID_F_SIRKA - 25: david->pozice.x+25,
                        david->pozice.y + DAVID_F_VYSKA/2 - 36
                    };
                    else {
                        // pokud david má jako aktuální animaci sedání, tak si zistíme index a vo ten budeme posouvat iksovou a ypsilonovou
                        // souřadnici střely. Neni to uplně přesný ale na to nikdo koukat nebude :D
                        int index = david->animace_sed.index;
                        s->pozice = ( Vector2 ) {
                            david->smer==1? david->pozice.x+DAVID_F_SIRKA - 25 - index*5: david->pozice.x+25 + index*5,david->pozice.y + DAVID_F_VYSKA/2 - 26 - 8 + index*14
                        };

                    }
                    
                    //polohu máme, teďko nastavíme další atributy střely
                    
                    // nastavíme střele rychlost, zohledníme i směr kterým poletí, ten vodpovídá
                    // směru kterým David právě teďko kouká
                    s->rychlost = RYCHLOST_STREL_DAVIDA * ( float ) david->smer;
                    
                    // vynulujem relativní čas
                    s->relativni_cas = 0.0f;
                    
                    // nastavíme dobu života třeba na čtyry vteřiny
                    s->doba_zivota = 4.0f;
                    
                    // a aktivujem
                    s->aktivni=true;
                    
                    // střelu máme upravenou, ukazatel už nepotřebujem
                    s=0;

                    // ..a když sme aktivovali střelu, tak sme vlastně vystřelili, takže nastavíme střílecí čekací dobu
                    // na maximální hodnotu
                    david->strileci_cooldown = STRILECI_COOLDOWN_DAVIDA;
                    
                    // zahrajem zvuk výstřelu
                    PlaySound ( zvuk_vystrel );
                    
                    // a přerušíme hledací for cyklus
                    break;
                }
            }
        }
        // eště upravíme aktuální animaci, pokud to neni animace sedání, tak přepnem animaci na idle na první snímek,
        // páč při tý animaci david hejbe pistolkou a vypadalo by to divně kdyby z ní vylítla vodorovně střela. Pokud david skáče,
        // běží nebo padá, tak se nám ta animace stejně přepne někde dál tady ve zdrojáčku týdle funkce
        // (při animaci běhu/skoků todle nepřectavuje problém, pistolka je na všech vobrázcích spritesheetu ve stejný vejšce,
        // kromě tý animace 'idle', tam je ve stejný vejšce právě jenom na prvním snimku)
        if ( david->aktualni_animace != &david->animace_sed ) {
            david->aktualni_animace = & david->animace_idle;
            david->aktualni_animace->index = 0;
            david->aktualni_animace->relativni_cas = 0.0f;
        }
    }


    
    if ( IsKeyDown ( KEY_LEFT ) && david->pozice.x > 0 && david->aktualni_animace != &david->animace_sed ) {
        
        david->smer = -1;
        
        Rectangle prepozice = david->okraje;
        prepozice.x -= RYCHLOST_CHUZE_DAVIDA * dt;

        if ( kolizeRectSeBlokemMapy ( prepozice, david->mapa ) ) {
            david->aktualni_animace = & david->animace_idle;
        } else {

            david->okraje.x = prepozice.x;
            david->pozice.x = david->okraje.x - 45;
            david->aktualni_animace = &david->animace_beh;
            }

        }
    
    else if ( IsKeyDown ( KEY_RIGHT ) && david->pozice.x < david->mapa->sirka*BLOK_SIRKA - DAVID_F_SIRKA && david->aktualni_animace != &david->animace_sed ) {

        david->smer = 1;
        Rectangle prepozice = david->okraje;
        prepozice.x += RYCHLOST_CHUZE_DAVIDA * dt;
        
        if ( kolizeRectSeBlokemMapy ( prepozice, david->mapa ) ) {
            david->aktualni_animace = & david->animace_idle;
        } else {
            david->okraje.x = prepozice.x;
            david->pozice.x = david->okraje.x - 45;
            david->aktualni_animace = & david->animace_beh;
            } 
    }
    // jestli je máčkutej na klávesnici čudlik šipky dolu, tak začnem přehrávat animaci sednutí
    // si na zadek
    else if ( IsKeyDown ( KEY_DOWN ) ) {

        david->aktualni_animace = & david->animace_sed;
        david->aktualni_animace->reverzne = false;
        david->aktualni_animace->pauznuta = false;
        }
        
    else if ( david->aktualni_animace != &david->animace_sed ) {
        david->aktualni_animace = & david->animace_idle;
        }
        
    Rectangle prepozice = david->okraje;
    prepozice.y += 5;
    if ( ! kolizeRectSeBlokemMapy ( prepozice, david->mapa ) ) {
        david->zdaSkace = true;
    }

    if ( david->zdaSkace ) {

        david->vertikalni_rychlost += dt * GRAVITACE_DAVIDA;
        
        Rectangle prepozice = david->okraje;
        prepozice.y += david->vertikalni_rychlost;

        if ( kolizeRectSeBlokemMapy ( prepozice, david->mapa ) ) {
            
            david->vertikalni_rychlost = 0.0f;
            david->zdaSkace = false;

            david->okraje.y =  ceil ( ( david->okraje.y + david->okraje.height  ) /BLOK_VYSKA ) * BLOK_VYSKA - david->okraje.height - 1;

            if ( david->aktualni_animace == &david->animace_beh ) {
                david->aktualni_animace->index = david->animace_skok.index +5;
                david->aktualni_animace->reverzne = david->animace_skok.reverzne;
            }

        } else {
            
            david->aktualni_animace = & david->animace_skok;
            
            david->okraje.y += david->vertikalni_rychlost;

        }

        david->pozice.y = david->okraje.y - 3;
        

    }

    david->aktualni_animace->zrcadlit = david->smer<0;


    if ( david->aktualni_animace == &david->animace_sed ) {
        if ( david->animace_sed.pauznuta ) {
            if ( david->animace_sed.index == 0 ) {
                david->aktualni_animace = & david->animace_idle;
            }
        }

    }

    else if ( david->aktualni_animace == &david->animace_beh ) {
        if ( !IsSoundPlaying ( zvuk_kroku ) ) {
            PlaySound ( zvuk_kroku );
        }
    }
    
    if ( david->aktualni_animace == &david->animace_skok && IsSoundPlaying ( zvuk_kroku ) ) {
        StopSound ( zvuk_kroku );
    }
    
    // odečteme časovou deltu vod zbejvajicí čekací doby mezi výstřelama
    if ( david->strileci_cooldown > 0.0f ) {
        david->strileci_cooldown -= dt;
    }
    
    // a aktualizujem si celej davidův zasobnik
    for ( size_t i=0; i<POCET_STREL_DAVIDA_MAX; i++ ) {
        aktualizovatStrelu ( david->strely+i, david->mapa,dt );
    }

    // nakonec aktualizujem aktuální animaci :D ;D
    aktualizovatAnimaci ( david->aktualni_animace, dt );

}

// funkce na vykreslování Davida
void vykreslitDavida ( David * david )
{
    vykreslitAnimaci ( david->aktualni_animace, david->pozice, WHITE );
    
    // vykreslíme střely
    for ( size_t i=0; i<POCET_STREL_DAVIDA_MAX; i++ ) {
        vykreslitStrelu ( david->strely+i );
    }

    // jestli je definovanej 'DEBUG', vykreslíme vokraje třeba zelenou barvičkou
#ifdef DEBUG
    DrawRectangleLines ( david->okraje.x, david->okraje.y, david->okraje.width, david->okraje.height, GREEN );
#endif
}

#endif

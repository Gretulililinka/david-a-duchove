#ifndef _DAVID_A_DUCHOVE_SEBRATELNE_H_
#define _DAVID_A_DUCHOVE_SEBRATELNE_H_

#include <raylib.h>
#include <math.h>
#include <stdlib.h>

extern Texture2D textura_ruzne;

// uděláme si tam taovej jednoduchej efekt tý sebratelný věci, rotaci kolem svý vosy
// todle je doba toho točení :O ;D
#define CAS_ROTACE_SEBRATELNE_VECI_MAX 3.0f

// budem mit dvě sebratelný věci, lečivý srdičko a magickou hvězdu
// pro voba dva druhy věcí budem používat stejnej voběkt/strukturu.
// By sme ale poznali, kterejže bonus z tědlech dvou má ta sebratelná věc reprezentovat,
// tak ji to nastavíme jako jako proměnou typu enum
// (podobně jako už nastavujem třeba druh střely)
enum DruhSebratelneVeci {SEBRATELNE_SRDICKO, SEBRATELNA_HVEZDA};

// voblasti podtextur srdička a magický hvězdy ve spritesheetu 'různé'
const Rectangle SRDICKO_TXTOBLAST_RECT = {2,503,96,83}, HVEZDA_TXTOBLAST_RECT = {2,758,74,80};

// struktura tý naší sebratelný věci
typedef struct Sebratelne
{
    // sebratelnej bonus musí mit ňáký vokraje, do kterejch David muže svým hitboxem vlízt a 
    // a jakoby ten bonus sebrat. Vykreslovací vobdelnik budem použivat na vykreslování efektu tý rotace
    Rectangle okraje, vykreslovaci_rect;
    
    // druh tý sebratelný věci (muže bejt srdičko nebo hvězda)
    enum DruhSebratelneVeci druh;
    
    // relativní čas
    // budem jim řídit tu rotaci vykreslovaný textury
    float relativni_cas;
    
    // voboba proměny aktivní u duchů nebo střel,
    // když bude bonus sebranej tak už ho nebudem vykreslovat ani aktualizovat
    bool sebrano;
}
Sebratelne;


void aktualizovatSebratelnouVec(Sebratelne * vec, float dt)
{
    // když už je věc Davidem sebraná tak ji nebudem aktualizovat
    if(vec->sebrano)
        return;
    
    // přičteme si deltu k relativnímu času a uděláme na něm modulo maximálním časem rotace,
    // takle zařídíme by byl náš čas furt v rosahu nula až maximální čas rotace, navíc se vyhnem
    // možnýmu přehoupnutí floatu zpátky někam na začátek až by nám třeba (nějak za strašně dlouho)
    // přetekla proměná
    // ( fmodf je funkce na dělání modula s floatama, z knihovny math.h )
    vec->relativni_cas += dt;
    vec->relativni_cas = fmodf(vec->relativni_cas, CAS_ROTACE_SEBRATELNE_VECI_MAX);
    
    // noa budem dělat rotaci toho vykreslovacího vobdelnika
    // chcem dosáhnout efektu rotace tý věci kolem svý vlastní vosy (podobně jako se točej třeba vypadlý věci v minecraftu
    // když je vyhodíme z inventáře). Toho dosáhnem tak že budem tomu vobdelniku podle uplynulýho postupně zmenčovat šiřku až
    // nebude mit vubec žádnou. Aby se nám ten vobelnik neposouval jakoby ke svýmu levýmu vokraji, tak musíme to zmenčování kompenzovat
    // vodpovidajicím posunem iksový souřadnice směrem doprava, by se jakože ten vobdelnik smrskával z vobou stran současně.
    // Noa jakmile se smrskne uplně někam na nulu, tak ho zase začnem zvěčovat.
    // Jenže chcem by se nám ta věc jakože točila kolem tý svý vosy, takže budem chtít by při tom zvěčování měl veskutečnosti zápornou
    // šířku, by se nám textura vykreslila převráceně.
    // Takže teďko sme si tu věc překlopili vo 180 stupňů, chcem ale 360 jakože uplně dokola, takže zase začnem zmenčovat ten překlopenej 
    // vobdelnik až na nulu a pak zase začnem zvěčovat vobdelnik z nuly na normální šiřku (a při tom furt kompenzovat ten iksovej posun)
    
    float pomer; // bude v rosahu -1 až 1, by sme s nim mohli překlápět šiřku vobdelnika
    
    // v první půlce času rotace budem překlápět z maximalní šiřky do minimální
    // (poměr pude pomaličku z jedničky na minus jedna)
    if(vec->relativni_cas <= CAS_ROTACE_SEBRATELNE_VECI_MAX/2)
    {
        pomer = 1.0f - (vec->relativni_cas / (CAS_ROTACE_SEBRATELNE_VECI_MAX/2))*2.0f;
    }
    
    // v druhý půlce času pudem navopak vod minus jedničky k jedničce
    else
    {        
        pomer = 1.0f - ((vec->relativni_cas - (CAS_ROTACE_SEBRATELNE_VECI_MAX/2)) / (CAS_ROTACE_SEBRATELNE_VECI_MAX/2))*2.0f;
        pomer*=-1.0f;
    }
    // (sem to takle vyifovala ale de to řešit i čistě matematikózně)
    
    vec->vykreslovaci_rect = (Rectangle){
            // absolutní hotnotou dosahnem toho že to měnění iksovýho posunu bude mit jakoby 2x víc věčí 'frekvenci' než
            // překlápětí šiřky ( za dobu jednoho uplnýho překlopení textury uděláme 2x tam a zpátky tim iksovým posunem)
            .x = vec->okraje.x + vec->okraje.width/2.0f - vec->okraje.width/2.0f * fabsf(pomer),
            .y = vec->okraje.y,
            .width = vec->okraje.width * pomer,
            .height = vec->okraje.height,
    };

}


void vykreslitSebratelnouVec(Sebratelne * vec)
{
    if(vec->sebrano)
        return;
    // texturu sebratelný věci vyberem podle atrubutu 'druh'
    switch(vec->druh)
    {
        case SEBRATELNE_SRDICKO:
        {
            Rectangle src = SRDICKO_TXTOBLAST_RECT;
            // (raylib nechce texturu ve funkci DrawTexturePro překlápět když neni záporná šířka ve zdrojovým rectanglu 'src',
            // na překlopenou šiřku v rectanglu 'dest' neregauje, myslimže to je bug. pokud se ale pletu a ukaže se žeto je feature
            // nóó tak se ke zdrojačku vrátim a upravim ho by v aktualizovávací funkci využival src vobdelnik)
            if(vec->vykreslovaci_rect.width < 0.0f)
            {
                vec->vykreslovaci_rect.width*=-1.0f;
                src.width *= -1.0f;
            }
            DrawTexturePro(textura_ruzne,src,vec->vykreslovaci_rect,(Vector2){0,0},0.0f,WHITE);
            break;
        }
        case SEBRATELNA_HVEZDA:
        {
            Rectangle src = HVEZDA_TXTOBLAST_RECT;
            if(vec->vykreslovaci_rect.width < 0.0f)
            {
                vec->vykreslovaci_rect.width*=-1.0f;
                src.width *= -1.0f;
            }
             DrawTexturePro(textura_ruzne,src,vec->vykreslovaci_rect,(Vector2){0,0},0.0f,WHITE);
            break;
        }
        default:
            break;
    }
    
#ifdef DEBUG
            DrawRectangleLines(vec->okraje.x,vec->okraje.y,vec->okraje.width,vec->okraje.height,GREEN);
#endif
}

Sebratelne * vygenerovatSebratelnouVec(Vector2 kde, enum DruhSebratelneVeci druh)
{
    Sebratelne * s = calloc(1,sizeof(Sebratelne));
    if(!s)
        return NULL;
    
    // vejšku a šiřku sebratelný věci nastavíme na velikost jednoho bloku dlaždicový mapy
    // textury sou +- čtvercový a ňáký malý nepřesnosti se snad stratěj :O ;D
    s->okraje = (Rectangle){kde.x,kde.y,BLOK_SIRKA,BLOK_VYSKA};
    s->vykreslovaci_rect = (Rectangle){kde.x,kde.y,BLOK_SIRKA,BLOK_VYSKA};
    s->druh = druh;
    return s;
}

#endif

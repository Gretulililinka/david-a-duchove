#ifndef _DAVID_A_DUCHOVE_DUCH_H_
#define _DAVID_A_DUCHOVE_DUCH_H_

#include <stdlib.h>
#include <raylib.h>

#include "mapa.h"
#include "projektily.h"

extern Texture2D textura_duchove_spritesheet;

extern Sound zvuk_duch_chcip;
extern Sound zvuk_duch_strela;

//přidáme si sem zvuk toho vejskání
extern Sound zvuk_zaghrouta;

#define HITPOINTU_DUCHA 3
#define CHIPACI_CAS_DUCHA 1.5f
#define ZASOBNIK_STREL_DUCHA 3
#define STRILECI_COOLDOWN_DUCHA 1.0f
#define RYCHLOST_STRELY_DUCHA 600.0f
#define RYCHLOST_CHUZE_DUCHA 120.0f

// definujem si počateční vertikální rychlost burkinátora
#define RYCHLOST_BURKINATORA 200.0f

#define DUCH_F_VYSKA 240.0f
#define DUCH_F_SIRKA 85.0f

typedef struct Duch {
    
    // směr kterým duch kouká, taky identicky řešený jako v davidoj
    int smer;
    
    // hp jakože hitpointy jakože počet životů
    int hp;

    Rectangle oblast_textury;
    Rectangle okraje;
    Rectangle hitbox;

    // jestli je duch aktivní
    // pokud neni, tak ho nebudem vykreslovat ani aktualizovat, prostě bude upně vyplej
    bool aktivni;
    
    // jestli duch chícpe, jakože mu už došly hitpointy a máme za
    bool chcipe;

    // vnitřní čas ducha
    float relativni_cas;
    
    // uhel ducha pro animaci efektu chcípání
    float uhel;
    
    // střílecí cooldown ducha
    float strileci_cooldown;

    
    // zásobnik střel ducha
    Strela * strely;

    // zvuky ducha
    Sound zvuk_chcipnuti;
    Sound zvuk_strela;
}
Duch;


Duch * vygenerovatDucha ( Vector2 kde )
{

    Duch * duch = calloc ( 1, sizeof ( Duch ) );
    if(!duch)return NULL;
    
    duch->smer = GetRandomValue( 0,1 ) ? 1 : -1;
    
    duch->okraje = ( Rectangle ) {
        kde.x,kde.y,DUCH_F_SIRKA,DUCH_F_VYSKA
    };

    duch->hitbox = ( Rectangle ) {
        kde.x+15,kde.y+15,55,155
    };
    
    // oblast textury určíme náhodně, vybereme uplně náhodnej podvobrázek
    duch->oblast_textury = ( Rectangle ) {
        DUCH_F_SIRKA * GetRandomValue( 0,5 ),0 + DUCH_F_VYSKA * GetRandomValue( 0,1 ),DUCH_F_SIRKA * duch->smer,DUCH_F_VYSKA
    };
    
    duch->aktivni = true;
    duch->hp = HITPOINTU_DUCHA;
    
    duch->zvuk_chcipnuti = LoadSoundAlias ( zvuk_duch_chcip );
    duch->zvuk_strela = LoadSoundAlias ( zvuk_duch_strela );

    duch->strely = calloc ( ZASOBNIK_STREL_DUCHA, sizeof ( Strela ) );
    if(!duch->strely)
    {
        free(duch);
        return NULL;
    }
    for ( size_t i =0; i<ZASOBNIK_STREL_DUCHA; i++ ) {
        duch->strely[i].druh = strela_ducha;
    }
    

    return duch;
}

// funkce na vykreslování ducha
void vykreslitDucha ( Duch * duch )
{
    // ducha budem vykreslovat jenom když je aktivní
    if ( !duch->aktivni ) {
        return;
    }


    // jestli duch chcípe, tak budem vykreslovat ten efekt umiraní
    if ( duch->chcipe ) {
        
        Rectangle _okraje = duch->okraje;
        const float chcipani = 1.0f - duch->relativni_cas/CHIPACI_CAS_DUCHA;
        _okraje.x += _okraje.width/2;
        _okraje.y += _okraje.height/2;
        _okraje.width *= chcipani;
        _okraje.height *= chcipani;
        
        DrawTexturePro ( textura_duchove_spritesheet,duch->oblast_textury,_okraje,
                            ( Vector2 ) { _okraje.width/2.0f,_okraje.height/2.0f},
                            duch->uhel, Fade ( WHITE, 1.0f - duch->relativni_cas/CHIPACI_CAS_DUCHA ) );

    } else {
        // když duch nechcípe, vykreslíme ho uplně normálně
        DrawTexturePro ( textura_duchove_spritesheet,duch->oblast_textury,duch->okraje, ( Vector2 ) {0,0},0.0f, WHITE );
    }

#ifdef DEBUG
    // v připadě debugovávání vykreslíme okraje a hitbox
    DrawRectangleLines ( duch->okraje.x,duch->okraje.y, duch->okraje.width, duch->okraje.height,GREEN );
    DrawRectangleLines ( duch->hitbox.x,duch->hitbox.y, duch->hitbox.width, duch->hitbox.height,RED );
#endif
}

// funkce na aktualizovávání ducha
// jako argumenty bere pochopytelně ducha, pak mapu ve který se duch jakože pohybuje noa pak vobligátní časovou deltu
void aktualizovatDucha ( Duch * duch, Mapa * mapa, float dt )
{

    if ( !duch->aktivni ) {
        return;
    }
    
    if ( !duch->chcipe && duch->hp <= 0 ) {
        duch->chcipe = true;
        PlaySound ( duch->zvuk_chcipnuti );
    }

    if ( duch->chcipe ) {
        duch->relativni_cas += dt;
        
        duch->uhel+= dt * ( 500 + 250 * duch->relativni_cas );
        if ( duch->relativni_cas > CHIPACI_CAS_DUCHA ) {
            duch->aktivni = false;
        }

        return;
    }

    if ( GetRandomValue ( 0,1000 ) == 1 ) {
        duch->smer*=-1;
        
        duch->oblast_textury.width *= -1.0f;
    }

    Rectangle prepozice = duch->okraje;
    
    if ( duch->smer == 1 ) {
        
        prepozice.x += RYCHLOST_CHUZE_DUCHA * dt;
        Vector2 bod = ( Vector2 ) {
            prepozice.x + prepozice.width, prepozice.y + prepozice.height +5
        };

        if ( kolizeRectSeBlokemMapy ( prepozice, mapa ) || ! kolizeSeBlokemMapy_bod ( bod,mapa ) )
        {
            duch->smer *= -1;
            duch->oblast_textury.width *= -1.0f;
        } else {
            
            duch->okraje.x += RYCHLOST_CHUZE_DUCHA * dt;
            duch->hitbox.x += RYCHLOST_CHUZE_DUCHA * dt;
        }
        

    } else if ( duch->smer == -1 ) {
        prepozice.x -= RYCHLOST_CHUZE_DUCHA * dt;
        if ( kolizeRectSeBlokemMapy ( prepozice, mapa ) || ! kolizeSeBlokemMapy_bod ( ( Vector2 ) {
        prepozice.x, prepozice.y + prepozice.height +5
        },mapa ) ) {
            duch->smer *= -1;
            duch->oblast_textury.width *= -1.0f;
        } else {
            duch->okraje.x -= RYCHLOST_CHUZE_DUCHA * dt;
            duch->hitbox.x -= RYCHLOST_CHUZE_DUCHA * dt;
        }
    }


    if ( duch->strileci_cooldown > 0.0f ) {
        duch->strileci_cooldown-=dt;
    } else {

        if ( GetRandomValue ( 0,200 ) == 1 ) {
            for ( size_t i =0; i<ZASOBNIK_STREL_DUCHA; i++ ) {
                if ( !duch->strely[i].aktivni ) {

                    Strela s = {

                        .druh = strela_ducha,
                        .rychlost = RYCHLOST_STRELY_DUCHA * ( float ) duch->smer,
                        .pozice = ( Vector2 ) { duch->smer==1 ? duch->okraje.x+DUCH_F_SIRKA/2.0f : duch->okraje.x,duch->okraje.y + 25},
                        .relativni_cas = 0.0f,
                        .doba_zivota = 2.0f,
                        .aktivni=true,
                    };
                    duch->strely[i] = s;

                    PlaySound ( duch->zvuk_strela );
                    duch->strileci_cooldown = STRILECI_COOLDOWN_DUCHA;
                    break;
                }
            }


        }
    }

}

// 'konstruktor' burkinátora
Duch * vygenerovatBurkinatora ( Vector2 kde )
{
    Duch * duch = ( Duch * ) calloc ( 1, sizeof ( Duch ) );
    if(!duch)
        return NULL;
    
    // směr nám řídí jenom vykreslování textury
    // (burkinátor bude lítat zezdol nahoru, tak jakej jako směr do stran)
    duch->smer = GetRandomValue ( 0,1 ) ? 1 : -1;
    duch->okraje = ( Rectangle ) {
        kde.x,kde.y,DUCH_F_SIRKA,DUCH_F_VYSKA
    };
    duch->hitbox = ( Rectangle ) {
        kde.x+15,kde.y+15,55,160
    };

    // by sme burkinátora vod vostatních duchů vodlišili, tak bude mit texturu jenom ducha v burce
    // sou tam na víběr dva různý, jeden v černým hadru, druhej  v bílým
    // z tědlech dou textur si pokaždý nahodně vyberem
    const Rectangle moznosti[2]= {
        ( Rectangle ) {DUCH_F_SIRKA*3,0,DUCH_F_SIRKA,DUCH_F_VYSKA},
        ( Rectangle ) {DUCH_F_SIRKA*2,DUCH_F_VYSKA,DUCH_F_SIRKA,DUCH_F_VYSKA},
    };
    duch->oblast_textury = moznosti[GetRandomValue ( 0,1 )];
    
    duch->aktivni = true;
    duch->hp = HITPOINTU_DUCHA;
    duch->zvuk_chcipnuti = LoadSoundAlias ( zvuk_duch_chcip );
    
    // mistu zvuku střely nakopírujem alias zvuku tamtoho terroristickýho vejskání
    duch->zvuk_strela = LoadSoundAlias ( zvuk_zaghrouta );


    // burkinátor nebude mít žádný střely
    duch->strely = NULL;

    return duch;
}

// burkinátor nebude řešit žádný kolize s ďourama a blokama, takže voproti vobyčejnýmu duchoj nepotřebuje znát mapu
// bude ale potřebovat znát iksovou pozici Davida, by burkinátor věděl kdy na Davida vybafnout
void aktualizovatBurkinatora ( Duch * duch, float x_pozice_cile,  float dt )
{

    if ( !duch->aktivni ) {
        return;
    }
    
    // tudle chcípací část kódu budem mit uplně stejnou jako u vobyč ducha
    if ( !duch->chcipe && duch->hp <= 0 ) {
        duch->chcipe = true;
        PlaySound ( duch->zvuk_chcipnuti );
        duch->relativni_cas = 0.0f;
    }

    if ( duch->chcipe ) {
        duch->relativni_cas += dt;
        duch->uhel+= dt * ( 500 + 250 * duch->relativni_cas );
        if ( duch->relativni_cas > CHIPACI_CAS_DUCHA ) {
            duch->aktivni = false;
        }
        return;
    }

    // pokud je pozice cíle blíž jak dýlka jeden a půl herní kostky, tak burkinátor začne letět nahoru
    // (stav, že má letět budem hlídat tim, že se kouknem jestli je burkinátorova vypsilonová souřadnice menčí než výchozí,
    // proto ten operátor 'or' s tou vejškou menčí než 960 (to ňákejch dvanáct kostek, takže spodek mapy))
    if ( fabsf ( x_pozice_cile - duch->okraje.x + duch->okraje.width/2 ) < BLOK_SIRKA*1.5 || duch->okraje.y < 959.9999f ) {
        
        // přičtem desetinu dt k relativnímu času (desetinu páč to rostlo moc rychle :D :D)
        duch->relativni_cas += dt/10;
        
        // a tim relativním časem vynasobeným rychlostí posuneme ducha nahoru
        // (děláme to takle, by burkinátor zrychloval )
        duch->okraje.y-= RYCHLOST_BURKINATORA*duch->relativni_cas;
        duch->hitbox.y = duch->okraje.y+15;
        if ( !IsSoundPlaying ( duch->zvuk_strela ) ) {
            PlaySound ( duch->zvuk_strela );
        }
    }

    // pokud duch vylít dostatečně vysoko, tak ho 'teleportujeme' zpátkydolu do ďoury
    // vynulujeme mu realtivní čas, vypneme zvuk vejskání noa vlastně ho necháme znova číhat na Davida
    if ( duch->okraje.y + DUCH_F_VYSKA <= -800 ) {
        duch->okraje.y = 960;
        duch->hitbox.y = duch->okraje.y+15;
        duch->relativni_cas = 0.0f;
        StopSound ( duch->zvuk_strela );
    }
}


// funguje i na burkinátory
void freeDucha ( Duch * duch )
{
    UnloadSoundAlias ( duch->zvuk_chcipnuti );
    UnloadSoundAlias ( duch->zvuk_strela );
    
    free( duch->strely );
    free ( duch );
}

#endif

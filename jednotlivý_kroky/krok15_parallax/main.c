// naimportujem si knihovny
#include <math.h>
#include <stdlib.h>
#include <raylib.h>
#include <time.h>

// Pokuď vodkomentujeme, tak to zkompiluje preprocesorovou podmínkou vypnutý věci
// napřiklad se kolem některejch herních voběktů budou vykreslovat okraje
// #define DEBUG

// naimportujem si vlastní hlavičky
#include "animace.h"
#include "david.h"
#include "projektily.h"
#include "level.h"

//makra na zišťování minimální a maximální hodnoty
#ifndef MAX
#define MAX(a, b) ((a)>(b)? (a) : (b))
#define MIN(a, b) ((a)<(b)? (a) : (b))
#endif

// šířka a výška vokna
#define HERNI_SIRKA 1280
#define HERNI_VYSKA 960

// kolik kostek bude dlouhá naše herní mapa
#define DELKA_LEVELU_BLOKU 100

// textury
Texture2D textura_mesic;
Texture2D textura_david_spritesheet;
Texture2D textura_kameny;
Texture2D textura_duchove_spritesheet;
Texture2D textura_ruzne;
Texture2D textura_hrad;

// zvuky
Sound zvuk_kroku;
Sound zvuk_skoku;
Sound zvuk_vystrel;
Sound zvuk_duch_chcip;
Sound zvuk_duch_strela;
Sound zvuk_zasah;
Sound zvuk_kontakt;
Sound zvuk_padu;
Sound zvuk_zaghrouta;
Sound zvuk_powerup;

// hudby
// máme zatim dvě, hudba_lvl jakože levelu, která bude normálně hrát na pozadí,
// druhá je hudba která bude hrát když David sebere bonus
Music hudba_lvl;
Music hudba_bonus;

//ukazatel na právě hranou hudbu
Music * hudba_aktualni;

int main ( void )
{
    // nastavíme generátor nahodnejch čisel nějakým seedem
    // (vobvykle se tam strká aktualní čas ale mužeme si tam dát
    // třeba ňákou konstantu by sme to měli vopakovatelný a mohli reprodukovat stejnej level)
    SetRandomSeed ( time ( 0 ) );

    SetConfigFlags ( FLAG_WINDOW_RESIZABLE | FLAG_VSYNC_HINT );

    InitWindow ( HERNI_SIRKA, HERNI_VYSKA, "David a duchove" );

    InitAudioDevice();

    SetTargetFPS ( 60 );

    // načtem soubory textur
    textura_david_spritesheet = LoadTexture ( "assets/david.png" );
    textura_mesic = LoadTexture ( "assets/moon.png" );
    textura_kameny = LoadTexture ( "assets/kameny.png" );
    textura_duchove_spritesheet = LoadTexture ( "assets/duchove.png" );
    textura_ruzne = LoadTexture ( "assets/misc.png" );
    textura_hrad = LoadTexture ( "assets/hrad2.png" );
    
    // načtem zvuky
    zvuk_kroku = LoadSound ( "assets/kroky.wav" );
    zvuk_skoku = LoadSound ( "assets/skok.wav" );
    zvuk_vystrel = LoadSound ( "assets/bum.wav" );
    zvuk_duch_chcip = LoadSound ( "assets/duch_chcip.wav" );
    zvuk_duch_strela = LoadSound ( "assets/duch_strela.wav" );
    zvuk_zasah = LoadSound ( "assets/zasah.wav" );
    zvuk_kontakt = LoadSound ( "assets/kontakt.wav" );
    zvuk_padu = LoadSound ( "assets/pad.wav" );
    zvuk_zaghrouta = LoadSound ( "assets/zaghrouta.ogg" );
    zvuk_powerup = LoadSound ( "assets/powerup.wav" );
    
    //načtem hudby
    hudba_lvl = LoadMusicStream ( "assets/waltz_of_the_ghosts.ogg" );
    hudba_bonus = LoadMusicStream ( "assets/for_a_few_shekels_more_band.ogg" );
    
    Camera2D kamera = {
        
        //posun 'středu' kamery, posunem na střed vobrazovky
        .offset = ( Vector2 ) { GetRenderWidth() / 2.0f, GetRenderHeight() / 2.0f },
        // souřadnice cíle, na co jakože kamera kouká
        .target = ( Vector2 ) {0,0},
        // uhel náklonu kamery ve stupních
        .rotation = 0.0f,
        //přiblížení
        .zoom = 1.0f
    };
    
    //ukazatel, kde si budeme držet vygenerovanej level
    Level * level = NULL;
    // ukazatel, kterej bude držet 'pole' davidovejch střel
    Strela * david_strely = NULL;
    
    //vygenerujeme si herní level
    level = vygenerovatLevel ( DELKA_LEVELU_BLOKU,textura_kameny );
    
    //alokujeme si 'pole' střel pomocí funkce calloc (se vod malloc liší tim, že nám alokovanou paměť vynuluje,
    // první argument je počet alokovanejch struktur, druhej velikost jedný tý struktury)
    david_strely = calloc ( POCET_STREL_DAVIDA_MAX,sizeof ( Strela ) );
    
    // animace běhu
    Animace david_beh = {
        .trvani_framu = 1.0f/30.0f, .relativni_cas=0.0f, .index = 0,
        .jojo = true, .reverzne = false, .zrcadlit = false, .loopovat = true,
        .textura = textura_david_spritesheet, .framy = david_framy_behu, .pocet_framu = sizeof ( david_framy_behu ) /sizeof ( Rectangle )
    };
    // animace idle, jakože když se fláká a nic nedělá. Je to takový pérování nohama na místě
    Animace david_idle = {
        .trvani_framu = 1.0f/15.0f, .relativni_cas=0.0f, .index = 0,
        .jojo = true, .reverzne = false, .zrcadlit = false, .loopovat = true,
        .textura = textura_david_spritesheet, .framy = david_framy_idle, .pocet_framu = sizeof ( david_framy_idle ) /sizeof ( Rectangle )
    };

    Animace david_sed = {
        .trvani_framu = 1.0f/30.0f, .relativni_cas=0.0f, .index = 0,
        .jojo = true, .reverzne = false, .zrcadlit = false, .loopovat = false,
        .textura = textura_david_spritesheet, .framy = david_framy_sed, .pocet_framu = sizeof ( david_framy_sed ) /sizeof ( Rectangle )
    };

    // animace skoku, david tam vicemeně jenom máchá nožičkama ve vzduchu
    Animace david_skok = {
        .trvani_framu = 1.0f/10.0f, .relativni_cas=0.0f, .index = 0,
        .jojo = true, .reverzne = false, .zrcadlit = false, .loopovat = true,
        .textura = textura_david_spritesheet, .framy = david_framy_skoku, .pocet_framu = sizeof ( david_framy_skoku ) /sizeof ( Rectangle )
    };
    
    
    // kdyžuž máme vyrobený animace, tak si mužeme vyrobit Davida
    David david = {
                    
        .animace_beh = david_beh,
        .animace_idle = david_idle,
        .animace_sed = david_sed,
        .animace_skok = david_skok,
         
        .aktualni_animace = NULL,

        // necháme ho na herní mapu spadnou z vejšky
        .pozice = {0,0},
                    

        .smer = 1,
        
        // nastavíme mapu na tu skovanou ve struktuře levelu
        .mapa = level->dlazdicova_mapa,
        
        .vertikalni_rychlost = 0.0f,
        .zdaSkace = true,
        
        // nastavíme ukazatel střel na ty naše callocem vygenerovaný střely
        .strely = david_strely,
                    
        //vynulujeme davidovy vnitřní časovače střílecího cooldownu a času blikání resp. dočasný
        // nezranitelnosti po nepřátelským zásahu ie. islamistickým duchem nebo rudou kulkou
        .strileci_cooldown = 0.0f,
        .blikaci_cas = 0.0f,
                    
        // nastavíme počet životů na max a atribut zranitelnosti na true
        .zivoty = POCET_ZIVOTU_DAVIDA_MAX,
        .zranitelny = true,
        
        // nastavíme atributy vokolo bonusu
        .ma_bonus = false,
        .hvezdny_bonus_cas = 0.0f,

    };

    // nastavíme ukazatel aktuální animace na animaci 'idle'
    david.aktualni_animace = &david.animace_idle;
                
    // podle aktuální pozice nastavíme okraje oběktu a teďko nově i hitbox
    david.okraje = ( Rectangle ) {
        david.pozice.x + 45, david.pozice.y +8, DAVID_F_SIRKA -90, DAVID_F_VYSKA - 10
    };
    david.hitbox = ( Rectangle ) {
        david.pozice.x + 55, david.pozice.y +20, DAVID_F_SIRKA -110, DAVID_F_VYSKA-30
    };
    
    // nastavíme aktuální hudbu
    hudba_aktualni = &hudba_lvl;
    
    // zapnem normální hudbu levelu
    PlayMusicStream( hudba_lvl );
    
    
    while ( !WindowShouldClose() ) {

        float dt = GetFrameTime();
        
        //aktualizujeme hudbu, na kterou ukazuje ukazatel 'hudba_aktualni'
        UpdateMusicStream ( *hudba_aktualni );
        
        // spočitáme si přiblížení naší kamery
        // uděláme to tak, že si spočitáme poměr skutečný šířky obrazovky s naší 'virtuální' požadovanou,
        // to samý uděláme se skutečnou a požadovanou vejškou, noa vybereme tu menší hodnotu
        // (jak se to chová si mužeme vyzkoušet behem hry, když budeme ruzně měnit velikost vokna)
        const float priblizeni = MIN ( ( float ) GetRenderWidth() / HERNI_SIRKA, ( float ) GetRenderHeight() / HERNI_VYSKA );

        // nastavíme atribut 'zoom' tou naší spočitanou hodnotou
        kamera.zoom = priblizeni;
        //nastavíme posun kamery na velikost půlky vobrazovky
        kamera.offset = ( Vector2 ) {
            GetScreenWidth() /2, GetScreenHeight() /2
        };
        
        //aktualizujem Davida
        aktualizovatDavida(&david, dt);
        
        //aktualizujem level
        aktualizovatLevel ( level, &david, dt );
        
        // nastavíme cíl kamery na střed davida
        kamera.target = ( Vector2 ) {
            david.okraje.x + david.okraje.width / 2.0f, david.okraje.y + david.okraje.height / 2.0f
        };
        
        const float kamera_target_min_x = GetRenderWidth() / 2.0f / priblizeni;
        const float kamera_target_max_x = DELKA_LEVELU_BLOKU * BLOK_SIRKA - GetRenderWidth() /2/priblizeni;
        const float kamera_target_max_y = GetRenderHeight() / 2.0f / priblizeni - DAVID_F_VYSKA + BLOK_VYSKA*5;

        // pohlídáme si ty minimální a maximální možný hodnoty
        kamera.target.x = MAX ( kamera.target.x, kamera_target_min_x );
        kamera.target.x = MIN ( kamera.target.x, kamera_target_max_x );
        kamera.target.y = MIN ( kamera.target.y, kamera_target_max_y );

        // zapnem vykreslování
        BeginDrawing();

        // vykreslíme ten gradient
        DrawRectangleGradientV ( 0,0,GetScreenWidth(),GetScreenHeight(),DARKBLUE,BLACK );
        
        // první kus našeho parallaxu
        // hovorka se asi jako bojí děsně velkejch měsiců když je furt v horrorovejch komixech kreslí,
        // tak mu uděláme radost na na pozadí vykreslíme supr strašidelnej měsíc :D :D
        float mensi_rozmer = MIN ( GetRenderWidth(),GetRenderHeight() );
        DrawTexturePro ( textura_mesic, ( Rectangle ) {
            0,0,textura_mesic.width,textura_mesic.height
        }, ( Rectangle ) {
            GetRenderWidth() /2,GetRenderHeight() /2,mensi_rozmer*1.5, mensi_rozmer*1.5
        }, ( Vector2 ) {
            mensi_rozmer*1.5/2,mensi_rozmer*1.5/2
        },0, ( Color ) {
            102, 191, 255, 128
        } );
        
        // aktivujem transformování tou naší kamerou
        // takže jakoby vykreslujem to, co kamera vidí
        BeginMode2D ( kamera );
        
        //vykreslíme parallax

        // to je jakože takový pozadí, který se různě pomaličku posouvá, když se hráč pohybuje na mapě a vytváří to
        // víc lepší iluzi že se hráč jakože někam pohybuje
        // todle je zatim dělaný jentak na hrubo, vykresluje to takovou siluletu jeruzalémskýho hradu která se děsně pomaličku posouvá
        // když David poskakuje nebo de dopředu


        // velikost tý textury hradu
        Rectangle hrad_zdroj = {0,0,1024,512};
        
        // cílovej vobdelnik hradu
        // uděláme ho 2x delší než je šířka vobrazovky a budem 
        // (asi to nebude moc dobře fungovat na uzkejch vobrazovkách, tam by sme asi jako museli zvolit
        // ňákej uplně jinej koncept, nvm)
        Rectangle hrad_cil = {
            .x = kamera.target.x - GetRenderWidth() /priblizeni,
            .y = kamera.target.y - GetRenderWidth() /2/priblizeni,
            // vodelnik taky bude muset bejt 2x širší než vyšší, by se nám moc nezdeformovala textura
            .width = GetRenderWidth() /priblizeni*2,
            .height = GetRenderWidth() /2/priblizeni*2,
        };
        
        // počátek/origin toho velkýho rectanglu budem určovat z mezí vobrazovky
        // musíme si pohlídat když hráč stojí u kraje mapy by nám tam nevylezla někam doprostředka vobrazovky hrana textury :D
        float hrad_y_posun_paralax = -300.0f + 300.0f * kamera.target.y/kamera_target_max_y;
        float hrad_x_posun_paralax = -GetRenderWidth() /priblizeni/2 + GetRenderWidth() /priblizeni * ( kamera.target.x - kamera_target_min_x ) /kamera_target_max_x;

        // noa vykreslíme ten náš parallax
        DrawTexturePro ( textura_hrad, hrad_zdroj, hrad_cil, ( Vector2 ) {
            hrad_x_posun_paralax,hrad_y_posun_paralax
        },0,WHITE );

        // vykreslíme level
        vykreslitLevel( level, &kamera);
        
        // vykreslíme davida
        vykreslitDavida(&david);
        
        DrawRectangleGradientV ( kamera.target.x - GetRenderWidth() / 2 / priblizeni,BLOK_VYSKA*10, GetRenderWidth()/priblizeni,BLOK_VYSKA*3,BLANK, BLACK );
        // a pod tim všecko vyčerníme černým vodelnikem, kterej hezky navazuje na ten náš černej gradient
        DrawRectangle ( kamera.target.x - GetRenderWidth() /2/priblizeni,BLOK_VYSKA*13,GetRenderWidth() /priblizeni,GetRenderHeight()/priblizeni,BLACK );
        
        // vypneme kameru
        EndMode2D();
        
        // nakreslíme HUD - takový to herní menu co ukazuje počet životů, skóre a tak
        // (nám to vykresluje jenom počet životů)
        // po tom co sme vypli kameru, malujeme bez tý kamerový transformace, takže nebudem
        // mit problem trefit levej spodní vokraj vobrazovky kde si budeme malovat
        // takovou řadu srdíček který budou jakože ukazovat kolik Davidoj zbejvá životů
        
        // zrojová voblast srdička ve spritesheetu 'různé'
        const Rectangle srdicko_rect = {2,503,96,83};
        
        // srdíček budem vykreslovat v řadě za sebou furt stejnej maximální počet,
        // akorát jenom když budem vykreslovat ty který by přesahovali aktualní počet Davidovejch
        // životů, tak je budeme malovat černý
        for ( int i = 0; i < POCET_ZIVOTU_DAVIDA_MAX; i++ ) {
            Rectangle cil = {
                .x = i*srdicko_rect.width/2,
                .y = GetScreenHeight() - srdicko_rect.height/2,
                .width = srdicko_rect.width/2,
                .height = srdicko_rect.height/2,
            };
            DrawTexturePro ( textura_ruzne,srdicko_rect,cil, ( Vector2 ) {
                0,0
            },0.0f,i<david.zivoty? WHITE:BLACK );
        }

        // a skončíme s vykreslováním by se naše scéna poslala na monitor
        EndDrawing();
    }

    CloseWindow();
    
    // uvolníme naše vlastní struktury
    if ( david_strely ) {
        free ( david_strely );
    }
    if ( level ) {
        freeLevel ( level );
    }

    //uklidíme textury
    UnloadTexture ( textura_mesic );
    UnloadTexture ( textura_david_spritesheet );
    UnloadTexture ( textura_kameny );
    UnloadTexture ( textura_duchove_spritesheet );
    UnloadTexture ( textura_ruzne );
    UnloadTexture ( textura_hrad );

    // vypnem audio zařízení
    CloseAudioDevice();
    
    // musíme uvolnit i muziku
    UnloadMusicStream ( hudba_lvl );
    UnloadMusicStream ( hudba_bonus );
    
    // a taky uvolníme zvuky
    UnloadSound ( zvuk_kroku );
    UnloadSound ( zvuk_skoku );
    UnloadSound ( zvuk_vystrel );
    UnloadSound ( zvuk_duch_chcip );
    UnloadSound ( zvuk_duch_strela );
    UnloadSound ( zvuk_zasah );
    UnloadSound ( zvuk_kontakt );
    UnloadSound ( zvuk_padu );
    UnloadSound ( zvuk_zaghrouta );
    UnloadSound ( zvuk_powerup );

    return 0;
}

#ifndef _DAVID_A_DUCHOVE_LEVEL_H_
#define _DAVID_A_DUCHOVE_LEVEL_H_

#include <raylib.h>
#include <stdlib.h>
#include <math.h>

#include "mapa.h"
#include "david.h"

// maximální vejška mapy (hodnota je v počtu kostek dlaždicový mapy)
#define MAPA_MAX_VYSKA 10

// struktura levelu
// zatim tam máme jenom tu mapu
typedef struct Level {

    Mapa * dlazdicova_mapa;

} Level;

// pomocná struktura pro generování mapy
// pude popisovat dílčí kousek ze kterejch budeme skládat tu svou náhodnou mapu
// bude to vlastně takový jakoby dvourozměrný pole
// (je to nadefinovaný jako jednorozměrný, ukazatelovou aritmetikou s nim budem pracovat jako s dvourozměrným )
typedef struct SegmentMapy {
    int sirka;
    int vyska;
    int  * pole;
} SegmentMapy;


// funkce na vygenerování náhodnýho levelu, vlastně něco jako konstruktor
// první argument 'šiřka' je počet kostek jak má bejt level dlouhej (předpokládá se čislo věčí dvacíti a dělitelný pěti)
// druhej textura tý dlaždicový mapy
Level * vygenerovatLevel ( int sirka, Texture2D texturaTiledMapy )
{
    // alokujem si tu strukturu kterou chcem jakože vracet
    Level * lvl = (Level * )malloc ( sizeof ( Level ) );
    
    // a mapu kterou do ní nacpem
    // (pozor, nehlídám selhávání alokace :O :O správně by sme měli uklízet alokovanou paměť když třeba selže až ten druhej malloc )
    Mapa * mapa = (Mapa * )malloc ( sizeof ( Mapa ) );

    // enum který nám bude popisovat jednotlivý prvky mapy, zsatim tám máme jenom 'N' jakože nic a 'B' jakože blok
    enum herniVec {N,B};

    // pole jednotlivejch segmentů, ze kterejch budeme skládat tu mapu
    // (sem to napsala takle 'dvourozměrně' na řádky by to bylo líp čitelný)
    
    // placka
    int seg1 [] = {
        0,0,0,0,0,
        0,0,0,0,0,
        0,0,0,0,0,
        0,0,0,0,0,
        B,B,B,B,B,
    };
    
    // taková malinkatá pyramidka
    int seg2 [] =
    {
        0,0,0,0,0,
        0,0,0,0,0,
        0,0,B,0,0,
        0,B,B,B,0,
        B,B,B,B,B
    };
    
    // díra v zemi
    // na ni si vyzkoušíme jak to vypadá když David spadne do ďoury a jak se při tom chová kamera
    int seg3 [] =
    {

        0,0,0,0,0,
        B,0,0,0,B,
    };

    // vyrobíme si ty pomocný struktury (vicemeně jenom by sme měli někde poznamennanou vejšku a šiřku těch segmentů,
    // páč nemusej bejt dycky stejně velký, jak to máme zrovna teďko)
    SegmentMapy segmenty [] = {
        ( SegmentMapy ) {5,5,seg1},
        ( SegmentMapy ) {5,5,seg2},
        ( SegmentMapy ) {5,2,seg3},
    };

    // počet těch segmentů ze kterejch budem vybírat
    const size_t segmentu = sizeof ( segmenty ) /sizeof ( SegmentMapy );

    // alokujem si bloky dlaždicový mapy
    int ** bloky = calloc ( MAPA_MAX_VYSKA, sizeof ( int * ) * MAPA_MAX_VYSKA );
    for ( size_t i=0; i<MAPA_MAX_VYSKA; i++ ) {
        bloky[i] = calloc ( sirka,sizeof ( int ) );
    }

    // prvních a posledních deset sloupečků herní mapy bude placka,
    // na začátku potřebujem dát hráčoj trošku volnýho místa by pak nespadnul rovnou někam do boje třeba,
    // na konci si pak uděláme ňákou specialitku která bude voznačovat konec mapy
    for ( size_t i=0; i<10; i++ ) {
        bloky[MAPA_MAX_VYSKA-1][i]=GetRandomValue ( 0,6 ) + 1; //vybíráme náhodnou texturu
        bloky[MAPA_MAX_VYSKA-1][ sirka - i - 1 ]=GetRandomValue ( 0,6 ) + 1;
    }

    // budeme postupně do mapy kopírovat náhodně vybraný segmenty pěkně v řadě za sebou,
    // na to si potřebujem měřit zbejvající místo v mapě
    int zbyva_delka = sirka - 10;

    // dokud je zbejvaicí dýlka věří než 15
    // k tomudle čislu sme došli tim, že chceme vynechat posledních 10 sloupců mapy a pětka je
    // nejmenčí možná dýlka náhodnýho segmentu. Takže dokavaď je dýlka rovna věčí patnácti, furt de
    // vygenerovat náhodnej segment aniž by sme vlezli do tý vyhrazený zóny na konci mapy
    while ( zbyva_delka >= 15 ) {
        
        // vyberem si náhodnej segment
        // (raylibová fuknce 'GetRandomValue' vrací celý čislo v rozsahu svýho prvního a druhýho argumentu(inkluzivně))
        int index = GetRandomValue ( 0,segmentu-1 );
        
        int vyska_segmentu = segmenty[index].vyska;
        int sirka_segmentu = segmenty[index].sirka;

        // pokud sme náhodou trefili moc dlouhej segment kterej se nám už na mapu nevejde (aniž by sme vlezli
        // do tý zóny na konci), tak si zkusíme vybrat jinej náhodnej segment
        if ( sirka_segmentu > zbyva_delka -10 ) {
            continue;
        }

        // noa teďko si projdem celý pole toho náhodně vybranýho segmentu....
        for ( size_t segment_y = 0; segment_y < vyska_segmentu; segment_y++ ) {
            for ( size_t segment_x = 0; segment_x < sirka_segmentu; segment_x++ ) {
                int hodnota = segmenty[index].pole[segment_x + segment_y * sirka_segmentu];

                // ....a podle toho na jakou hodnotu sme tam narazili se budem chovat
                // zatim tam máme jenom ten blok mapy
                switch ( hodnota ) {
                case B:
                    // vyrobíme náhodnej blok mapy
                    // zarovnáváme to k dolnímu vokraji mapy
                    bloky[segment_y + MAPA_MAX_VYSKA - vyska_segmentu][segment_x + ( sirka - zbyva_delka )] = GetRandomValue ( 0,6 ) + 1;
                    break;
                default:
                    break;
                };

            }

        }

        // vod zbejvajicí dýlky vodečtem dýlku segmentu kterej sme tam nacpali
        zbyva_delka-=sirka_segmentu;
    }

    // strčíme bloky do mapy
    mapa->bloky = bloky;
    mapa->sirka = sirka;
    mapa->vyska = MAPA_MAX_VYSKA;
    mapa->textura = texturaTiledMapy;
    
    // a mapu strčíme do levelu
    lvl->dlazdicova_mapa = mapa;

    return lvl;
}

// až si s levelem přestanem hrát, tak musíme alokovanou paměť po sobě uklidit
// (a alokovanou pamět se musí dycky pak volat free())
void freeLevel ( Level * lvl )
{
    for ( size_t i=0; i<MAPA_MAX_VYSKA; i++ ) {
        free ( lvl->dlazdicova_mapa->bloky[i] );
    }
    free ( lvl->dlazdicova_mapa->bloky );
    free ( lvl->dlazdicova_mapa );
    
    free ( lvl );
}

void aktualizovatLevel ( Level * lvl, David * david, float dt )
{
    // zatim eště nemáme co aktualizovat
}

// vykreslíme level
void vykreslitLevel ( Level * lvl, Camera2D * kamera )
{
    // minimální a maximální vykreslovanou vzdalenost dlaždicový mapy spočitáme z kamery
    // kamera kouká někam doprostředka vobrazovky, takže když k tomu přičtem půlku šířky vobrazovky
    // na vobě strany, tak si krásně vymezíme v dlaždicový mapě voblast vobrazovky
    // (vodtransformováváme hodnotu z kamerovýho světa, proto to eště dělíme hodnotou zoomu)
    float min_x = kamera->target.x - GetRenderWidth() / 2.0f / kamera->zoom;
    float max_x = kamera->target.x + GetRenderWidth() / 2.0f / kamera->zoom;
    vykreslitMapu ( lvl->dlazdicova_mapa,min_x,max_x );

}

#endif

#ifndef _DAVID_A_DUCHOVE_ANIMACE_H_
#define _DAVID_A_DUCHOVE_ANIMACE_H_
// ten fígl s ifndef nahoře slouží k tomu by se nám
// soubor naimportoval jenom jednou

#include <raylib.h>

typedef struct Animace {
    
    // textura animace
    Texture2D textura;

    // jak dlouho bude trvat jeden frame/snímek animace
    float trvani_framu;
    
    // jakože vnitřní čas tý animace (se bude hodit)
    float relativni_cas;

    // jestli je animace pozastavená
    bool pauznuta;
    
    // jestli má jojo efekt
    bool jojo;
    
    // jestli běží pozpátku
    bool reverzne;
    
    // jestli běží furt dokolečka nebo jestli se zasekné, když dojede nakonec
    bool loopovat;
    
    // jestli animaci vykreslujeme překlopenou vodorovně/po ose x
    bool zrcadlit;

    // ukazatel na pole framů (v cčku je pole a ukazatel tak trochu uplně to samý)
    Rectangle * framy;
    
    // kolik má animace framů celkem
    size_t pocet_framu;
    
    // index právě zobrazenýho framu
    int index;
} Animace;

// bere dva argumenty, ukazatel na animaci noa pak časovou deltu 
void aktualizovatAnimaci ( Animace * animace, float dt )
{
    if ( animace->pauznuta ) {
        return;
    }
    animace->relativni_cas += dt;

    while ( animace->relativni_cas > animace->trvani_framu ) {
        animace->relativni_cas -= animace->trvani_framu;

        if ( animace->reverzne ) {
            if ( --animace->index <= 0 ) {
                if ( animace->loopovat ) {
                    if ( animace->jojo ) {
                        animace->index = 0;
                        animace->reverzne = false;
                    } else {
                        animace->index = animace->pocet_framu - 1;
                    }
                } else {
                    animace->pauznuta=true;
                    animace->index=0;
                    return;
                }
            }
        }

        else {
            if ( ++animace->index == animace->pocet_framu ) {
                if ( animace->loopovat ) {
                    if ( animace->jojo ) {
                        animace->index = animace->pocet_framu - 1;
                        animace->reverzne = true;
                    } else {
                        animace->index = 0;
                    }
                } else {
                    animace->pauznuta=true;
                    animace->index =animace->pocet_framu-1;
                    return;
                }
            }
        }

    }
}

// bere tři argumenty, ukazatel na animaci, souřadnici kde se bude vykreslovat levej horní roh a barvu
void vykreslitAnimaci ( Animace * animace, Vector2 pozice, Color barva )
{
    // převrácení po některý s os dosháhneme nastavením šiřky a/nebo vejšky
    // zdrojovýho vobdelnika na zápornou hodnotu
    // ( víc efektivnější by asi bylo mit dvě sady animací, jednu normální a druhou s
    // překlopenou šiřkou, než to počitat v každým kroku renderu)
    if ( animace->zrcadlit ) {
        Rectangle frame = animace->framy[animace->index];
        frame.width = - frame.width;

        DrawTextureRec ( animace->textura, frame,pozice, barva );
    } else {
        DrawTextureRec ( animace->textura, animace->framy[animace->index],pozice, barva );
    }
}

#endif

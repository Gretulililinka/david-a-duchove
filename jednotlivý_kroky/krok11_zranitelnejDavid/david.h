#ifndef _DAVID_A_DUCHOVE_DAVID_H_
#define _DAVID_A_DUCHOVE_DAVID_H_

#include "animace.h"
#include "mapa.h"
#include "projektily.h"

extern Sound zvuk_kroku;
extern Sound zvuk_skoku;
extern Sound zvuk_vystrel;

// načtem si zvuk pádu kterej budem hrát dycky když nám David zahučí někam do ďoury v zemi
extern Sound zvuk_padu;

#define DAVID_F_SIRKA 150.0f
#define DAVID_F_VYSKA 240.0f

Rectangle david_framy_behu[] = {
    {DAVID_F_SIRKA*0,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*1,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*2,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*3,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*4,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*5,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},

    {DAVID_F_SIRKA*0,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*1,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*2,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA}
};

Rectangle david_framy_skoku[] = {

    {DAVID_F_SIRKA*5,DAVID_F_VYSKA*0,DAVID_F_SIRKA,DAVID_F_VYSKA},

    {DAVID_F_SIRKA*0,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*1,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*2,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA}


};

Rectangle david_framy_idle[] = {
    {DAVID_F_SIRKA*4,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*5,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*0,DAVID_F_VYSKA*3,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*1,DAVID_F_VYSKA*3,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*2,DAVID_F_VYSKA*3,DAVID_F_SIRKA,DAVID_F_VYSKA},
};

Rectangle david_framy_sed[] = {
    {DAVID_F_SIRKA*3,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*4,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*5,DAVID_F_VYSKA*1,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*0,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*1,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*2,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
    {DAVID_F_SIRKA*3,DAVID_F_VYSKA*2,DAVID_F_SIRKA,DAVID_F_VYSKA},
};

#define RYCHLOST_CHUZE_DAVIDA 350.0f
#define RYCHLOST_SKOKU_DAVIDA -15.0f
#define GRAVITACE_DAVIDA 40.0f
#define RYCHLOST_STREL_DAVIDA 800.0f
#define STRILECI_COOLDOWN_DAVIDA 0.25f
#define POCET_STREL_DAVIDA_MAX 32

// přidáme si sem maximální počet životů/hitpointů davida
#define POCET_ZIVOTU_DAVIDA_MAX 10
// a blikací čas (v sekundách)
#define BLIKACI_CAS_DAVIDA 0.5f

typedef struct David {
    
    Animace animace_beh;
    Animace animace_idle;
    Animace animace_sed;
    Animace animace_skok;

    Animace * aktualni_animace;

    Vector2 pozice;
    
    Rectangle okraje;
    
    // david bude mit vodteď hitbox vymezujicí jeho zranitelnou voblast, podobně jako duchové
    Rectangle hitbox; 
    
    int smer;
    
    Mapa * mapa;

    bool zdaSkace;
    float vertikalni_rychlost;
    
    Strela * strely;
    float strileci_cooldown;
    
    // david bude mit eště další nový tři atributy
    // aktuální počet životů
    int zivoty;
    
    // zbejvajicí čas blikání (dočasný nezranitelnosti po zásahu)
    float blikaci_cas;
    
    // a boolean jestli je právě teďko zranitelnej nebo ne
    bool zranitelny;

} David;


void aktualizovatDavida ( David * david, float dt )
{

    if ( IsKeyDown ( KEY_UP ) ) {

        if ( david->aktualni_animace == &david->animace_sed ) {
            david->aktualni_animace->pauznuta = false;
            david->aktualni_animace->reverzne=true;

            if ( david->aktualni_animace->index == 0 ) {
                david->aktualni_animace = & david->animace_idle;
            }

        } else if ( !david->zdaSkace ) {
            PlaySound ( zvuk_skoku );
            david->zdaSkace = true;
            david->animace_skok.index = 0;
            david->vertikalni_rychlost = RYCHLOST_SKOKU_DAVIDA;
        }
    }
    
    // když hráč zmáčkne mezernik, tak se David pokusí vystřelit z tý svý pistolky
    if ( IsKeyDown ( KEY_SPACE ) ) {
        // vystřelit mužeme jenom když uplynula čekací doba mezi výstřelama resp. střílecí cooldown je menší nebo rovnej nule
        if ( david->strileci_cooldown <= 0.0f ) {
            // projdeme si celej davidův zásobník a pokusíme se v něm najít střelu, kterou budeme moct použít
            // to poznáme tak, že bude mit atribut aktivní nastavenej na nulu
            for ( size_t i=0; i<POCET_STREL_DAVIDA_MAX; i++ ) {
                if ( !david->strely[i].aktivni ) {
                    
                    // pokud sme takovou střelu našli, tak si vybereme pro upravování atributů
                    Strela * s = david->strely + i;

                    // nejdřiv ji nastavíme novou polohu,
                    //to znamená přibližně někam na konec hlavně tý pistolky co má david v rukou
                    
                    //nj jenže david muže stát, sedět na zemi, muže koukat z prava doleva, nebo muže dokonce právě vstávat ze země
                    // všecky tydlecty eventualitky jakoby musíme pokrejt
                    if ( david->aktualni_animace != &david->animace_sed )
                        s->pozice = ( Vector2 ) {
                        // střelu umisťujem podle toho jakým směrem david kouká
                        david->smer==1? david->pozice.x+DAVID_F_SIRKA - 25: david->pozice.x+25,
                        david->pozice.y + DAVID_F_VYSKA/2 - 36
                    };
                    else {
                        // pokud david má jako aktuální animaci sedání, tak si zistíme index a vo ten budeme posouvat iksovou a ypsilonovou
                        // souřadnici střely. Neni to uplně přesný ale na to nikdo koukat nebude :D
                        int index = david->animace_sed.index;
                        s->pozice = ( Vector2 ) {
                            david->smer==1? david->pozice.x+DAVID_F_SIRKA - 25 - index*5: david->pozice.x+25 + index*5,david->pozice.y + DAVID_F_VYSKA/2 - 26 - 8 + index*14
                        };

                    }
                    
                    //polohu máme, teďko nastavíme další atributy střely
                    
                    // nastavíme střele rychlost, zohledníme i směr kterým poletí, ten vodpovídá
                    // směru kterým David právě teďko kouká
                    s->rychlost = RYCHLOST_STREL_DAVIDA * ( float ) david->smer;
                    
                    // vynulujem relativní čas
                    s->relativni_cas = 0.0f;
                    
                    // nastavíme dobu života třeba na čtyry vteřiny
                    s->doba_zivota = 4.0f;
                    
                    // a aktivujem
                    s->aktivni=true;
                    
                    // střelu máme upravenou, ukazatel už nepotřebujem
                    s=0;

                    // ..a když sme aktivovali střelu, tak sme vlastně vystřelili, takže nastavíme střílecí čekací dobu
                    // na maximální hodnotu
                    david->strileci_cooldown = STRILECI_COOLDOWN_DAVIDA;
                    
                    // zahrajem zvuk výstřelu
                    PlaySound ( zvuk_vystrel );
                    
                    // a přerušíme hledací for cyklus
                    break;
                }
            }
        }
        // eště upravíme aktuální animaci, pokud to neni animace sedání, tak přepnem animaci na idle na první snímek,
        // páč při tý animaci david hejbe pistolkou a vypadalo by to divně kdyby z ní vylítla vodorovně střela. Pokud david skáče,
        // běží nebo padá, tak se nám ta animace stejně přepne někde dál tady ve zdrojáčku týdle funkce
        // (při animaci běhu/skoků todle nepřectavuje problém, pistolka je na všech vobrázcích spritesheetu ve stejný vejšce,
        // kromě tý animace 'idle', tam je ve stejný vejšce právě jenom na prvním snimku)
        if ( david->aktualni_animace != &david->animace_sed ) {
            david->aktualni_animace = & david->animace_idle;
            david->aktualni_animace->index = 0;
            david->aktualni_animace->relativni_cas = 0.0f;
        }
    }


    
    if ( IsKeyDown ( KEY_LEFT ) && david->pozice.x > 0 && david->aktualni_animace != &david->animace_sed ) {
        
        david->smer = -1;
        
        Rectangle prepozice = david->okraje;
        prepozice.x -= RYCHLOST_CHUZE_DAVIDA * dt;

        if ( kolizeRectSeBlokemMapy ( prepozice, david->mapa ) ) {
            david->aktualni_animace = & david->animace_idle;
        } else {

            david->okraje.x = prepozice.x;
            david->pozice.x = david->okraje.x - 45;
            
            // aktualizujem pozici hitboxu
            david->hitbox.x = david->okraje.x + 10;
            
            david->aktualni_animace = &david->animace_beh;
            }

        }
    
    else if ( IsKeyDown ( KEY_RIGHT ) && david->pozice.x < david->mapa->sirka*BLOK_SIRKA - DAVID_F_SIRKA && david->aktualni_animace != &david->animace_sed ) {

        david->smer = 1;
        Rectangle prepozice = david->okraje;
        prepozice.x += RYCHLOST_CHUZE_DAVIDA * dt;
        
        if ( kolizeRectSeBlokemMapy ( prepozice, david->mapa ) ) {
            david->aktualni_animace = & david->animace_idle;
        } else {
            david->okraje.x = prepozice.x;
            david->pozice.x = david->okraje.x - 45;
            
            // aktualizujem pozici hitboxu
            david->hitbox.x = david->okraje.x + 10;
            
            david->aktualni_animace = & david->animace_beh;
            } 
    }
    // jestli je máčkutej na klávesnici čudlik šipky dolu, tak začnem přehrávat animaci sednutí
    // si na zadek
    else if ( IsKeyDown ( KEY_DOWN ) ) {

        david->aktualni_animace = & david->animace_sed;
        david->aktualni_animace->reverzne = false;
        david->aktualni_animace->pauznuta = false;
        }
        
    else if ( david->aktualni_animace != &david->animace_sed ) {
        david->aktualni_animace = & david->animace_idle;
        }
        
    Rectangle prepozice = david->okraje;
    prepozice.y += 5;
    if ( ! kolizeRectSeBlokemMapy ( prepozice, david->mapa ) ) {
        david->zdaSkace = true;
    }

    if ( david->zdaSkace ) {

        david->vertikalni_rychlost += dt * GRAVITACE_DAVIDA;
        
        Rectangle prepozice = david->okraje;
        prepozice.y += david->vertikalni_rychlost;

        if ( kolizeRectSeBlokemMapy ( prepozice, david->mapa ) ) {
            
            david->vertikalni_rychlost = 0.0f;
            david->zdaSkace = false;

            david->okraje.y =  ceil ( ( david->okraje.y + david->okraje.height  ) /BLOK_VYSKA ) * BLOK_VYSKA - david->okraje.height - 1;

            if ( david->aktualni_animace == &david->animace_beh ) {
                david->aktualni_animace->index = david->animace_skok.index +5;
                david->aktualni_animace->reverzne = david->animace_skok.reverzne;
            }

        } else {
            
            david->aktualni_animace = & david->animace_skok;
            
            david->okraje.y += david->vertikalni_rychlost;

        }

        // aktualizujem po dopadu taky hitbox
        david->hitbox.y = david->okraje.y + 12;
        david->pozice.y = david->okraje.y - 3;
        

    }

    david->aktualni_animace->zrcadlit = david->smer<0;


    if ( david->aktualni_animace == &david->animace_sed ) {
        
        // pokud má David jako aktualní animaci sedání, tak mu musíme nastavit hitbox podle jednotlivejch framů tý animace
        // asi to takle nebude uplně přesný, nicmeně pro náš učel to je dostatečně přesný, schvalně si zapněte v main.c DEBUG :D ;D
        david->hitbox= ( Rectangle ) {
            david->pozice.x + 55 + david->smer* ( -5*david->animace_sed.index ), david->pozice.y + 10 + 13*david->animace_sed.index, DAVID_F_SIRKA -110, DAVID_F_VYSKA-30-13*david->animace_sed.index
        };
        
        
        if ( david->animace_sed.pauznuta ) {
            if ( david->animace_sed.index == 0 ) {
                david->aktualni_animace = & david->animace_idle;
            }
        }

    }

    else if ( david->aktualni_animace == &david->animace_beh ) {
        if ( !IsSoundPlaying ( zvuk_kroku ) ) {
            PlaySound ( zvuk_kroku );
        }
    }
    
    // jestli má David moc podezřele velkou ypsilonovou souřadnici tak asi jako nejspíš spadnul do ďoury
    // zahrajeme zvuk pádu (jestli už nehraje) a budem mu postupně každým voláním týdle aktualizvávací funkce
    // vodečítat jeden život za druhým až uplně umře :O ;D
    if ( david->pozice.y > BLOK_VYSKA*13 ) {
        if ( !IsSoundPlaying ( zvuk_padu ) ) {
            PlaySound ( zvuk_padu );
        }

        david->zivoty--;
    }
    
    if ( david->aktualni_animace == &david->animace_skok && IsSoundPlaying ( zvuk_kroku ) ) {
        StopSound ( zvuk_kroku );
    }
    
    if ( david->strileci_cooldown > 0.0f ) {
        david->strileci_cooldown -= dt;
    }
    
    // pokud je davidův blikací čas věčí než nula, tak vod něj vodečtem časovou deltu
    // a uděláme Davida nezranitelnýho, páč furt eště bliká
    if ( david->blikaci_cas > 0.0f ) {
        david->blikaci_cas -= dt;
        david->zranitelny = false;
    } else {
        //ale jestli už doblikal, tak už zase zranitelnej bude
        david->zranitelny = true;
    }
    
    for ( size_t i=0; i<POCET_STREL_DAVIDA_MAX; i++ ) {
        aktualizovatStrelu ( david->strely+i, david->mapa,dt );
    }

    aktualizovatAnimaci ( david->aktualni_animace, dt );

}

// funkce na vykreslování Davida
void vykreslitDavida ( David * david )
{
    // pokud je David nezranitelnej, tak bude červeně blikat
    // podělíme si blikací čas ňákým kouskem kterej nám bude určovat periodu blikání
    // tou desetinou bliknem nějak 10x za vteřinu
    if ( !david->zranitelny ) {
        Color barva = ( int ) ( david->blikaci_cas/0.1f ) %2 ? WHITE : RED;
        vykreslitAnimaci ( david->aktualni_animace, david->pozice,barva );
    } else {
        vykreslitAnimaci ( david->aktualni_animace, david->pozice,WHITE );
    }
    
    for ( size_t i=0; i<POCET_STREL_DAVIDA_MAX; i++ ) {
        vykreslitStrelu ( david->strely+i );
    }

#ifdef DEBUG
    DrawRectangleLines ( david->okraje.x, david->okraje.y, david->okraje.width, david->okraje.height, GREEN );
    // nově teďko vykreslujem červeně i ten hitbox
    DrawRectangleLines ( david->hitbox.x, david->hitbox.y, david->hitbox.width, david->hitbox.height, RED );
#endif
}

#endif
